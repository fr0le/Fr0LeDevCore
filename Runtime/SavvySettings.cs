﻿using Savvy.Infrastructure;
using Savvy.Services;
using UnityEngine;

namespace Savvy
{
    [CreateAssetMenu(menuName = nameof(Savvy) + "/" + nameof(SavvySettings), fileName = nameof(SavvySettings))]
    public class SavvySettings : ValidateScriptableObject, ISavvySettings
    {
        [Header("Logging")] 
        public bool Debug = false;

        [Header("Bootstrap Editor Wrapper")] 
        public bool ClearEditorConsole = false;

        [Header("Settings")]
        [Range(-2, -1)]
        public int SleepTimeout = -1;
        [Range(60, 120)]
        public int TargetFrameRate = 120;

        [Header("Register Services")] 
        public bool AppMetrica = false;
        public bool AssetProvider = false;
        public bool Audio = false;
        public bool CAS = false;
        public bool CoroutineRunner = false;
        public bool CountryChecker = false;
        public bool DateTime = false;
        public bool Factory = false;
        public bool FirebaseApp = false;
        public bool FirebaseRemoteConfig = false;
        public bool IAP = false;
        public bool Localization = false;
        public bool Logger = true;
        public bool Notification = false;
        public bool Numbers = false;
        public bool Preferences = false;
        public bool Random = false;
        public bool SaveLoad = false;
        public bool SceneLoader = false;
        public bool Social = false;
        public bool StateMachine = false;
        public bool Tenjin = false;
        public bool UnityGaming = false;
        public bool Vibration = false;
        
        [Header("Register Game Dev Club Services")] 
        public bool Yookassa = false;
    }
}