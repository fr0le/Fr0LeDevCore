﻿using UnityEngine;

namespace Savvy
{
    public static class SavvyConstants
    {
        public static readonly Color LogColor = default;
        
        public const string ResourcesPath = "Assets" + "/" + nameof(Resources);
        public const string SavvyPath = ResourcesPath + "/" + nameof(Savvy);
        public const string TranslationsPath = ResourcesPath + "/" + "Translations";

        public const string AppMetricaDefineSymbols = "SAVVY_APP_METRICA";
        public const string AssetProviderDefineSymbols = "SAVVY_ASSET_PROVIDER";
        public const string CASDefineSymbols = "SAVVY_CAS";
        public const string FirebaseAppDefineSymbols = "SAVVY_FIREBASE_APP";
        public const string FirebaseRemoteConfigDefineSymbols = "SAVVY_FIREBASE_REMOTE_CONFIG";
        public const string IAPDefineSymbols = "SAVVY_IAP";
        public const string NotificationDefineSymbols = "SAVVY_NOTIFICATION";
        public const string TenjinDefineSymbols = "SAVVY_TENJIN";
        public const string UnityGamingDefineSymbols = "SAVVY_UNITY_GAMING";

        public const string CreateSavvySettingsFileButton = "Create Savvy Settings File";
        public const string ResolveButton = "Resolve";
        public const string DownloadRemoteLocalizationsButton = "Download Remote Localizations";
        public const string OpenRemoteLocalizationsButton = "Open Remote Localizations";
    }
}