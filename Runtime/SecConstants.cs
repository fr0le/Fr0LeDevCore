﻿namespace Savvy
{
    public static class SecConstants
    {
        public const int Day = 86400;
        public const int Hour = 3600;
        public const int Min = 60;
        public const int Sec = 1;
        public const float HalfSec = 0.5f;
        public const float QuarterSec = 0.25f;
    }
}