using UnityEngine;
#if UNITY_EDITOR
using System.Reflection;
using Savvy.Editor;
using Savvy.Extensions;
using UnityEditor;
using UnityEngine.SceneManagement;
#endif

namespace Savvy.Infrastructure
{
    public class BootstrapEditorWrapper : MonoBehaviour
    {
#if UNITY_EDITOR
        [SerializeField] private int _defaultSceneIndex = 0;

        private static BootstrapEditorWrapper _instance;

        private readonly string _sourceSettings = $"{nameof(SavvySettings)}";

        private SavvySettings _settings;

        private void Awake()
        {
            if (_instance == null)
            {
                DontDestroyOnLoad(this);
                _instance = this;
                _settings = _sourceSettings.LoadResources<SavvySettings>();
                SceneManager.sceneUnloaded += OnSceneUnloaded;

                if (SceneManager.GetActiveScene().buildIndex != _defaultSceneIndex)
                    SceneManager.LoadSceneAsync(_defaultSceneIndex);
            }
            else
            {
                Destroy(gameObject);
            }
        }

        private void OnSceneUnloaded(Scene scene)
        {
            ClearEditorConsole(scene.buildIndex);
            LoggerEditor.Debug($"Load bootstrap scene '{SceneManager.GetSceneByBuildIndex(_defaultSceneIndex).name}'");
            SceneManager.sceneUnloaded -= OnSceneUnloaded;
        }

        private void ClearEditorConsole(int sceneIndex)
        {
            if (!_settings.ClearEditorConsole)
                return;

            if (sceneIndex == _defaultSceneIndex)
                return;

            var assembly = Assembly.GetAssembly(typeof(SceneView));
            var type = assembly.GetType("UnityEditor.LogEntries");
            var method = type.GetMethod("Clear");
            method?.Invoke(new object(), null);
        }
#endif
    }
}