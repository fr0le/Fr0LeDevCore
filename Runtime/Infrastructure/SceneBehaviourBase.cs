﻿using Savvy.Extensions;
using Savvy.Services;

namespace Savvy.Infrastructure
{
    public abstract class SceneBehaviourBase : MonoSavvy
    {
        private const ServiceType ServiceType = Services.ServiceType.Scene;
        private readonly string _sourceSettings = $"{nameof(SavvySettings)}";

        private SavvySettings _settings;
        private Service _container;
        private bool _isPause;

        private async void Awake()
        {
            Setup();
            RegisterGameServices();
            await _container.SetupFromBehaviourAsync(ServiceType);
            PostAwake();
        }

        protected void RegisterService<TService>(TService service) where TService : IService =>
            _container.RegisterSingle(service, ServiceType);

        private void Setup()
        {
            _settings = _sourceSettings.LoadResources<SavvySettings>();
            _container = Service.Container;
        }

        protected virtual void RegisterGameServices() { }

        protected virtual void PostAwake() { }

        private void OnDestroy()
        {
            _container.DestroyFromBehaviour(ServiceType);
            Debug("'Scene Behaviour' destroyed", _settings.Debug);
        }
    }
}