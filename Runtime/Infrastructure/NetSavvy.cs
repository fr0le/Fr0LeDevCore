﻿using Savvy.Services;
using Savvy.Services.Logger;

namespace Savvy.Infrastructure
{
    public class NetSavvy
    {
        private ILoggerService _log;

        protected TService GetService<TService>() where TService : IService =>
            Service.Container.GetSingle<TService>();

        protected void Info(object msg, bool isInfo = true)
        {
            if (!isInfo)
                return;

            if (GetLoggerService() != null)
                _log.Info(msg);
            else
                UnityEngine.Debug.Log(msg);
        }

        protected void Verbose(object msg, bool isVerbose = true)
        {
            if (!isVerbose)
                return;

            if (GetLoggerService() != null)
                _log.Verbose(msg);
            else
                UnityEngine.Debug.Log(msg);
        }

        protected void Debug(object msg, bool isDebug = true)
        {
            if (!isDebug)
                return;

            if (GetLoggerService() != null)
                _log.Debug(msg);
            else
                UnityEngine.Debug.Log(msg);
        }

        protected void Warning(object msg, bool isWarning = true)
        {
            if (!isWarning)
                return;

            if (GetLoggerService() != null)
                _log.Warning(msg);
            else
                UnityEngine.Debug.LogWarning(msg);
        }

        protected void Error(object msg, bool isError = true)
        {
            if (!isError)
                return;

            if (GetLoggerService() != null)
                _log.Error(msg);
            else
                UnityEngine.Debug.LogError(msg);
        }

        protected void Critical(object msg, bool isCritical = true)
        {
            if (!isCritical)
                return;

            if (GetLoggerService() != null)
                _log.Critical(msg);
            else
                UnityEngine.Debug.LogError(msg);
        }

        private ILoggerService GetLoggerService() =>
            _log ??= SingleService<ILoggerService>.Instance;
    }
}