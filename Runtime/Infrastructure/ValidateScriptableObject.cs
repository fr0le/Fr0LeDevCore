using UnityEngine;
#if UNITY_EDITOR
using Savvy.Editor;
using UnityEditor;
#endif

namespace Savvy.Infrastructure
{
    public abstract class ValidateScriptableObject : ScriptableObject
    {
#if UNITY_EDITOR
        private bool _isSaveScheduled;
        private double _lastEditTime;
        private float _delay = 1;

        private void OnValidate()
        {
            if (Application.isPlaying)
                return;
            
            EditorUtility.SetDirty(this);
            _lastEditTime = EditorApplication.timeSinceStartup;

            if (!_isSaveScheduled)
            {
                _isSaveScheduled = true;
                EditorApplication.update += DelayedSave;
            }
        }

        private void DelayedSave()
        {
            if (EditorApplication.timeSinceStartup - _lastEditTime >= _delay)
            {
                AssetDatabase.SaveAssets();
                _isSaveScheduled = false;
                EditorApplication.update -= DelayedSave;
            }
        }
#endif
    }
}