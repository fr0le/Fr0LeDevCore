﻿using Savvy.Extensions;
using Savvy.Services;
using Savvy.Services.AppMetricaWrapper;
using Savvy.Services.AssetProvider;
using Savvy.Services.Audio;
using Savvy.Services.CAS;
using Savvy.Services.CoroutineRunner;
using Savvy.Services.CountryChecker;
using Savvy.Services.DateTime;
using Savvy.Services.Factory;
using Savvy.Services.FirebaseApp;
using Savvy.Services.Localization;
using Savvy.Services.Logger;
using Savvy.Services.Notification;
using Savvy.Services.Numbers;
using Savvy.Services.Preferences;
using Savvy.Services.Randomize;
using Savvy.Services.SceneLoader;
using Savvy.Services.Social;
using Savvy.Services.StateMachine;
using Savvy.Services.TenjinWrapper;
using Savvy.Services.UnityGaming;
using Savvy.Services.Vibration;
using UnityEngine;

namespace Savvy.Infrastructure
{
    public abstract class ProjectBehaviourBase : MonoSavvy
    {
        private const ServiceType ServiceType = Services.ServiceType.Project;
        private readonly string _sourceSettings = $"{nameof(SavvySettings)}";

        private SavvySettings _settings;
        private Service _container;
        private bool _isUpdate;
        private bool _isPause;

        private async void Awake()
        {
            DontDestroyOnLoad(this);
            Setup();
            InitializeSettings();
            RegisterSavvyServices();
            RegisterGameServices();
            await _container.SetupFromBehaviourAsync(ServiceType);
            _isUpdate = true;
            PostAwake();
        }

        protected void RegisterService<TService>(TService service) where TService : IService =>
            _container.RegisterSingle(service, ServiceType);

        private void Setup()
        {
            _settings = _sourceSettings.LoadResources<SavvySettings>();
            _container = new Service(_settings);
        }

        private void InitializeSettings()
        {
            Screen.sleepTimeout = _settings.SleepTimeout;
            Application.targetFrameRate = _settings.TargetFrameRate;
        }

        private void RegisterSavvyServices()
        {
            if (_settings.Logger)
                RegisterLoggerService();
            
            if (_settings.UnityGaming)
                RegisterUnityGamingService();

            if (_settings.IAP)
                RegisterIAPService();
            
            if (_settings.Tenjin)
                RegisterTenjinService();
            
            if (_settings.FirebaseApp)
                RegisterFirebaseAppService();
            
            if (_settings.FirebaseRemoteConfig)
                RegisterFirebaseRemoteConfigService();
            
            if (_settings.AppMetrica)
                RegisterAppMetricaService();
            
            if (_settings.AssetProvider)
                RegisterAssetProviderService();
            
            if (_settings.Audio)
                RegisterAudioService();
            
            if (_settings.CAS)
                RegisterCASService();

            if (_settings.CoroutineRunner)
                RegisterCoroutineRunnerService();

            if (_settings.CountryChecker)
                RegisterCountryCheckerService();
            
            if (_settings.DateTime)
                RegisterDateTimeService();

            if (_settings.Factory)
                RegisterFactoryService();
            
            if (_settings.Localization)
                RegisterLocalizationService();

            if (_settings.Notification)
                RegisterNotificationService();

            if (_settings.Numbers)
                RegisterNumbersService();

            if (_settings.Preferences)
                RegisterPreferencesService();
            
            if (_settings.Random)
                RegisterRandomService();

            if (_settings.SaveLoad)
                RegisterSaveLoadService();

            if (_settings.SceneLoader)
                RegisterSceneLoaderService();

            if (_settings.Social)
                RegisterSocialService();
            
            if (_settings.StateMachine)
                RegisterStateMachineService();

            if (_settings.Vibration)
                RegisterVibrationService();

            if (_settings.Yookassa)
                RegisterYookassaService();
        }

        protected virtual void RegisterAppMetricaService() =>
            RegisterService<IAppMetricaService>(new AppMetricaService());

        protected virtual void RegisterAssetProviderService() =>
            RegisterService<IAssetProviderService>(new AssetProviderService());
        
        protected virtual void RegisterAudioService() =>
            RegisterService<IAudioService>(new AudioService());
        
        protected virtual void RegisterCASService() =>
            RegisterService<ICASService>(new CASService());

        protected virtual void RegisterCoroutineRunnerService() =>
            RegisterService<ICoroutineRunnerService>(new CoroutineRunnerService(this));
        
        protected virtual void RegisterCountryCheckerService() =>
            RegisterService<ICountryCheckerService>(new CountryCheckerService());
        
        protected virtual void RegisterDateTimeService() =>
            RegisterService<IDateTimeService>(new DateTimeService());

        protected virtual void RegisterFactoryService() =>
            RegisterService<IFactoryService>(new FactoryService());
        
        protected virtual void RegisterFirebaseAppService() =>
            RegisterService<IFirebaseAppService>(new FirebaseAppService());

        protected virtual void RegisterFirebaseRemoteConfigService() =>
            RealizationError(nameof(_settings.FirebaseRemoteConfig));

        protected virtual void RegisterIAPService() =>
            RealizationError(nameof(_settings.IAP));

        protected virtual void RegisterLocalizationService() =>
            RegisterService<ILocalizationService>(new LocalizationService());
        
        protected virtual void RegisterNotificationService() =>
            RegisterService<INotificationService>(new NotificationService());

        protected virtual void RegisterLoggerService() =>
            RegisterService<ILoggerService>(new LoggerService());

        protected virtual void RegisterNumbersService() =>
            RegisterService<INumbersService>(new NumbersService());

        protected virtual void RegisterPreferencesService() =>
            RegisterService<IPreferencesService>(new PreferencesService());
        
        protected virtual void RegisterRandomService() =>
            RegisterService<IRandomService>(new RandomService());

        protected virtual void RegisterSaveLoadService() =>
            RealizationError(nameof(_settings.SaveLoad));

        protected virtual void RegisterSceneLoaderService() =>
            RegisterService<ISceneLoaderService>(new SceneLoaderService());

        protected virtual void RegisterSocialService() =>
            RegisterService<ISocialService>(new SocialService());

        protected virtual void RegisterStateMachineService() =>
            RegisterService<IStateMachineService>(new StateMachineService());

        protected virtual void RegisterTenjinService() =>
            RegisterService<ITenjinService>(new TenjinService());

        protected virtual void RegisterVibrationService() =>
            RegisterService<IVibrationService>(new VibrationService());
        
        protected virtual void RegisterUnityGamingService() =>
            RegisterService<IUnityGamingService>(new UnityGamingService());

        protected virtual void RegisterYookassaService() =>
            RealizationError(nameof(_settings.Yookassa));
        
        protected virtual void RegisterGameServices() { }

        protected virtual void PostAwake() =>
            Error($"Realize next loading logic in the '{nameof(PostAwake)}' in the inherited from '{name}' class");
        
        private void RealizationError(string serviceName) =>
            Error($"'{serviceName}' realization missing. Realize the service in the inherited from '{name}' class");
        
        private void Update()
        {
            if (_isUpdate)
                _container.UpdateFromBehaviour();
        }

        private void FixedUpdate()
        {
            if (_isUpdate)
                _container.FixedUpdateFromBehaviour();
        }

        private void LateUpdate()
        {
            if (_isUpdate)
                _container.LateUpdateFromBehaviour();
        }

        private void OnApplicationPause(bool isPause)
        {
            if (_isPause != isPause)
            {
                _isPause = isPause;
                _container.PauseFromBehaviour(_isPause);
            }
        }

        private void OnDestroy()
        {
            _container.DestroyFromBehaviour(ServiceType);
            Debug("'Project Behaviour' destroyed", _settings.Debug);
        }
    }
}