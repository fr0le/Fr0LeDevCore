﻿#if UNITY_EDITOR
using UnityEngine;
using UnityDebug = UnityEngine.Debug;

namespace Savvy.Editor
{
    public static class LoggerEditor
    {
        public static void Debug(string log) => 
            UnityDebug.Log($"[<color=#{ColorUtility.ToHtmlStringRGB(SavvyConstants.LogColor)}>{nameof(Savvy)}</color>] {log}");

        public static void Error(string log) => 
            UnityDebug.LogError($"[<color=#{ColorUtility.ToHtmlStringRGB(SavvyConstants.LogColor)}>{nameof(Savvy)}</color>] {log}");
    }
}
#endif