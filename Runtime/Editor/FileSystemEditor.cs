﻿#if UNITY_EDITOR
using System.IO;
using UnityEditor;
using UnityEngine;

namespace Savvy.Editor
{
    public static class FileSystemEditor
    {
        public static void CreateAssetFile<TData>(string path = SavvyConstants.SavvyPath) where TData : ScriptableObject
        {
            CreateFoldersRecursively(path);
            var asset = typeof(TData).Name;
            var assetPath = Path.Combine(path, $"{asset}.asset");
            var filePath = $"{path}/{asset}.asset";
            var scriptableObject = ScriptableObject.CreateInstance<TData>();

            if (!File.Exists(assetPath))
            {
                AssetDatabase.CreateAsset(scriptableObject, assetPath);
                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();
                EditorUtility.FocusProjectWindow();
                Selection.activeObject = scriptableObject;
                LoggerEditor.Debug($"'{filePath}' file created");
            }
        }

        public static void CreateTranslationFile(string language, string dictionaryData, string path = SavvyConstants.TranslationsPath)
        {
            CreateFoldersRecursively(path);
            var filePath = $"{path}/{language}.json";
            var isExists = File.Exists(filePath);
            File.WriteAllText(filePath, dictionaryData);
            AssetDatabase.Refresh();
            LoggerEditor.Debug(isExists ? $"'{filePath}' file updated" : $"'{filePath}' file created");
        }
        
        private static void CreateFoldersRecursively(string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                LoggerEditor.Error($"Folders path is not cannot be null or empty");
                return;
            }
                
            var folders = path.Split('/');
            var currentPath = folders[0];

            for (int i = 1; i < folders.Length; i++)
            {
                var newFolder = folders[i];
                
                if (!AssetDatabase.IsValidFolder($"{currentPath}/{newFolder}")) 
                    CreateFolder(currentPath, newFolder);

                currentPath += $"/{newFolder}";
            }
        }

        private static void CreateFolder(string path, string folder)
        {
            AssetDatabase.CreateFolder(path, folder);
            LoggerEditor.Debug($"'{path}/{folder}' folder created");
        }
    }
}
#endif