﻿#if UNITY_EDITOR
using UnityEditor;

namespace Savvy.Editor
{
    public static class SavvyMenuTools
    {
        [MenuItem(nameof(Savvy) + "/" + SavvyConstants.CreateSavvySettingsFileButton)]
        private static void CreateSavvySettingsFile() => 
            FileSystemEditor.CreateAssetFile<SavvySettings>(SavvyConstants.ResourcesPath);
    }
}
#endif