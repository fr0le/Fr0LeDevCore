﻿#if UNITY_EDITOR
using System.Collections.Generic;
using Savvy.Services.AppMetricaWrapper;
using Savvy.Services.AssetProvider;
using Savvy.Services.CAS;
using Savvy.Services.CoroutineRunner;
using Savvy.Services.CountryChecker;
using Savvy.Services.DateTime;
using Savvy.Services.Factory;
using Savvy.Services.FirebaseApp;
using Savvy.Services.FirebaseRemoteConfig;
using Savvy.Services.IAP;
using Savvy.Services.Localization;
using Savvy.Services.Logger;
using Savvy.Services.Notification;
using Savvy.Services.Numbers;
using Savvy.Services.Preferences;
using Savvy.Services.Randomize;
using Savvy.Services.SaveLoad;
using Savvy.Services.SceneLoader;
using Savvy.Services.Social;
using Savvy.Services.StateMachine;
using Savvy.Services.TenjinWrapper;
using Savvy.Services.UnityGaming;
using Savvy.Services.Vibration;
using Savvy.Services.Yookassa;
using UnityEditor;
using UnityEngine;
using AudioSettings = Savvy.Services.Audio.AudioSettings;

namespace Savvy.Editor
{
    [CustomEditor(typeof(SavvySettings))]
    public class SavvyEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            GUILayout.Space(20);
            var settings = (SavvySettings)target;

            if (GUILayout.Button(SavvyConstants.ResolveButton, GUILayout.Height(30)))
            {
                CreateSettingsFiles(settings);
                ResolveDefineSymbols(settings, BuildTargetGroup.Android);
                ResolveDefineSymbols(settings, BuildTargetGroup.iOS);
            }
        }

        private void CreateSettingsFiles(SavvySettings settings)
        {
            if (settings.AppMetrica)
                FileSystemEditor.CreateAssetFile<AppMetricaSettings>();
            
            if (settings.AssetProvider)
                FileSystemEditor.CreateAssetFile<AssetProviderSettings>();
            
            if (settings.Audio)
                FileSystemEditor.CreateAssetFile<AudioSettings>();
            
            if (settings.CAS)
                FileSystemEditor.CreateAssetFile<CASSettings>();

            if (settings.CoroutineRunner)
                FileSystemEditor.CreateAssetFile<CoroutineRunnerSettings>();

            if (settings.CountryChecker)
                FileSystemEditor.CreateAssetFile<CountryCheckerSettings>();
            
            if (settings.DateTime)
                FileSystemEditor.CreateAssetFile<DateTimeSettings>();
            
            if (settings.Factory)
                FileSystemEditor.CreateAssetFile<FactorySettings>();
            
            if (settings.IAP)
                FileSystemEditor.CreateAssetFile<IAPSettings>();
            
            if (settings.FirebaseApp)
                FileSystemEditor.CreateAssetFile<FirebaseAppSettings>();
            
            if (settings.FirebaseRemoteConfig)
                FileSystemEditor.CreateAssetFile<FirebaseRemoteConfigSettings>();

            if (settings.Localization)
                FileSystemEditor.CreateAssetFile<LocalizationSettings>();
            
            if (settings.Logger)
                FileSystemEditor.CreateAssetFile<LoggerSettings>();
            
            if (settings.Notification)
                FileSystemEditor.CreateAssetFile<NotificationSettings>();
            
            if (settings.Numbers)
                FileSystemEditor.CreateAssetFile<NumbersSettings>();
            
            if (settings.Preferences)
                FileSystemEditor.CreateAssetFile<PreferencesSettings>();
            
            if (settings.Random)
                FileSystemEditor.CreateAssetFile<RandomSettings>();
            
            if (settings.SaveLoad)
                FileSystemEditor.CreateAssetFile<SaveLoadSettings>();
            
            if (settings.SceneLoader)
                FileSystemEditor.CreateAssetFile<SceneLoaderSettings>();
            
            if (settings.Social)
                FileSystemEditor.CreateAssetFile<SocialSettings>();
            
            if (settings.StateMachine)
                FileSystemEditor.CreateAssetFile<StateMachineSettings>();
            
            if (settings.Tenjin)
                FileSystemEditor.CreateAssetFile<TenjinSettings>();
            
            if (settings.UnityGaming)
                FileSystemEditor.CreateAssetFile<UnityGamingSettings>();
            
            if (settings.Vibration)
                FileSystemEditor.CreateAssetFile<VibrationSettings>();
            
            if (settings.Yookassa)
                FileSystemEditor.CreateAssetFile<YookassaSettings>();
        }
        
        private void ResolveDefineSymbols(SavvySettings settings, BuildTargetGroup targetGroup)
        {
            var symbolsList = PlayerSettings.GetScriptingDefineSymbolsForGroup(targetGroup);

            UpdateDefineSymbolsList(settings.AppMetrica, ref symbolsList, SavvyConstants.AppMetricaDefineSymbols, targetGroup);
            UpdateDefineSymbolsList(settings.AssetProvider, ref symbolsList, SavvyConstants.AssetProviderDefineSymbols, targetGroup);
            UpdateDefineSymbolsList(settings.CAS, ref symbolsList, SavvyConstants.CASDefineSymbols, targetGroup);
            UpdateDefineSymbolsList(settings.FirebaseApp, ref symbolsList, SavvyConstants.FirebaseAppDefineSymbols, targetGroup);
            UpdateDefineSymbolsList(settings.FirebaseRemoteConfig, ref symbolsList, SavvyConstants.FirebaseRemoteConfigDefineSymbols, targetGroup);
            UpdateDefineSymbolsList(settings.IAP, ref symbolsList, SavvyConstants.IAPDefineSymbols, targetGroup);
            UpdateDefineSymbolsList(settings.Notification, ref symbolsList, SavvyConstants.NotificationDefineSymbols, targetGroup);
            UpdateDefineSymbolsList(settings.UnityGaming, ref symbolsList, SavvyConstants.UnityGamingDefineSymbols, targetGroup);
            UpdateDefineSymbolsList(settings.Tenjin, ref symbolsList, SavvyConstants.TenjinDefineSymbols, targetGroup);

            PlayerSettings.SetScriptingDefineSymbolsForGroup(targetGroup, symbolsList);
            LoggerEditor.Debug($"Define symbols resolved for '{targetGroup}'");
        }

        private void UpdateDefineSymbolsList(bool isActive, ref string symbolsList, string symbols, BuildTargetGroup targetGroup) => 
            symbolsList = isActive ? AddSymbolToList(symbolsList, symbols, targetGroup) : RemoveSymbolFromList(symbolsList, symbols, targetGroup);

        private static string AddSymbolToList(string symbolsList, string symbols, BuildTargetGroup targetGroup)
        {
            var list = new HashSet<string>(symbolsList.Split(';'));

            if (!list.Contains(symbols))
            {
                list.Add(symbols);
                LoggerEditor.Debug($"'{symbols}' added to 'Scripting Define Symbols' on '{targetGroup}' platform");
            }

            return string.Join(";", list);
        }

        private static string RemoveSymbolFromList(string symbolsList, string symbols, BuildTargetGroup targetGroup)
        {
            var list = new List<string>(symbolsList.Split(';'));

            if (list.Remove(symbols)) 
                LoggerEditor.Debug($"'{symbols}' removed from 'Scripting Define Symbols' on '{targetGroup}' platform");

            return string.Join(";", list);
        }
    }
}
#endif