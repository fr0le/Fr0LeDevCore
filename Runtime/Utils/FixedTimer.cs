﻿using System;
using UnityEngine;

namespace Savvy.Utils
{
    public class FixedTimer
    {
        private float _timer;

        public FixedTimer(float timer = 0) => 
            _timer = timer;

        public void Update(Action<Action<float>> action)
        {
            if (_timer < Time.time)
            {
                _timer = Time.time;
                action?.Invoke(Callback);
            }
        }

        private void Callback(float interval) => 
            _timer += interval;
    }
}