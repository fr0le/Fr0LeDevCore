﻿using TMPro;
using UnityEngine;

namespace Savvy.Utils
{
    [RequireComponent(typeof(TMP_Text))]
    public class FpsCounter : MonoBehaviour
    {
        private TMP_Text _fpsUI;
        private int _fps;
        private int _count;
        private double _startTime;

        private void Awake() => 
            _fpsUI = GetComponent<TMP_Text>();

        private void Update()
        {
            ++_count;

            if (Time.time >= _startTime + 1)
            {
                _fps = _count;
                _startTime = Time.time;
                _count = 0;
            }

            _fpsUI.text = $"fps: {_fps}";
        }
    }
}