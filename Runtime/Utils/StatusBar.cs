﻿using System;
using UnityEngine;

namespace Savvy.Utils
{
    public class StatusBar : MonoBehaviour
    {
        private void Start()
        {
            switch (Application.platform)
            {
                case RuntimePlatform.IPhonePlayer:
#if UNITY_IPHONE
                    Debug.LogError("Need StatusBar IPhonePlayer Logic");
#endif
                    break;
                case RuntimePlatform.Android:
#if UNITY_ANDROID
                    SetBarColor(Color.clear, "setStatusBarColor");
                    ClearFlags(1024);
#endif
                    break;
            }
        }

        private void SetBarColor(Color color, string methodName) => 
            RunOnUiThread(() =>
                GetWindow().Call(methodName, ColorToARGB(color)));

        private void ClearFlags(int flags) => 
            RunOnUiThread(() =>
                GetWindow().Call("clearFlags", flags));

        private AndroidJavaObject GetWindow()
        {
            var unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            var currentActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
            return currentActivity.Call<AndroidJavaObject>("getWindow");
        }

        private void RunOnUiThread(Action action)
        {
            var unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            var currentActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
            currentActivity.Call("runOnUiThread", new AndroidJavaRunnable(action));
        }

        private int ColorToARGB(Color32 color) => 
            (color.a << 24) | (color.r << 16) | (color.g << 8) | color.b;
    }
}