﻿using System;

namespace Savvy.Services.Mediation
{
    [Serializable]
    public struct BannerAdData
    {
        public int Count;
    }
}