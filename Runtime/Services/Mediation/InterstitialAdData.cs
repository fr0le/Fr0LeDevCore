﻿using System;

namespace Savvy.Services.Mediation
{
    [Serializable]
    public struct InterstitialAdData
    {
        public int Count;
    }
}