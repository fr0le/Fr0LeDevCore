using System;

namespace Savvy.Services.Mediation
{
    public interface IMediationService : ISavvyService
    {
        event Action OnBannerAdLoaded;
        event Action<bool> OnInterstitialAdLoaded;
        event Action<bool> OnRewardedAdLoaded;
        bool BannerAdIsReady { get; }
        bool InterstitialAdIsReady { get; }
        bool RewardedAdIsReady { get; }
        void SetInterstitialInterval(int interval);
        void ShowBannerAd();
        void HideBannerAd();
        void ShowInterstitialAd(string placement = "unknown");
        void ShowRewardedAd(Action rewardedAdCompleted = null, Action rewardedAdClosed = null, string placement = "unknown");
    }
}