﻿namespace Savvy.Services
{
    public interface ILateTickable
    {
        void LateTick();
    }
}