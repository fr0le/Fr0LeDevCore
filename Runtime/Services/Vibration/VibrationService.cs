﻿using System.Linq;
using Savvy.Extensions;
using Savvy.Services.Preferences;
using UnityEngine;

namespace Savvy.Services.Vibration
{
    public class VibrationService : ServiceBase, IVibrationService
    {
        private readonly string _sourceSettings = $"{nameof(Savvy)}/{nameof(VibrationSettings)}";
        private readonly VibrationSettings _settings;
       
        private IPreferencesService _preferences;
        private bool _isEnable;

#if UNITY_ANDROID
        private const string VibrateAndroid = "vibrate";
        private const string CancelAndroid = "cancel";
        
        private AndroidJavaClass _unityPlayer;
        private AndroidJavaObject _currentActivity;
        private AndroidJavaObject _vibrator;
#endif

        public VibrationService() => 
            _settings = _sourceSettings.LoadResources<VibrationSettings>();

        public override void Inject() => 
            _preferences = GetService<IPreferencesService>();

        public override void CheckSettings()
        {
            if (_settings.VibrationData == null || _settings.VibrationData.Length == 0)
                Error($"'{nameof(_settings.VibrationData)}' cannot be null or empty. Correct the settings file '{_sourceSettings}'");
        }

        public override void Init()
        {
            _isEnable = _preferences.LoadBool(_settings.PrefsKey, _settings.IsEnableDefault);

            switch (Application.platform)
            {
                case RuntimePlatform.IPhonePlayer:
#if UNITY_IPHONE
                    Error("Need Vibrate IPhonePlayer Logic");
#endif
                    break;
                case RuntimePlatform.Android:
#if UNITY_ANDROID
                    _unityPlayer = new("com.unity3d.player.UnityPlayer");
                    _currentActivity = _unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
                    _vibrator = _currentActivity.Call<AndroidJavaObject>("getSystemService", "vibrator");
#endif
                    break;
            }
        }

        public void Vibrate(VibrationType type)
        {
            if (!_isEnable)
                return;
            
            switch (Application.platform)
            {
                case RuntimePlatform.IPhonePlayer:
#if UNITY_IPHONE
                    Error("Need Vibrate IPhonePlayer Logic");
#endif
                    break;

                case RuntimePlatform.Android:
#if UNITY_ANDROID
                    _vibrator.Call(VibrateAndroid, GetMilliseconds(type));
#endif
                    break;
            }
            
            Debug($"Start vibration '{type}' on platform '{Application.platform}'", _settings.Debug);
        }

        public void Cancel()
        {
            if (!_isEnable)
                return;
            
            switch (Application.platform)
            {
                case RuntimePlatform.IPhonePlayer:
#if UNITY_IPHONE
                    Error("Need Vibrate IPhonePlayer Logic");
#endif
                    break;

                case RuntimePlatform.Android:
#if UNITY_ANDROID
                    _vibrator.Call(CancelAndroid);
#endif
                    break;
            }
            
            Debug($"Cancel vibration on platform '{Application.platform}'", _settings.Debug);
        }

        private long GetMilliseconds(VibrationType type) => 
            (from vibration in _settings.VibrationData where type == vibration.Type select vibration.Milliseconds).FirstOrDefault();
    }
}