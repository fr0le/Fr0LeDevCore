﻿using Savvy.Infrastructure;
using UnityEngine;

namespace Savvy.Services.Vibration
{
    [CreateAssetMenu(menuName = nameof(Savvy) + "/" + nameof(VibrationSettings), fileName = nameof(VibrationSettings))]
    public class VibrationSettings : ValidateScriptableObject, ISavvySettings
    {
        [Header("Logging")]
        public bool Debug = false;

        [Header("Save Load")]
        public string PrefsKey = "VibrationSettings";

        [Header("Settings")]
        public bool IsEnableDefault = true; 
        public VibrationData[] VibrationData = 
        {
            new()
            {
                Type = VibrationType.VerySoft,
                Milliseconds = 50
            },
            new()
            {
                Type = VibrationType.Soft,
                Milliseconds = 100
            },
            new()
            {
                Type = VibrationType.Medium,
                Milliseconds = 250
            },
            new()
            {
                Type = VibrationType.Hard,
                Milliseconds = 500
            },
            new()
            {
                Type = VibrationType.VeryHard,
                Milliseconds = 1000
            }
        };
    }
}