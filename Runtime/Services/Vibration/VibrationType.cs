﻿namespace Savvy.Services.Vibration
{
    public enum VibrationType
    {
        VerySoft = 0,
        Soft = 1,
        Medium = 2,
        Hard = 3,
        VeryHard = 4
    }
}