﻿namespace Savvy.Services.Vibration
{
    public interface IVibrationService : ISavvyService
    {
        void Vibrate(VibrationType type);
        void Cancel();
    }
}