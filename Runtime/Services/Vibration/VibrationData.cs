﻿using System;
using UnityEngine;

namespace Savvy.Services.Vibration
{
    [Serializable]
    public struct VibrationData
    {
        public VibrationType Type;
        [Min(0)]
        public long Milliseconds;
    }
}