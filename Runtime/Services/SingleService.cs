﻿namespace Savvy.Services
{
    public static class SingleService<TService> where TService : IService
    {
        public static TService Instance;

        public static void Remove() => 
            Instance = default;
    }
}