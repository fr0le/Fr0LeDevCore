﻿namespace Savvy.Services.Audio
{
    public interface IAudioService : ISavvyService
    { 
        void ChangeAudio(AudioMixerType type, float volume);
    }
}