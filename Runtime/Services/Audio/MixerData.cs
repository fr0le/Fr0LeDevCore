using System;
using UnityEngine;

namespace Savvy.Services.Audio
{
    [Serializable]
    public struct MixerData
    {
        public AudioMixerType Type;
        [Range(-80, 20)]
        public float Default;
    }
}