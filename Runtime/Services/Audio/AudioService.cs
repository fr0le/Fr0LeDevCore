﻿using System.Linq;
using Savvy.Extensions;
using Savvy.Services.Preferences;
using UnityEngine;

namespace Savvy.Services.Audio
{
    public class AudioService : ServiceBase, IAudioService
    {
        private readonly string _sourceSettings = $"{nameof(Savvy)}/{nameof(AudioSettings)}";
        private readonly AudioSettings _settings;

        private IPreferencesService _preferences;

        public AudioService() =>
            _settings = _sourceSettings.LoadResources<AudioSettings>();

        public override void Inject() =>
            _preferences = GetService<IPreferencesService>();

        public override void CheckSettings()
        {
            if (string.IsNullOrEmpty(_settings.PrefsKey))
                Error($"'{nameof(_settings.PrefsKey)}' cannot be null or empty. Correct the settings file '{_sourceSettings}'");

            if (_settings.Mixer == null)
                Error($"'{nameof(_settings.Mixer)}' cannot be null or empty. Correct the settings file '{_sourceSettings}'");

            if (_settings.MixerData == null || _settings.MixerData.Length == 0)
                Error($"'{nameof(_settings.MixerData)}' cannot be null or empty. Correct the settings file '{_sourceSettings}'");
        }

        public override void Init()
        {
            foreach (var mixer in _settings.MixerData)
                _settings.Mixer.audioMixer.SetFloat($"{mixer.Type}", GetVolume(mixer));
        }

        public void ChangeAudio(AudioMixerType type, float value)
        {
            var mixer = GetMixerData(type);
            var volume = value == 0 ? _settings.Mute : Mathf.Lerp(_settings.MinValue, _settings.MaxValue, value);
            SaveVolume(mixer, volume);
            Debug($"Change audio '{mixer}', value '{value}'", _settings.Debug);
        }

        private float GetVolume(MixerData mixer)
        {
            var key = $"{_settings.PrefsKey}{mixer.Type}";
            return _preferences.LoadFloat(key, mixer.Default);
        }

        private void SaveVolume(MixerData mixer, float volume)
        {
            _settings.Mixer.audioMixer.SetFloat($"{mixer.Type}", volume);
            var key = $"{_settings.PrefsKey}{mixer.Type}";
            _preferences.SaveFloat(key, volume);
        }

        private MixerData GetMixerData(AudioMixerType type) =>
            _settings.MixerData.FirstOrDefault(mixer => type == mixer.Type);
    }
}