namespace Savvy.Services.Audio
{
    public enum AudioMixerType
    {
        Master = 0,
        Music = 1,
        SoundFx = 2,
        UI = 3
    }
}