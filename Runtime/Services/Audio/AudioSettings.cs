﻿using Savvy.Infrastructure;
using UnityEngine;
using UnityEngine.Audio;

namespace Savvy.Services.Audio
{
    [CreateAssetMenu(menuName = nameof(Savvy) + "/" + nameof(AudioSettings), fileName = nameof(AudioSettings))]
    public class AudioSettings : ValidateScriptableObject, ISavvySettings
    {
        [Header("Logging")]
        public bool Debug = false;
        
        [Header("Save Load")]
        public string PrefsKey = "AudioSettings";
        
        [Header("Settings")]
        public AudioMixerGroup Mixer;
        [Range(-80, 20)]
        public float Mute = -80;
        [Range(-80, 20)]
        public float MinValue = -60;
        [Range(-80, 20)]
        public float MaxValue = 0;
        public MixerData[] MixerData =
        {
            new()
            {
                Type = AudioMixerType.Master,
                Default = 0
            },
            new()
            {
                Type = AudioMixerType.Music,
                Default = -10
            },
            new()
            {
                Type = AudioMixerType.SoundFx,
                Default = -5
            },
            new()
            {
                Type = AudioMixerType.UI,
                Default = -5
            }
        };
    }
}