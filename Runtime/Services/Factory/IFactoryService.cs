using UnityEngine;

namespace Savvy.Services.Factory
{
    public interface IFactoryService : ISavvyService
    {
        TObject Instantiate<TObject>(TObject original) where TObject : Object;
        TObject Instantiate<TObject>(TObject original, Transform parent) where TObject : Object;
        TObject Instantiate<TObject>(TObject original, Vector3 position, Quaternion rotation) where TObject : Object;
    }
}