﻿using Savvy.Infrastructure;
using UnityEngine;

namespace Savvy.Services.Factory
{
    [CreateAssetMenu(menuName = nameof(Savvy) + "/" + nameof(FactorySettings), fileName = nameof(FactorySettings))]
    public class FactorySettings : ValidateScriptableObject, ISavvySettings
    {
        [Header("Logging")] 
        public bool Debug = false;
    }
}