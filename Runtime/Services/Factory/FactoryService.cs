﻿using Savvy.Extensions;
using UnityEngine;

namespace Savvy.Services.Factory
{
    public class FactoryService : ServiceBase, IFactoryService
    {
        private readonly string _sourceSettings = $"{nameof(Savvy)}/{nameof(FactorySettings)}";
        private readonly FactorySettings _settings;

        public FactoryService() => 
            _settings = _sourceSettings.LoadResources<FactorySettings>();

        public TObject Instantiate<TObject>(TObject original) where TObject : Object
        {
            var newObject = Object.Instantiate(original);
            Debug($"Instantiate '{original.GetType().Name}'", _settings.Debug);
            return newObject;
        }
        
        public TObject Instantiate<TObject>(TObject original, Transform parent) where TObject : Object
        {
            var newObject = Object.Instantiate(original, parent);
            Debug($"Instantiate '{original.GetType().Name}' on '{parent.position}'", _settings.Debug);
            return newObject;
        }

        public TObject Instantiate<TObject>(TObject original, Vector3 position, Quaternion rotation) where TObject : Object
        {
            var newObject = Object.Instantiate(original, position, rotation);
            Debug($"Instantiate '{original.GetType().Name}' on '{position}'", _settings.Debug);
            return newObject;
        }
    }
}