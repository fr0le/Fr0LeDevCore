﻿namespace Savvy.Services
{
    public enum ServiceType
    {
        Unknown = 0,
        Project = 1,
        Scene = 2
    }
}