﻿namespace Savvy.Services
{
    public interface IFixedTickable
    {
        void FixedTick();
    }
}