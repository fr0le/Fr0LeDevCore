using System;

namespace Savvy.Services.Yookassa
{
    [Serializable]
    public struct YookassaConvertData
    {
        public string Id;
        public decimal Price;
        public string Title;
    }
}