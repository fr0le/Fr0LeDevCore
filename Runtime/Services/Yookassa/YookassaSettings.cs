using Savvy.Infrastructure;
using UnityEngine;

namespace Savvy.Services.Yookassa
{
    [CreateAssetMenu(menuName = nameof(Savvy) + "/" + nameof(YookassaSettings), fileName = nameof(YookassaSettings))]
    public class YookassaSettings : ValidateScriptableObject, ISavvySettings
    {
        [Header("Logging")] 
        public bool Debug = false;
        
        [Header("Settings")]
        public string ServerDomain;
        public bool IsHttps;
        public string ShopId;
        public string DeepLink;
        public string SupportChatUrl;

        [Header("Analytics")]
        public bool SendPurchaseToAppMetrica = false;
    }
}