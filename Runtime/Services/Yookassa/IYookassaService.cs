using System;

namespace Savvy.Services.Yookassa
{
    public interface IYookassaService : ISavvyService
    {
        void InitiatePurchase(Enum id, Action purchaseSuccessful);
        void InitiatePurchase(string id, Action purchaseSuccessful);
        ProductData GetProductData(Enum id);
        ProductData GetProductData(string id);
    }
}