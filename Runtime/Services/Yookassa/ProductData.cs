﻿using System;

namespace Savvy.Services.Yookassa
{
    [Serializable]
    public struct ProductData
    {
        public string Id;
        public decimal Price;
        public string Title;
    }
}