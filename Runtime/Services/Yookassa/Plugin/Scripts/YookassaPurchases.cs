using System;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Savvy.Services.Yookassa.Plugin
{
    public class YookassaPurchases : IDisposable
    {
        public string CurrencyCode => _yookassaClient.CurrencyCode;
        public Purchase CurrentPurchase { get; private set; }
        private Action<bool> _result;
        private readonly YookassaClient _yookassaClient;
        private readonly YookassaView _yookassaView;
        private readonly string _supportChatUrl;

        public YookassaPurchases(YookassaClient yookassaClient, string supportChatUrl)
        {
            if (string.IsNullOrEmpty(supportChatUrl)) throw new ArgumentNullException(nameof(supportChatUrl));

            _yookassaClient = yookassaClient ?? throw new ArgumentNullException(nameof(yookassaClient));

            var yookassaViewPrefab = Resources.Load<YookassaView>("YookassaView");
            if (yookassaViewPrefab == null) throw new InvalidOperationException("Can't load YookassaView prefab. Make sure that file Resources/YookassaView.prefab exists.");

            _yookassaView = Object.Instantiate(yookassaViewPrefab, null);
            _yookassaView.gameObject.name = "[YookassaView]";
            Object.DontDestroyOnLoad(_yookassaView.gameObject);
            _yookassaView.Hide();

            _supportChatUrl = supportChatUrl;

            Application.focusChanged += OnFocusChanged;
        }

        public void Dispose()
        {
            Application.focusChanged -= OnFocusChanged;
            if (_yookassaView != null)
            {
                Object.Destroy(_yookassaView.gameObject);
            }
        }

        public void Purchase(Purchase purchase, Action<bool> result)
        {
            if (CurrentPurchase != null)
            {
                Debug.LogWarning($"Purchase already in progress: {CurrentPurchase.Id}. Can't start new purchase.");
                result?.Invoke(false);
                return;
            }

            if (purchase == null)
            {
                Debug.LogWarning($"Purchase is null. Can't start new purchase.");
                result?.Invoke(false);
                return;
            }

            if (string.IsNullOrEmpty(purchase.Id) || string.IsNullOrEmpty(purchase.Amount) || string.IsNullOrEmpty(purchase.Description))
            {
                Debug.LogWarning($"Purchase is invalid. Can't start new purchase.");
                result?.Invoke(false);
                return;
            }

            CurrentPurchase = purchase;
            _result = result;

            var url = _yookassaClient.GetPaymentUrl(CurrentPurchase);
            _yookassaView.SetPaymentIdText(CurrentPurchase.Id);
            _yookassaView.ShowLoading("Открытие страницы оплаты...");
            Application.OpenURL(url);
        }

        private async void OnFocusChanged(bool focus)
        {
            if (!focus || CurrentPurchase == null) return;

            _yookassaView.ShowLoading("Проверка оплаты...");
            PurchaseStatus status = await _yookassaClient.GetPurchaseStatus(CurrentPurchase);
            OnCurrentPurchaseStatusChanged(status);
        }

        private async void OnCurrentPurchaseStatusChanged(PurchaseStatus status)
        {
            UpdateUI(status);
            switch (status)
            {
                case PurchaseStatus.Unknown:
                    _result?.Invoke(false);
                    ResetCurrentYookassaPurchase();
                    break;
                case PurchaseStatus.Pending:
                    break;
                case PurchaseStatus.WaitingForCapture:
                    PurchaseStatus captureStatus = await _yookassaClient.CapturePurchase(CurrentPurchase);
                    OnCurrentPurchaseStatusChanged(captureStatus);
                    break;
                case PurchaseStatus.Succeeded:
                    _result?.Invoke(true);
                    ResetCurrentYookassaPurchase();
                    break;
                case PurchaseStatus.Canceled:
                    _result?.Invoke(false);
                    ResetCurrentYookassaPurchase();
                    break;
            }
        }

        private void UpdateUI(PurchaseStatus status)
        {
            if (_yookassaView == null)
            {
                Debug.LogWarning("Can't update UI. YookassaView is null");
                return;
            }

            switch (status)
            {
                case PurchaseStatus.Unknown:
                    var errorText = "Произошла ошибка. Если вы оплатили покупку, деньги вернутся автоматически в течение 7 дней.";
                    _yookassaView.ShowError(errorText, "Чат поддержки", () => Application.OpenURL(_supportChatUrl));
                    break;
                case PurchaseStatus.Pending:
                    var purchaseUrl = _yookassaClient.GetPaymentUrl(CurrentPurchase);
                    _yookassaView.ShowPending(purchaseUrl, CancelCurrentPurchase);
                    break;
                case PurchaseStatus.WaitingForCapture:
                    _yookassaView.ShowLoading("Подтверждение оплаты...");
                    break;
                case PurchaseStatus.Succeeded:
                    _yookassaView.ShowSucceeded();
                    break;
                case PurchaseStatus.Canceled:
                    _yookassaView.ShowError("Оплата отменена.", "Чат поддержки", () => Application.OpenURL(_supportChatUrl));
                    break;
            }
        }

        private void CancelCurrentPurchase()
        {
            _result?.Invoke(false);
            ResetCurrentYookassaPurchase();
        }

        private void ResetCurrentYookassaPurchase()
        {
            CurrentPurchase = null;
            _result = null;
        }
    }
}