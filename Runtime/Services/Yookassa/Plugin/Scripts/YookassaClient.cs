using System;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Networking;

namespace Savvy.Services.Yookassa.Plugin
{
    public class YookassaClient
    {
        /// <summary>
        /// Currency code for payment in ISO 4217 format
        /// </summary>
        public string CurrencyCode { get; }
        /// <summary>
        /// Id of the shop in Yookassa
        /// </summary>
        public string ShopId { get; }
        private readonly string _serverDomain;
        private readonly string _deepLink;
        private readonly string _protocol;
        private readonly bool _log;
        private const string LOG_PREFIX = "<color=cyan>[YookassaClient]: </color>";

        public YookassaClient(string serverDomain, string shopId, string deepLink, string currencyCode = "RUB", bool https = false, bool log = false)
        {
            if (string.IsNullOrEmpty(serverDomain)) throw new ArgumentNullException(nameof(serverDomain));
            if (string.IsNullOrEmpty(shopId)) throw new ArgumentNullException(nameof(shopId));
            if (string.IsNullOrEmpty(deepLink)) throw new ArgumentNullException(nameof(deepLink));
            if (shopId.Length != 6) throw new ArgumentException("ShopId must be 6 characters long. Example: 123456");
            foreach (var c in shopId) if (!char.IsDigit(c)) throw new ArgumentException("ShopId must contain only digits. Example: 123456");
            if (string.IsNullOrEmpty(currencyCode)) throw new ArgumentException("Currency must be specified. ISO 4217");
            if (currencyCode.Length != 3) throw new ArgumentException("Currency must be 3 characters long. ISO 4217");
            foreach (var c in currencyCode) if (!char.IsLetter(c)) throw new ArgumentException("Currency must contain only letters. ISO 4217");

            _serverDomain = serverDomain;
            ShopId = shopId;
            CurrencyCode = currencyCode.ToUpper();
            _deepLink = deepLink;
            _protocol = https ? "https://" : "http://";
            _log = log;

            if (_log) Debug.Log($"{LOG_PREFIX}Created Client: ServerDomain={_serverDomain}, ShopId={ShopId}, DeepLink={_deepLink}, Currency={CurrencyCode}, Protocol={_protocol}");
        }

        public string GetPaymentUrl(Purchase purchase)
        {
            var url = $"{_protocol}{_serverDomain}/v1/pay?amount={purchase.Amount}&currency={CurrencyCode}&purchase_id={purchase.Id}&deeplink_link={_deepLink}&description={purchase.Description}&shop_id={ShopId}";

            if (_log) Debug.Log($"{LOG_PREFIX}GetPaymentUrl <- {url}");

            return url;
        }

        public async Task<PurchaseStatus> GetPurchaseStatus(Purchase purchase)
        {
            var url = $"{_protocol}{_serverDomain}/v1/check?purchase_id={purchase.Id}&shop_id={ShopId}";

            if (_log) Debug.Log($"{LOG_PREFIX}Start {nameof(GetPurchaseStatus)}");

            StatusResponce responce;

            try
            {
                var json = await GetTextAsync(url);
                responce = JsonUtility.FromJson<StatusResponce>(json);
            }
            catch (Exception e)
            {
                Debug.LogError(e);
                return PurchaseStatus.Unknown;
            }

            PurchaseStatus status = StringToPurchaseStatus(responce.status);

            if (_log) Debug.Log($"{LOG_PREFIX}End {nameof(GetPurchaseStatus)} <- {status}");

            return status;
        }

        public async Task<PurchaseStatus> CapturePurchase(Purchase purchase)
        {
            var url = $"{_protocol}{_serverDomain}/v1/capture?purchase_id={purchase.Id}&amount={purchase.Amount}&currency={CurrencyCode}&shop_id={ShopId}";

            if (_log) Debug.Log($"{LOG_PREFIX}Start {nameof(CapturePurchase)}");

            StatusResponce responce;

            try
            {
                var json = await GetTextAsync(url);
                responce = JsonUtility.FromJson<StatusResponce>(json);
            }
            catch (Exception e)
            {
                Debug.LogError(e);
                return PurchaseStatus.Unknown;
            }

            PurchaseStatus status = StringToPurchaseStatus(responce.status);

            if (_log) Debug.Log($"{LOG_PREFIX}End {nameof(CapturePurchase)} <- {status}");

            return status;
        }

        private async Task<string> GetTextAsync(string url)
        {
            if (string.IsNullOrEmpty(url)) throw new ArgumentNullException(nameof(url));

            UnityWebRequest request = UnityWebRequest.Get(url);
            request.SendWebRequest();

            if (_log) Debug.Log($"{LOG_PREFIX}HTTP Request: {request.url}");

            while (!request.isDone) await Task.Yield();

            if (request.result == UnityWebRequest.Result.Success)
            {
                if (_log) Debug.Log($"{LOG_PREFIX}HTTP Response: {request.downloadHandler.text}");

                return request.downloadHandler.text;
            }
            else
            {
                throw new Exception(request.error);
            }
        }

        private PurchaseStatus StringToPurchaseStatus(string status)
        {
            return status switch
            {
                "pending" => PurchaseStatus.Pending,
                "waiting_for_capture" => PurchaseStatus.WaitingForCapture,
                "succeeded" => PurchaseStatus.Succeeded,
                "canceled" => PurchaseStatus.Canceled,
                _ => PurchaseStatus.Unknown
            };
        }
    }
}