namespace Savvy.Services.Yookassa.Plugin
{
    public enum PurchaseStatus
    {
        Unknown = 0,
        Pending = 1,
        WaitingForCapture = 2,
        Succeeded = 3,
        Canceled = 4,
    }
}