using System;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Savvy.Services.Yookassa.Plugin
{
    public class YookassaView : MonoBehaviour
    {
        [SerializeField] private TMP_Text _statusText;
        [SerializeField] private TMP_Text _paymentIdText;
        [SerializeField] private Button _actionButton;
        [SerializeField] private TMP_Text _actionButtonText;
        [SerializeField] private Button _cancelButton;
        [SerializeField] private Button _paymentIdButton;
        [SerializeField] private GameObject _copyImage;
        private Action _action;
        private Action _cancelAction;

        public void SetPaymentIdText(string text)
        {
            if (string.IsNullOrEmpty(text))
            {
                _paymentIdText.gameObject.SetActive(false);
                _copyImage.SetActive(false);
                return;
            }

            _paymentIdText.text = text;
            _paymentIdText.gameObject.SetActive(true);
            _copyImage.SetActive(true);
        }

        public void ShowLoading(string loadingText = "Загрузка...")
        {
            _statusText.text = loadingText;
            _actionButton.gameObject.SetActive(false);
            _cancelButton.gameObject.SetActive(false);
            gameObject.SetActive(true);
        }

        public void ShowError(string errorText, string actionButtonText, Action onActionButtonClicked)
        {
            _statusText.text = errorText;
            _actionButtonText.text = actionButtonText;
            _action = () => onActionButtonClicked?.Invoke();
            _actionButton.gameObject.SetActive(true);
            _cancelAction = () => Hide();
            _cancelButton.gameObject.SetActive(true);
            gameObject.SetActive(true);
        }

        public void ShowPending(string purchaseUrl, Action onCancelButtonClicked)
        {
            _statusText.text = "Ожидание оплаты.";
            _actionButtonText.text = "Оплатить";
            _action = () => Application.OpenURL(purchaseUrl);
            _actionButton.gameObject.SetActive(true);
            _cancelAction = () =>
            {
                Hide();
                onCancelButtonClicked?.Invoke();
            };
            _cancelButton.gameObject.SetActive(true);
            gameObject.SetActive(true);
        }

        public void ShowSucceeded()
        {
            _statusText.text = "Оплата прошла успешно. Спасибо за покупку!";
            _actionButtonText.text = "Закрыть";
            _action = () => Hide();
            _actionButton.gameObject.SetActive(true);
            _cancelButton.gameObject.SetActive(false);
            gameObject.SetActive(true);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }

        private void OnEnable()
        {
            _actionButton.onClick.AddListener(() => _action?.Invoke());
            _cancelButton.onClick.AddListener(() => _cancelAction?.Invoke());
            _paymentIdButton.onClick.AddListener(CopyIdToClipboard);
        }

        private void OnDisable()
        {
            _actionButton.onClick.RemoveAllListeners();
            _cancelButton.onClick.RemoveAllListeners();
            _paymentIdButton.onClick.RemoveAllListeners();
        }

        private async void CopyIdToClipboard()
        {
            var id = _paymentIdText.text;
            _paymentIdButton.interactable = false;
            GUIUtility.systemCopyBuffer = id;
            _copyImage.SetActive(false);
            _paymentIdText.text = "Скопировано в буфер обмена.";
            await Task.Delay(1500);
            _paymentIdText.text = id;
            _paymentIdButton.interactable = true;
            _copyImage.SetActive(true);
        }
    }
}