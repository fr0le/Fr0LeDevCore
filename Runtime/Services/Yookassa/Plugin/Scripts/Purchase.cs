using System;
using System.Globalization;
using UnityEngine;

namespace Savvy.Services.Yookassa.Plugin
{
    public class Purchase
    {
        /// <summary>
        /// Amount of money (Currency defined in YookassaClient constructor)
        /// </summary>
        public string Amount { get; }
        /// <summary>
        /// Id of the purchase (Can be found in Yookassa dashboard as part of description)
        /// </summary>
        public string Id { get; }
        /// <summary>
        /// Description of the purchase (Can be found in Yookassa dashboard)
        /// </summary>
        public string Description { get; }
        private const int MAX_DESCRIPTION_LENGTH = 128;

        public Purchase(decimal amount, string description = null)
        {
            if (amount <= 0) throw new ArgumentException("Amount must be greater than zero");

            Amount = amount.ToString("0.00", CultureInfo.InvariantCulture); // Yookassa accepts only 2 decimal places with dot as decimal separator
            Id = Guid.NewGuid().ToString();

            var descriptionPrefix = $"{Id}, ";
            Description = descriptionPrefix + description;

            if (Description.Length > MAX_DESCRIPTION_LENGTH)
            {
                var maxDescriptionLength = MAX_DESCRIPTION_LENGTH - descriptionPrefix.Length;
                Debug.LogWarning($"Description must be less than or equal to {maxDescriptionLength} characters long! Descripton will be truncated!");
                Description = Description[..maxDescriptionLength];
            }
        }
    }
}