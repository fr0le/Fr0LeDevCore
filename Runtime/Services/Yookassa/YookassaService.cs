﻿using System;
using Savvy.Extensions;
using Savvy.Services.AppMetricaWrapper;
using Savvy.Services.Yookassa.Plugin;

namespace Savvy.Services.Yookassa
{
    public class YookassaService : ServiceBase, IYookassaService
    {
        private readonly string _sourceSettings = $"{nameof(Savvy)}/{nameof(YookassaSettings)}";
        private readonly YookassaSettings _settings;
        private readonly YookassaConvertData[] _products;

        private IAppMetricaService _appMetrica;
        private YookassaPurchases _yookassaPurchases;

        public YookassaService(YookassaConvertData[] products)
        {
            _products = products;
            _settings = _sourceSettings.LoadResources<YookassaSettings>();
        }

        public override void Inject() => 
            _appMetrica = GetService<IAppMetricaService>();

        public override void CheckSettings()
        {
            if (string.IsNullOrEmpty(_settings.ServerDomain))
                Error($"'{nameof(_settings.ServerDomain)}' cannot be null or empty. Correct the settings file '{_sourceSettings}'");
            
            if (string.IsNullOrEmpty(_settings.ShopId))
                Error($"'{nameof(_settings.ShopId)}' cannot be null or empty. Correct the settings file '{_sourceSettings}'");
            
            if (string.IsNullOrEmpty(_settings.DeepLink))
                Error($"'{nameof(_settings.DeepLink)}' cannot be null or empty. Correct the settings file '{_sourceSettings}'");
            
            if (string.IsNullOrEmpty(_settings.SupportChatUrl))
                Error($"'{nameof(_settings.SupportChatUrl)}' cannot be null or empty. Correct the settings file '{_sourceSettings}'");
        }

        public override void Init()
        {
            var yookassaClient = new YookassaClient(_settings.ServerDomain, _settings.ShopId, _settings.DeepLink, https: _settings.IsHttps, log: true);
            _yookassaPurchases = new YookassaPurchases(yookassaClient, _settings.SupportChatUrl);
        }

        public void InitiatePurchase(Enum id, Action purchaseSuccessful) => 
            InitiatePurchase(id.ToString(), purchaseSuccessful);

        public void InitiatePurchase(string id, Action purchaseSuccessful)
        {
            foreach (var iap in _products)
            {
                if (iap.Id == id)
                {
                    Debug($"Try to purchase product '{id}'", _settings.Debug);
                    var purchase = GetPurchase(id);
                    
                    _yookassaPurchases.Purchase(purchase, success =>
                    {
                        if (success)
                        {
                            Debug($"Purchase complete - product '{purchase.Id}'", _settings.Debug);
                            purchaseSuccessful?.Invoke();
                            
                            if (_settings.SendPurchaseToAppMetrica)
                                _appMetrica.SendEvent(GetProductData(id), "YookassaPurchase_Successful");
                        }
                        else
                        {
                            Error($"Purchase failed - product '{purchase.Id}'");
                            
                            if (_settings.SendPurchaseToAppMetrica)
                                _appMetrica.SendEvent(GetProductData(id), "YookassaPurchase_Failed");
                        }
                    });
                    
                    break;
                }
            }
        }

        public ProductData GetProductData(Enum id) =>
            GetProductData(id.ToString());
        
        public ProductData GetProductData(string id)
        {
            var product = GetProduct(id);
                
            var productData = new ProductData
            {
                Price = product.Price,
                Title = product.Title
            };
            
            return productData;
        }

        private Purchase GetPurchase(string id)
        {
            var product = GetProduct(id);
            return new Purchase(product.Price, product.Title);
        }

        private YookassaConvertData GetProduct(string id)
        {
            foreach (var product in _products)
            {
                if (product.Id.Contains(id))
                    return product;
            }

            return new();
        }
    }
}