﻿using System;
using Savvy.Extensions;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.Purchasing.Extension;
#if SAVVY_UNITY_GAMING && SAVVY_IAP
using UnityEngine.Purchasing;
using UnityEngine.Purchasing.Extension;
#endif

namespace Savvy.Services.IAP
{
    public class IAPService : ServiceBase, IIAPService
#if SAVVY_UNITY_GAMING && SAVVY_IAP
        ,IDetailedStoreListener
#endif
    {
        private readonly string _sourceSettings = $"{nameof(Savvy)}/{nameof(IAPSettings)}";
        private readonly IAPSettings _settings;
        private readonly IAPConvertData[] _products;
        
        //отрефакторить
        public event Action<PurchaseEventArgs> OnProcessPurchase;

#if SAVVY_UNITY_GAMING && SAVVY_IAP
        private IStoreController _storeController;
        private IGooglePlayStoreExtensions _googlePlayStoreExtensions;
        private IAppleExtensions _appleExtensions;
        

#endif

        private Action _purchaseSuccessful;
        private Action _restoreSuccessful;
        private Action _restoreFailed;

        public IAPService(IAPConvertData[] products)
        {
            _products = products;
            _settings = _sourceSettings.LoadResources<IAPSettings>();
        }

#if SAVVY_UNITY_GAMING && SAVVY_IAP
        public override void Init()
        {
            var instance = StandardPurchasingModule.Instance();
            instance.useFakeStoreUIMode = FakeStoreUIMode.StandardUser;
            var builder = ConfigurationBuilder.Instance(instance);

            foreach (var iap in _products)
            {
                builder.AddProduct(iap.Id, iap.Type.ToEnumOrDefault<ProductType>());
                Debug($"Add '{iap.Type}' in-app '{iap.Id}'", _settings.Debug);
            }
            
            UnityPurchasing.Initialize(this, builder);
        }
        
        public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
        {
            _storeController = controller;

            switch (Application.platform)
            {
                case RuntimePlatform.Android:
                    _googlePlayStoreExtensions = extensions.GetExtension<IGooglePlayStoreExtensions>();
                    break;
                case RuntimePlatform.IPhonePlayer:
                    _appleExtensions = extensions.GetExtension<IAppleExtensions>();
                    break;
                default:
                    DebugNotSupport();
                    break;
            }
            
            Debug("In-App Purchasing successfully initialized", _settings.Debug);
        }
        
        public void OnInitializeFailed(InitializationFailureReason error) => 
            OnInitializeFailed(error, null);

        public void OnInitializeFailed(InitializationFailureReason error, string message)
        {
            var errorMessage = $"Purchasing failed to initialize. Reason: {error}. ";

            if (message != null) 
                errorMessage += $"More details: {message}";

            Error(errorMessage);
        }

        public void InitiatePurchase(string id, Action purchaseSuccessful)
        {
            foreach (var iap in _products)
            {
                if (iap.Id == id)
                {
                    Debug($"Try to purchase product '{id}'", _settings.Debug);
                    _purchaseSuccessful = purchaseSuccessful;
                    _storeController.InitiatePurchase(id);
                    break;
                }
            }
        }

        public ProductData GetProductData(string id)
        {
            var product = _storeController.products.WithID(id);

            if (product == null)
            {
                Error($"Product with '{id}' not found");
                return new ProductData();
            }

            if (product.metadata == null)
            {
                Error($"Product metadata with '{id}' not found");
                return new ProductData();
            }
            
            var metadata = product.metadata;
                
            var productData = new ProductData
            {
                Price = metadata.localizedPrice,
                PriceString = metadata.localizedPriceString,
                Title = metadata.localizedTitle,
                Description = metadata.localizedDescription,
                CurrencyCode = metadata.isoCurrencyCode
            };
            
            return productData;
        }

        public bool IsPurchased(string id)
        {
            var product = _storeController.products.WithID(id);

            if (product != null && product.hasReceipt)
                return true;

            return false;
        }
        
        public void RestoreTransactions(Action restoreSuccessful, Action restoreFailed)
        {
            _restoreSuccessful = restoreSuccessful;
            _restoreFailed = restoreFailed;

            Debug($"Try to restore on '{Application.platform}'", _settings.Debug);

            switch (Application.platform)
            {
                case RuntimePlatform.Android:
                    _googlePlayStoreExtensions.RestoreTransactions(OnRestore);
                    break;
                case RuntimePlatform.IPhonePlayer:
                case RuntimePlatform.OSXPlayer:
                    _appleExtensions.RestoreTransactions(OnRestore);
                    break;
                case RuntimePlatform.WindowsEditor:
                case RuntimePlatform.LinuxEditor:
                case RuntimePlatform.OSXEditor:
                    Debug($"Fake restore in Editor on '{Application.platform}'", _settings.Debug);
                    OnRestore(true, null);
                    break;
                default:
                    DebugNotSupport();
                    break;
            }
        }
        
        private void OnRestore(bool success, string error)
        {
            if (success)
            {
                Debug("Restore Successful", _settings.Debug);
                _restoreSuccessful?.Invoke();
            }
            else
            {
                Error($"Restore Failed with error: {error}");
                _restoreFailed?.Invoke();
            }
        }

        public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs purchaseEvent)
        {
            var product = purchaseEvent.purchasedProduct;
            Debug($"Purchase complete - product '{product.definition.id}'. Receipt: '{product.receipt}'", _settings.Debug);
            _purchaseSuccessful?.Invoke();
            OnProcessPurchase?.Invoke(purchaseEvent);
            return PurchaseProcessingResult.Complete;
        }

        public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason) => 
            Error($"Purchase failed - product '{product.definition.id}', Purchase failure reason: {failureReason}");
        
        public void OnPurchaseFailed(Product product, PurchaseFailureDescription failureDescription) =>
            Error($"Purchase failed - product: '{product.definition.id}', " +
                  $"Purchase failure reason: {failureDescription.reason}, " +
                  $"Purchase failure details: {failureDescription.message}");
#else
        public void InitiatePurchase(string id, Action purchaseSuccessful) => 
            DebugNotSupport();

        public ProductData GetProductData(string id)
        {
            DebugNotSupport();
            return new ProductData();
        }

        public bool IsPurchased(string id)
        {
            DebugNotSupport();
            return false;
        }

        public void RestoreTransactions(Action restoreSuccessful, Action restoreFailed) => 
            DebugNotSupport();
#endif
        
        public void InitiatePurchase(Enum id, Action purchaseSuccessful) => 
            InitiatePurchase(id.ToString(), purchaseSuccessful);
        
        public ProductData GetProductData(Enum id) => 
            GetProductData(id.ToString());
        
        public bool IsPurchased(Enum id) =>
            IsPurchased(id.ToString());
        
        private void DebugNotSupport() =>
            Debug($"'{Application.platform}' is not supported in '{nameof(IAPService)}'", _settings.Debug);
    }
}