using System;
using UnityEngine.Purchasing;

namespace Savvy.Services.IAP
{
    public interface IIAPService : ISavvyService
    {
        event Action<PurchaseEventArgs> OnProcessPurchase;
        void InitiatePurchase(Enum id, Action purchaseSuccessful);
        void InitiatePurchase(string id, Action purchaseSuccessful);
        ProductData GetProductData(Enum id);
        ProductData GetProductData(string id);
        bool IsPurchased(Enum id);
        bool IsPurchased(string id);
        void RestoreTransactions(Action restoreSuccessful, Action restoreFailed);
    }
}