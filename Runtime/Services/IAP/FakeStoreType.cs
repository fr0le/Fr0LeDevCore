﻿namespace Savvy.Services.IAP
{
    public enum FakeStoreType
    {
        Default = 0,
        StandardUser = 1,
        DeveloperUser = 2
    }
}