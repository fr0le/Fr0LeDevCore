﻿using System;

namespace Savvy.Services.IAP
{
    [Serializable]
    public struct ProductData
    {
        public decimal Price;
        public string PriceString;
        public string Title;
        public string Description;
        public string CurrencyCode;
    }
}