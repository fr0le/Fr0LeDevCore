﻿using System;

namespace Savvy.Services.IAP
{
    [Serializable]
    public struct IAPConvertData
    {
        public string Id;
        public IAPType Type;
    }
}