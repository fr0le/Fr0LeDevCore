namespace Savvy.Services.IAP
{
    public enum IAPType
    {
        Consumable = 0,
        NonConsumable = 1,
        Subscription = 2
    }
}