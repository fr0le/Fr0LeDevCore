using Savvy.Infrastructure;
using UnityEngine;

namespace Savvy.Services.IAP
{
    [CreateAssetMenu(menuName = nameof(Savvy) + "/" + nameof(IAPSettings), fileName = nameof(IAPSettings))]
    public class IAPSettings : ValidateScriptableObject, ISavvySettings
    {
        [Header("Logging")] 
        public bool Debug = false;
    }
}