﻿using System.Threading.Tasks;

namespace Savvy.Services
{
    public interface IService
    {
        void Inject();
        void CheckSettings();
        Task InitAsync();
        void Pause(bool isPause);
        void Destroy();
    }
}