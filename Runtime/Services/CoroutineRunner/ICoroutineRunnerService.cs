using System.Collections;
using UnityEngine;

namespace Savvy.Services.CoroutineRunner
{
    public interface ICoroutineRunnerService : ISavvyService
    {
        Coroutine StartCoroutine(IEnumerator coroutine);
        void StopCoroutine(Coroutine coroutine);
    }
}