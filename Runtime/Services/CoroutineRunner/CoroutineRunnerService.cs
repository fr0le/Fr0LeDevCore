﻿using System.Collections;
using Savvy.Extensions;
using UnityEngine;

namespace Savvy.Services.CoroutineRunner
{
    public class CoroutineRunnerService : ServiceBase, ICoroutineRunnerService
    {
        private readonly string _sourceSettings = $"{nameof(Savvy)}/{nameof(CoroutineRunnerSettings)}";
        private readonly CoroutineRunnerSettings _settings;
        private readonly MonoBehaviour _monoBehaviour;

        public CoroutineRunnerService(MonoBehaviour monoBehaviour)
        {
            _settings = _sourceSettings.LoadResources<CoroutineRunnerSettings>();
            _monoBehaviour = monoBehaviour;
        }

        public Coroutine StartCoroutine(IEnumerator coroutine)
        {
            Debug($"Start coroutine '{coroutine.GetType().Name}'", _settings.Debug);
            return _monoBehaviour.StartCoroutine(coroutine);
        }
        
        public void StopCoroutine(Coroutine coroutine)
        {
            if (coroutine != null)
            {
                Debug($"Stop coroutine '{coroutine.GetType().Name}'", _settings.Debug);
                _monoBehaviour.StopCoroutine(coroutine);   
            }
            else
            {
                Warning("It's impossible to stop Coroutine. Coroutine is null", _settings.Debug);
            }
        }
    }
}