using Savvy.Infrastructure;
using UnityEngine;

namespace Savvy.Services.CoroutineRunner
{
    [CreateAssetMenu(menuName = nameof(Savvy) + "/" + nameof(CoroutineRunnerSettings), fileName = nameof(CoroutineRunnerSettings))]
    public class CoroutineRunnerSettings : ValidateScriptableObject, ISavvySettings
    {
        [Header("Logging")]
        public bool Debug = false;
    }
}