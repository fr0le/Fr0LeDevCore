using Savvy.Extensions;
using UnityEngine;
using UnityDebug = UnityEngine.Debug;
using SystemDateTime = System.DateTime;

namespace Savvy.Services.Logger
{
    public class LoggerService : ServiceBase, ILoggerService
    {
        private readonly string _sourceSettings = $"{nameof(Savvy)}/{nameof(LoggerSettings)}";
        private readonly LoggerSettings _settings;

        public LoggerService() => 
            _settings = _sourceSettings.LoadResources<LoggerSettings>();

        public void Info(object msg) =>
            WriteLog(LogType.Info, _settings.Info, msg);

        public void Verbose(object msg) =>
            WriteLog(LogType.Verbose, _settings.Verbose, msg);

        public void Debug(object msg) =>
            WriteLog(LogType.Debug, _settings.Debug, msg);

        public void Warning(object msg) =>
            WriteLog(LogType.Warning, _settings.Warning, msg);

        public void Error(object msg) =>
            WriteLog(LogType.Error, _settings.Error, msg);

        public void Critical(object msg) =>
            WriteLog(LogType.Critical, _settings.Critical, msg);

        private void WriteLog(LogType logType, Color color, object msg)
        {
            switch (logType)
            {
                case LogType.Info:
                case LogType.Verbose:
                case LogType.Debug:
                    UnityDebug.Log(GetLog(logType, color, msg));
                    break;
                case LogType.Warning:
                    UnityDebug.LogWarning(GetLog(logType, color, msg));
                    break;
                case LogType.Error:
                case LogType.Critical:
                    UnityDebug.LogError(GetLog(logType, color, msg));
                    break;
            }
        }

        private string GetLog(LogType logType, Color color, object msg)
        {
            var log = "";

            if (_settings.Date)
                log += $"[{SystemDateTime.Now.Date.ToShortDateString()}] ";

            if (_settings.Time)
            {
                log += $"[{SystemDateTime.Now.ToLongTimeString()}";

                if (_settings.Millisecond)
                    log += $".{SystemDateTime.Now.Millisecond}";

                log += "] ";
            }

#if UNITY_EDITOR
            log += $"[<color=#{ColorUtility.ToHtmlStringRGB(color)}>{logType}</color>] ";
#else
            log += $"[{logType}] ";
#endif

            log += msg;
            return log;
        }
    }
}