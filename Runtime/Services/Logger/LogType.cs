namespace Savvy.Services.Logger
{
    public enum LogType
    {
        Info = 0,
        Verbose = 1,
        Debug = 2,
        Warning = 3,
        Error = 4,
        Critical = 5
    }
}