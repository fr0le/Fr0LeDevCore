namespace Savvy.Services.Logger
{
    public interface ILoggerService : ISavvyService
    {
        void Info(object msg);
        void Verbose(object msg);
        void Debug(object msg);
        void Warning(object msg);
        void Error(object msg);
        void Critical(object msg);
    }
}