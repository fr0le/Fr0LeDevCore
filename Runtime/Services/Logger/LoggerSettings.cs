﻿using Savvy.Infrastructure;
using UnityEngine;

namespace Savvy.Services.Logger
{
    [CreateAssetMenu(menuName = nameof(Savvy) + "/" + nameof(LoggerSettings), fileName = nameof(LoggerSettings))]
    public class LoggerSettings : ValidateScriptableObject, ISavvySettings
    {
        [Header("Logging")]
        public bool Date;
        public bool Time = true;
        public bool Millisecond = true;

        [Header("Settings")]
        public Color Info = new(1, 0.9f, 0.01f, 1);
        public Color Verbose = new(0, 1, 1, 1);
        public Color Debug = new(1, 0, 1, 1);
        public Color Warning = new(1, 0.75f, 0.025f, 1);
        public Color Error = new(1, 0.35f, 0.35f, 1);
        public Color Critical = new(1, 0, 0, 1);
    }
}