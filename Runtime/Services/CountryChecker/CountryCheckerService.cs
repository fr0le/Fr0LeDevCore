﻿using System.Linq;
using System.Threading.Tasks;
using Savvy.Extensions;
using UnityEngine.Networking;

namespace Savvy.Services.CountryChecker
{
    public class CountryCheckerService : ServiceBase, ICountryCheckerService
    {
        private readonly string _sourceSettings = $"{nameof(Savvy)}/{nameof(CountryCheckerSettings)}";
        private readonly CountryCheckerSettings _settings;

        private string _userCountry;
        
        public CountryCheckerService() => 
            _settings = _sourceSettings.LoadResources<CountryCheckerSettings>();
        
        public override void CheckSettings()
        {
            if (string.IsNullOrEmpty(_settings.URL))
                Error($"'{nameof(_settings.URL)}' cannot be null or empty. Correct the settings file '{_sourceSettings}'");
            
            if (_settings.Target == null || _settings.Target.Length == 0)
                Error($"'{nameof(_settings.Target)}' cannot be null or empty. Correct the settings file '{_sourceSettings}'");
        }

        public override async Task InitAsync() => 
            _userCountry = await FetchUserCountry();

        private async Task<string> FetchUserCountry()
        {
            using var webRequest = UnityWebRequest.Get(_settings.URL);
            var requestAsync = webRequest.SendWebRequest();

            while (!requestAsync.isDone)
                await Task.Yield();

            if (webRequest.result is UnityWebRequest.Result.ConnectionError or UnityWebRequest.Result.ProtocolError)
            {
                Debug($"Error fetching location data: {webRequest.error}");
                return null;
            }
            else
            {
                var jsonResult = webRequest.downloadHandler.text;
                var locationData = jsonResult.FromJson<LocationData>();

                Debug($"User Country: '{locationData.country}'", _settings.Debug);
                return locationData.country;
            }
        }

        public bool IsTargetMatch()=> 
            _settings.Target.Contains(_userCountry);

        public bool IsListMatch(string[] targetList) => 
            targetList.Contains(_userCountry);
    }
}