﻿namespace Savvy.Services.CountryChecker
{
    public interface ICountryCheckerService : ISavvyService
    {
        bool IsTargetMatch();
        bool IsListMatch(string[] targetList);
    }
}