﻿using Savvy.Infrastructure;
using UnityEngine;

namespace Savvy.Services.CountryChecker
{
    [CreateAssetMenu(menuName = nameof(Savvy) + "/" + nameof(CountryCheckerSettings), fileName = nameof(CountryCheckerSettings))]
    public class CountryCheckerSettings : ValidateScriptableObject, ISavvySettings
    {
        [Header("Logging")] 
        public bool Debug = false;

        [Header("Settings")] 
        public string URL = "https://ipinfo.io/json";
        public string[] Target = {"RU"};
    }
}