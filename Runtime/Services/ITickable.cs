﻿namespace Savvy.Services
{
    public interface ITickable
    {
        void Tick();
    }
}