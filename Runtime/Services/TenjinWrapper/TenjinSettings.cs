﻿using Savvy.Infrastructure;
using UnityEngine;

namespace Savvy.Services.TenjinWrapper
{
    [CreateAssetMenu(menuName = nameof(Savvy) + "/" + nameof(TenjinSettings), fileName = nameof(TenjinSettings))]
    public class TenjinSettings : ValidateScriptableObject, ISavvySettings
    {
        [Header("Logging")]
        public bool Debug = false;

        [Header("Settings")] 
        public string GooglePlayKey;
        public string IOSKey;
    }
}