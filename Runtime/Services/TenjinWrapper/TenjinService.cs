﻿using Savvy.Extensions;
#if SAVVY_TENJIN
using System.Collections.Generic;
using Google.MiniJSON;
using Savvy.Services.IAP;
using UnityEngine.Purchasing;
#else
using UnityEngine;
#endif

namespace Savvy.Services.TenjinWrapper
{
    public class TenjinService : ServiceBase, ITenjinService
    {
        private readonly string _sourceSettings = $"{nameof(Savvy)}/{nameof(TenjinSettings)}";
        private readonly TenjinSettings _settings;
        
#if SAVVY_TENJIN
        private BaseTenjin _instance;
        private IIAPService _iap;
#endif
        
        public TenjinService() => 
            _settings = _sourceSettings.LoadResources<TenjinSettings>();

#if SAVVY_TENJIN     
        public override void Inject() => 
            _iap = GetService<IIAPService>();

        public override void Init()
        {
            Connect();
            
            if (_iap != null)
                _iap.OnProcessPurchase += OnProcessPurchase;
        }

        private void Connect()
        {
#if UNITY_ANDROID
            _instance = Tenjin.getInstance(_settings.GooglePlayKey);
            _instance.SetAppStoreType(AppStoreType.googleplay);
#endif
            
#if UNITY_IOS
            _instance = Tenjin.getInstance(_settings.IOSKey);
#endif
            
            _instance.OptIn();
            _instance.Connect();
        }

        public override void Pause(bool isPause)
        {
            if (!isPause) 
                Connect();
        }
        
        public override void Destroy()
        {
            if (_iap != null)
                _iap.OnProcessPurchase -= OnProcessPurchase;
        }
        
        private void OnProcessPurchase(PurchaseEventArgs purchaseEventArgs) 
        {
            var price = purchaseEventArgs.purchasedProduct.metadata.localizedPrice;
            var lPrice = decimal.ToDouble(price);
            var currencyCode = purchaseEventArgs.purchasedProduct.metadata.isoCurrencyCode;

            var wrapper = Json.Deserialize(purchaseEventArgs.purchasedProduct.receipt) as Dictionary<string, object>;  // https://gist.github.com/darktable/1411710
            
            if (null == wrapper)
                return;

            var store = (string)wrapper["Store"]; // GooglePlay, AmazonAppStore, AppleAppStore, etc.
            var payload = (string)wrapper["Payload"]; // For Apple this will be the base64 encoded ASN.1 receipt. For Android, it is the raw JSON receipt.
            var productId = purchaseEventArgs.purchasedProduct.definition.id;

#if UNITY_ANDROID
            if (store.Equals("GooglePlay")) {
                var googleDetails = Json.Deserialize(payload) as Dictionary<string, object>;
                var googleJson = (string)googleDetails["json"];
                var googleSig = (string)googleDetails["signature"];

                CompletedAndroidPurchase(productId, currencyCode, 1, lPrice, googleJson, googleSig);
            }
#elif UNITY_IOS
            var transactionId = purchaseEventArgs.purchasedProduct.transactionID;
            CompletedIosPurchase(productId, currencyCode, 1, lPrice , transactionId, payload);
#endif
        }

        private void CompletedAndroidPurchase(string ProductId, string CurrencyCode, int Quantity, double UnitPrice, string Receipt, string Signature) => 
            _instance.Transaction(ProductId, CurrencyCode, Quantity, UnitPrice, null, Receipt, Signature);

        private void CompletedIosPurchase(string ProductId, string CurrencyCode, int Quantity, double UnitPrice, string TransactionId, string Receipt) => 
            _instance.Transaction(ProductId, CurrencyCode, Quantity, UnitPrice, TransactionId, Receipt, null);
#else
        private void DebugNotSupport() =>
            Debug($"'{Application.platform}' is not supported in '{nameof(TenjinService)}'", _settings.Debug);
#endif
    }
}