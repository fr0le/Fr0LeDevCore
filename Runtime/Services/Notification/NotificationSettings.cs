﻿using Savvy.Infrastructure;
using UnityEngine;

namespace Savvy.Services.Notification
{
    [CreateAssetMenu(menuName = nameof(Savvy) + "/" + nameof(NotificationSettings), fileName = nameof(NotificationSettings))]
    public class NotificationSettings : ValidateScriptableObject, ISavvySettings
    {  
        [Header("Logging")]
        public bool Debug = false;

        [Header("Settings")]
        public string GroupId = "Main";
        public string GroupName = "Main Notification";
        public string GroupDescription = "Main Description";
        [Space]
        public string ChannelId = "ChannelId";
        public string ChannelName = "ChannelName";
        public string ChannelDescription = "ChannelDescription";
        
        [Header("Notifications")]
        public NotificationData[] NotificationData;
    }
}