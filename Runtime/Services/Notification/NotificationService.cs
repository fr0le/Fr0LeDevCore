﻿using Savvy.Extensions;
#if SAVVY_NOTIFICATION
using Savvy.Services.Localization;
using Unity.Notifications.Android;
#else
using UnityEngine;
#endif

namespace Savvy.Services.Notification
{
    public class NotificationService : ServiceBase, INotificationService
    {
        private readonly string _sourceSettings = $"{nameof(Savvy)}/{nameof(NotificationSettings)}";
        private readonly NotificationSettings _settings;

        public NotificationService() =>
            _settings = _sourceSettings.LoadResources<NotificationSettings>();

#if SAVVY_NOTIFICATION
        public override void Init()
        {
            var group = new AndroidNotificationChannelGroup
            {
                Id = _settings.GroupId,
                Name = _settings.GroupName,
                Description = _settings.GroupDescription
            };
            
            AndroidNotificationCenter.RegisterNotificationChannelGroup(group);
            Debug($"Register notification channel group '{group.Id}'", _settings.Debug);
            
            var channel = new AndroidNotificationChannel
            {
                Id = _settings.ChannelId,
                Name = _settings.ChannelName,
                Importance = Importance.Default,
                Description = _settings.ChannelDescription,
                Group = _settings.GroupId
            };
            
            AndroidNotificationCenter.RegisterNotificationChannel(channel);
            Debug($"Register notification channel '{channel.Id}'", _settings.Debug);
            CancelAllNotifications();
            Debug("Notifications successfully initialized", _settings.Debug);
        }

        public override void Pause(bool isPause)
        {
            if (isPause)
                foreach (var notification in _settings.NotificationData)
                    CreateNotification(notification);
            else
                CancelAllNotifications();
        }

        public void CreateNotification(NotificationData data) =>
            CreateNotification(data.TitleKey.ToLocalization(), data.TextKey.ToLocalization(), data.Seconds, data.SmallIcon, data.LargeIcon);
        
        public void CreateNotification(string title, string text, int seconds, string smallIcon, string largeIcon)
        {
            var notification = new AndroidNotification
            {
                Title = title,
                Text = text,
                FireTime = System.DateTime.Now.AddSeconds(seconds),
                SmallIcon = smallIcon,
                LargeIcon = largeIcon
            };

            var notificationId = AndroidNotificationCenter.SendNotification(notification, _settings.ChannelId);
            Debug($"Created notification by id '{notificationId}'. Will be send in {seconds} seconds", _settings.Debug);
        }

        private void CancelAllNotifications()
        {
            AndroidNotificationCenter.CancelAllNotifications();
            Debug("Cancel all notifications", _settings.Debug);
        }
#else
        public void CreateNotification(NotificationData data) =>
            DebugNotSupport();
        
        public void CreateNotification(string title, string text, int seconds, string smallIcon, string largeIcon) =>
            DebugNotSupport();

        private void DebugNotSupport() =>
            Debug($"'{Application.platform}' is not supported in '{nameof(NotificationService)}'", _settings.Debug);
#endif
    }
}