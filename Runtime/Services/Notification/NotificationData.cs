﻿using System;

namespace Savvy.Services.Notification
{
    [Serializable]
    public struct NotificationData
    {
        public string TitleKey;
        public string TextKey;
        public int Seconds;
        public string SmallIcon;
        public string LargeIcon;
    }
}