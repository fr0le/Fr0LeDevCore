﻿namespace Savvy.Services.Notification
{
    public interface INotificationService : ISavvyService
    {
        void CreateNotification(NotificationData data);
        void CreateNotification(string title, string text, int seconds, string smallIcon, string largeIcon);
    }
}