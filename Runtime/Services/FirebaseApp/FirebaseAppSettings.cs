﻿using Savvy.Infrastructure;
using UnityEngine;

namespace Savvy.Services.FirebaseApp
{
    [CreateAssetMenu(menuName = nameof(Savvy) + "/" + nameof(FirebaseAppSettings), fileName = nameof(FirebaseAppSettings))]
    public class FirebaseAppSettings : ValidateScriptableObject, ISavvySettings
    {
        [Header("Logging")] 
        public bool Debug = false;
    }
}