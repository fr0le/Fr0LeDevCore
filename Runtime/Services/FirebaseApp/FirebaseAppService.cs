﻿using Savvy.Extensions;
#if SAVVY_FIREBASE_APP
using System.Threading.Tasks;
using FirebaseInstance = Firebase.FirebaseApp;
using Firebase;
#else
using UnityEngine;
#endif

namespace Savvy.Services.FirebaseApp
{
    public class FirebaseAppService : ServiceBase, IFirebaseAppService
    {
        private readonly string _sourceSettings = $"{nameof(Savvy)}/{nameof(FirebaseAppSettings)}";
        private readonly FirebaseAppSettings _settings;

#if SAVVY_FIREBASE_APP
        private FirebaseInstance _instance;
#endif
        
        public FirebaseAppService() => 
            _settings = _sourceSettings.LoadResources<FirebaseAppSettings>();

#if SAVVY_FIREBASE_APP
        public override async Task InitAsync()
        {
            var dependencyStatus = await FirebaseInstance.CheckAndFixDependenciesAsync();
    
            if (dependencyStatus == DependencyStatus.Available)
            {
                _instance = FirebaseInstance.DefaultInstance;
                Debug("FirebaseApp successfully initialized", _settings.Debug);
            }
            else
            {
                Error($"Could not resolve all Firebase dependencies: {dependencyStatus}");
            }
        }
#else
        private void DebugNotSupport() =>
            Debug($"'{Application.platform}' is not supported in '{nameof(FirebaseAppService)}'", _settings.Debug);
#endif
    }
}