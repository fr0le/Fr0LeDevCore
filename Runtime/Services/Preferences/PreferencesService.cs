﻿using System.Globalization;
using Savvy.Extensions;
using UnityEngine;

namespace Savvy.Services.Preferences
{
    public class PreferencesService : ServiceBase, IPreferencesService
    {
        private readonly string _sourceSettings = $"{nameof(Savvy)}/{nameof(PreferencesSettings)}";
        private readonly PreferencesSettings _settings;

        public PreferencesService() => 
            _settings = _sourceSettings.LoadResources<PreferencesSettings>();

        public bool HasKey(string key)
        {
            var result = PlayerPrefs.HasKey(key);
            Debug($"Prefs key '{key}' {(result ? "found" : "not found")}", _settings.Debug);
            return result;
        }

        public void SaveInt(string key, int value)
        {
            if (IsNullOrEmpty(key)) 
                return;
            
            PlayerPrefs.SetInt(key, value);
            PlayerPrefs.Save();
            Debug($"Save int prefs. Key '{key}', value '{value}'", _settings.Debug);
        }

        public void SaveFloat(string key, float value)
        {
            if (IsNullOrEmpty(key)) 
                return;
            
            PlayerPrefs.SetFloat(key, value);
            PlayerPrefs.Save();
            Debug($"Save float prefs. Key '{key}', value '{value}'", _settings.Debug);
        }

        public void SaveDouble(string key, double value)
        {
            if (IsNullOrEmpty(key)) 
                return;
            
            PlayerPrefs.SetString(key, value.ToString(CultureInfo.InvariantCulture));
            PlayerPrefs.Save();
            Debug($"Save double prefs. Key '{key}', value '{value}'", _settings.Debug);
        }

        public void SaveString(string key, string value)
        {
            if (IsNullOrEmpty(key)) 
                return;
            
            PlayerPrefs.SetString(key, value);
            PlayerPrefs.Save();
            Debug($"Save string prefs. Key '{key}', value '{value}'", _settings.Debug);
        }

        public void SaveBool(string key, bool value)
        {
            if (IsNullOrEmpty(key)) 
                return;
            
            var result = value.ToIntByBool();
            PlayerPrefs.SetInt(key, result);
            PlayerPrefs.Save();
            Debug($"Save bool prefs. Key '{key}', value '{value}'", _settings.Debug);
        }

        public void SaveEnum<TEnum>(string key, TEnum value)
        {
            if (IsNullOrEmpty(key)) 
                return;
            
            var result = value.ToString();
            PlayerPrefs.SetString(key, result);
            PlayerPrefs.Save();
            Debug($"Save enum prefs. Key '{key}', value '{result}'", _settings.Debug);
        }

        public void SaveJson<TData>(string key, TData value)
        {
            if (IsNullOrEmpty(key)) 
                return;
            
            var result = value.ToJson();
            PlayerPrefs.SetString(key, result);
            PlayerPrefs.Save();
            Debug($"Save json prefs. Key '{key}', value '{result}'", _settings.Debug);
        }

        public int LoadInt(string key, int defaultValue)
        {
            if (!HasKey(key)) 
                SaveInt(key, defaultValue);

            var result = PlayerPrefs.GetInt(key);
            Debug($"Load int prefs. Key '{key}', value '{result}'", _settings.Debug);
            return result;
        }

        public float LoadFloat(string key, float defaultValue)
        {
            if (!HasKey(key)) 
                SaveFloat(key, defaultValue);

            var result = PlayerPrefs.GetFloat(key);
            Debug($"Load float prefs. Key '{key}', value '{result}'", _settings.Debug);
            return result;
        }

        public double LoadDouble(string key, double defaultValue)
        {
            if (!HasKey(key)) 
                SaveDouble(key, defaultValue);

            var result = double.Parse(PlayerPrefs.GetString(key), CultureInfo.InvariantCulture);
            Debug($"Load double prefs. Key '{key}', value '{result}'", _settings.Debug);
            return result;
        }

        public string LoadString(string key, string defaultValue)
        {
            if (!HasKey(key)) 
                SaveString(key, defaultValue);

            var result = PlayerPrefs.GetString(key);
            Debug($"Load string prefs. Key '{key}', value '{result}'", _settings.Debug);
            return result;
        }

        public bool LoadBool(string key, bool defaultValue)
        {
            if (!HasKey(key)) 
                SaveBool(key, defaultValue);

            var result = PlayerPrefs.GetInt(key).ToBoolByInt();
            Debug($"Load bool prefs. Key '{key}', value '{result}'", _settings.Debug);
            return result;
        }

        public TEnum LoadEnum<TEnum>(string key, TEnum defaultValue) where TEnum : struct
        {
            if (!HasKey(key)) 
                SaveEnum(key, defaultValue);

            var result = PlayerPrefs.GetString(key).ToEnumOrDefault(defaultValue);
            Debug($"Load enum prefs. Key '{key}', value '{result}'", _settings.Debug);
            return result;
        }

        public TData LoadJson<TData>(string key, TData defaultValue)
        {
            if (!HasKey(key)) 
                SaveJson(key, defaultValue);

            var result = PlayerPrefs.GetString(key);
            Debug($"Load json prefs. Key '{key}', value '{result}'", _settings.Debug);
            return result.FromJson<TData>();
        }

        private bool IsNullOrEmpty(string key)
        {
            if (string.IsNullOrEmpty(key))
            {
                Error($"Save key cannot be null or empty");
                return true;
            }

            return false;
        }
    }
}