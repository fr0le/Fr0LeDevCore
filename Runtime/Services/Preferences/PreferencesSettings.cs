﻿using Savvy.Infrastructure;
using UnityEngine;

namespace Savvy.Services.Preferences
{
    [CreateAssetMenu(menuName = nameof(Savvy) + "/" + nameof(PreferencesSettings), fileName = nameof(PreferencesSettings))]
    public class PreferencesSettings : ValidateScriptableObject, ISavvySettings
    {
        [Header("Logging")]
        public bool Debug = false;
    }
}