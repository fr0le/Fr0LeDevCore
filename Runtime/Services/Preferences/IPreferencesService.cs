﻿namespace Savvy.Services.Preferences
{
    public interface IPreferencesService : ISavvyService
    {
        bool HasKey(string key);
        void SaveInt(string key, int value);
        void SaveFloat(string key, float value);
        void SaveDouble(string key, double value);
        void SaveString(string key, string value);
        void SaveBool(string key, bool value);
        void SaveEnum<TEnum>(string key, TEnum value);
        void SaveJson<TData>(string key, TData value);
        int LoadInt(string key, int defaultValue = default);
        float LoadFloat(string key, float defaultValue = default);
        double LoadDouble(string key, double defaultValue = default);
        string LoadString(string key, string defaultValue = default);
        bool LoadBool(string key, bool defaultValue = default);
        TEnum LoadEnum<TEnum>(string key, TEnum defaultValue = default) where TEnum : struct;
        TData LoadJson<TData>(string key, TData defaultValue = default);
    }
}