using Savvy.Infrastructure;
using UnityEngine;

namespace Savvy.Services.DateTime
{
    [CreateAssetMenu(menuName = nameof(Savvy) + "/" + nameof(DateTimeSettings), fileName = nameof(DateTimeSettings))]
    public class DateTimeSettings : ValidateScriptableObject, ISavvySettings
    {
        [Header("Logging")] 
        public bool Debug = false;

        [Header("Settings")]
        public FormatLocalizationData[] Short =
        {
            new()
            {
                Type = FormatTime.Days,
                Key = "Dd"
            },
            new()
            {
                Type = FormatTime.Hours,
                Key = "Hh"
            },
            new()
            {
                Type = FormatTime.Minutes,
                Key = "Mm"
            },
            new()
            {
                Type = FormatTime.Seconds,
                Key = "Ss"
            }
        };

        public FormatLocalizationData[] Average =
        {
            new()
            {
                Type = FormatTime.Days,
                Key = "Ddd"
            },
            new()
            {
                Type = FormatTime.Hours,
                Key = "Hhh"
            },
            new()
            {
                Type = FormatTime.Minutes,
                Key = "Mmm"
            },
            new()
            {
                Type = FormatTime.Seconds,
                Key = "Sss"
            }
        };
    }
}