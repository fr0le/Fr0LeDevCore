using System;
using System.Collections.Generic;
using System.Linq;
using Savvy.Services.Localization;

namespace Savvy.Services.DateTime
{
    public static class DateTimeExtensions
    {
        public static long AddDaysToUnixTime(this DateTimeOffset dateTimeOffset, long day = 1) => 
            dateTimeOffset.AddDays(day).ToUnixTimeSeconds();    
        
        public static long AddHoursToUnixTime(this DateTimeOffset dateTimeOffset, long hour = 1) => 
            dateTimeOffset.AddHours(hour).ToUnixTimeSeconds();
        
        public static long AddMinutesToUnixTime(this DateTimeOffset dateTimeOffset, long minute = 1) => 
            dateTimeOffset.AddMinutes(minute).ToUnixTimeSeconds();
        
        public static long AddSecondsToUnixTime(this DateTimeOffset dateTimeOffset, long second = 1) => 
            dateTimeOffset.AddSeconds(second).ToUnixTimeSeconds();
        
        public static string ToTimeFormat(this long time, params FormatTime[] exceptions)
        {
            if (time < 0)
                time = 0;

            exceptions ??= new FormatTime[0];
            
            var timeFormat = DateTimeOffset.FromUnixTimeSeconds(time).DateTime;
            var resultParts = new List<string>();
            
            if (!exceptions.Contains(FormatTime.Days))
                resultParts.Add($"{time / SecConstants.Day}");

            if (!exceptions.Contains(FormatTime.Hours))
                resultParts.Add(exceptions.Contains(FormatTime.Days)
                    ? $"{time / SecConstants.Hour}"
                    : $"{GetHourFormat(timeFormat)}");

            if (!exceptions.Contains(FormatTime.Minutes))
                resultParts.Add(exceptions.Contains(FormatTime.Hours)
                    ? $"{time / SecConstants.Min}"
                    : $"{GetMinutesFormat(timeFormat)}");

            if (!exceptions.Contains(FormatTime.Seconds))
                resultParts.Add(exceptions.Contains(FormatTime.Minutes) 
                    ? $"{time}"
                    : GetSecondsFormat(timeFormat));

            return string.Join(":", resultParts);
        }

        public static string ToTimeFormatLocalization(this long time, FormatLocalization formatLocalization, params FormatTime[] exceptions)
        {
            if (time < 0)
                time = 0;

            exceptions ??= new FormatTime[0];
            
            var timeFormat = DateTimeOffset.FromUnixTimeSeconds(time).DateTime;
            var resultParts = new List<string>();
            
            if (!exceptions.Contains(FormatTime.Days))
                resultParts.Add($"{time / SecConstants.Day}{GetLocalization(FormatTime.Days, formatLocalization)} ");

            if (!exceptions.Contains(FormatTime.Hours))
            {
                var timePart = exceptions.Contains(FormatTime.Days)
                    ? $"{time / SecConstants.Hour}"
                    : $"{GetHourFormat(timeFormat)}";

                timePart += $"{GetLocalization(FormatTime.Hours, formatLocalization)} ";
                resultParts.Add(timePart);
            }

            if (!exceptions.Contains(FormatTime.Minutes))
            {
                var timePart = exceptions.Contains(FormatTime.Hours)
                    ? $"{time / SecConstants.Min}"
                    : $"{GetMinutesFormat(timeFormat)}";
                
                timePart += $"{GetLocalization(FormatTime.Minutes, formatLocalization)} ";
                resultParts.Add(timePart);
            }

            if (!exceptions.Contains(FormatTime.Seconds))
            {
                var timePart = exceptions.Contains(FormatTime.Minutes) 
                    ? $"{time}"
                    : GetSecondsFormat(timeFormat);
                
                timePart += $"{GetLocalization(FormatTime.Seconds, formatLocalization)}";
                resultParts.Add(timePart);
            }
            
            return string.Join("", resultParts);
        }

        private static string GetLocalization(FormatTime formatTime, FormatLocalization formatLocalization)
        {
            var dateTime = Service.Container.GetSingle<IDateTimeService>();
            var key = dateTime.GetFormatLocalization(formatTime, formatLocalization);
            return key.ToLocalization();
        }
        
        private static string GetHourFormat(System.DateTime time) => 
            $"{time:HH}";
        
        private static string GetMinutesFormat(System.DateTime time) => 
            $"{time:mm}";
        
        private static string GetSecondsFormat(System.DateTime time) => 
            $"{time:ss}";
    }
}