using System;

namespace Savvy.Services.DateTime
{
    public interface IDateTimeService : ISavvyService
    {
        public DateTimeOffset GetNow();
        public long GetNowUnixTime();
        long GetTimeLeft(long nextTime);
        string GetFormatLocalization(FormatTime formatTime, FormatLocalization formatLocalization);
    }
}