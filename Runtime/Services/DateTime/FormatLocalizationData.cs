﻿using System;

namespace Savvy.Services.DateTime
{
    [Serializable]
    public struct FormatLocalizationData
    {
        public FormatTime Type;
        public string Key;
    }
}