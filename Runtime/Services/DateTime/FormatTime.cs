﻿namespace Savvy.Services.DateTime
{
    public enum FormatTime
    {
        Seconds = 0,
        Minutes = 1,
        Hours = 2,
        Days = 3
    }
}