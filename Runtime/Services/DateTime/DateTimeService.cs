using System;
using System.Linq;
using Savvy.Extensions;

namespace Savvy.Services.DateTime
{
    public class DateTimeService : ServiceBase, IDateTimeService
    {
        private readonly string _sourceSettings = $"{nameof(Savvy)}/{nameof(DateTimeSettings)}";
        private readonly DateTimeSettings _settings;

        public DateTimeService() => 
            _settings = _sourceSettings.LoadResources<DateTimeSettings>();
        
        public override void CheckSettings()
        {
            if (_settings.Short == null || _settings.Short.Length == 0)
                Error($"'{nameof(_settings.Short)}' cannot be null or empty. Correct the settings file '{_sourceSettings}'");
            
            if (_settings.Average == null || _settings.Average.Length == 0)
                Error($"'{nameof(_settings.Average)}' cannot be null or empty. Correct the settings file '{_sourceSettings}'");
        }

        public DateTimeOffset GetNow()
        {
            Debug($"DateTimeOffset now '{DateTimeOffset.Now}'", _settings.Debug);
            return DateTimeOffset.Now;
        }

        public long GetNowUnixTime()
        {
            Debug($"DateTimeOffset now unix time '{DateTimeOffset.Now.ToUnixTimeSeconds()}'", _settings.Debug);
            return DateTimeOffset.Now.ToUnixTimeSeconds();
        }

        public long GetTimeLeft(long nextTime)
        {
            var leftTime = nextTime - GetNowUnixTime();
            return leftTime < 0 ? 0 : leftTime;
        }

        public string GetFormatLocalization(FormatTime formatTime, FormatLocalization formatLocalization)
        {
            var formatData = formatLocalization == FormatLocalization.Average 
                ? _settings.Average 
                : _settings.Short;
            
            return (from data in formatData where data.Type == formatTime select data.Key).FirstOrDefault();
        }
    }
}