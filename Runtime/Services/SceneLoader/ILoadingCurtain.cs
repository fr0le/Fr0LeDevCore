namespace Savvy.Services.SceneLoader
{
    public interface ILoadingCurtain
    {    
        void Show();
        void Hide();
        void SlowlyHide();
    }
}