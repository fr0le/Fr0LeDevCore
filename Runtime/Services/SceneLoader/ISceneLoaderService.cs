using System;

namespace Savvy.Services.SceneLoader
{
    public interface ISceneLoaderService : ISavvyService
    { 
        event Action<float> OnProgressUpdated;
        public void LoadSceneAsync(string sceneName, Action onLoaded = null);
        ILoadingCurtain LoadingCurtain { get; }
    }
}