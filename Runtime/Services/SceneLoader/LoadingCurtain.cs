﻿using System.Collections;
using Savvy.Infrastructure;
using UnityEngine;
using UnityEngine.UI;

namespace Savvy.Services.SceneLoader
{
    public class LoadingCurtain : MonoSavvy, ILoadingCurtain
    {
        [SerializeField] private CanvasGroup _curtain;
        [SerializeField] private Slider _slider;
        [SerializeField] private float _fade;
        
        private ISceneLoaderService _sceneLoader;

        private void Awake()
        {
            DontDestroyOnLoad(this);
            _sceneLoader = GetService<ISceneLoaderService>();

            if (_sceneLoader != null)
                _sceneLoader.OnProgressUpdated += HandleProgressUpdated;
        }

        private void OnDestroy()
        {
            if (_sceneLoader != null)
                _sceneLoader.OnProgressUpdated -= HandleProgressUpdated;
        }

        public void Show()
        {
            gameObject.SetActive(true);
            _curtain.alpha = 1;
        }

        public void Hide()
        {
            _curtain.alpha = 0;
            gameObject.SetActive(false);
        }

        public void SlowlyHide() =>
            StartCoroutine(DoFadeIn());

        private IEnumerator DoFadeIn()
        {
            while (_curtain.alpha > 0)
            {
                _curtain.alpha -= _fade;
                yield return new WaitForSeconds(_fade);
            }

            gameObject.SetActive(false);
        }

        private void HandleProgressUpdated(float value) => 
            _slider.value = value;
    }
}