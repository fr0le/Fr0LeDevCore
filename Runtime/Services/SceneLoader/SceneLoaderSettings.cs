﻿using Savvy.Infrastructure;
using UnityEngine;

namespace Savvy.Services.SceneLoader
{
    [CreateAssetMenu(menuName = nameof(Savvy) + "/" + nameof(SceneLoaderSettings), fileName = nameof(SceneLoaderSettings))]
    public class SceneLoaderSettings : ValidateScriptableObject, ISavvySettings
    {
        [Header("Logging")]
        public bool Debug = false;
        
        [Header("Settings")]
        public LoadingCurtain LoadingCurtainPrefab;
    }
}