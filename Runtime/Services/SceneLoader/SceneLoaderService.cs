﻿using System;
using System.Collections;
using Savvy.Extensions;
using Savvy.Services.CoroutineRunner;
using UnityEngine;
using UnityEngine.SceneManagement;
using Object = UnityEngine.Object;

namespace Savvy.Services.SceneLoader
{
    public class SceneLoaderService : ServiceBase, ISceneLoaderService
    {
        private readonly string _sourceSettings = $"{nameof(Savvy)}/{nameof(SceneLoaderSettings)}";
        private readonly SceneLoaderSettings _settings;
        
        public event Action<float> OnProgressUpdated;
        
        private ICoroutineRunnerService _coroutineRunner;
        
        public ILoadingCurtain LoadingCurtain { get; private set; }

        public SceneLoaderService() => 
            _settings = _sourceSettings.LoadResources<SceneLoaderSettings>();

        public override void Inject() => 
            _coroutineRunner = GetService<ICoroutineRunnerService>();

        public override void CheckSettings()
        {
            if (_settings.LoadingCurtainPrefab == null)
                Error($"'{nameof(_settings.LoadingCurtainPrefab)}' cannot be null or empty. Correct the settings file '{_sourceSettings}'");
        }

        public override void Init() => 
            LoadingCurtain = Object.Instantiate(_settings.LoadingCurtainPrefab);

        public void LoadSceneAsync(string sceneName, Action onLoaded)
        {
            Debug($"Load scene '{sceneName}'", _settings.Debug);
            _coroutineRunner.StartCoroutine(LoadSceneCoroutine(sceneName, onLoaded));
        }

        private IEnumerator LoadSceneCoroutine(string sceneName, Action onLoaded)
        {
            var operation = SceneManager.LoadSceneAsync(sceneName);

            if (operation == null)
            {
                Error($"Load scene '{sceneName}' failed to load");
                yield break;
            }
            
            operation.allowSceneActivation = false;

            while (operation.progress < 0.9f)
            {
                OnProgressUpdated?.Invoke(operation.progress);
                yield return null;
            }
            
            OnProgressUpdated?.Invoke(1f);
            operation.allowSceneActivation = true;
            yield return new WaitUntil(() => operation.isDone);
            
            Debug($"Scene '{sceneName}' loaded", _settings.Debug);
            onLoaded?.Invoke();
        }
    }
}