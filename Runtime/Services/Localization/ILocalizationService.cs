using System;
using UnityEngine;

namespace Savvy.Services.Localization
{
    public interface ILocalizationService : ISavvyService
    {
        event Action OnLocalizationUpdate;
        SystemLanguage GetLanguage();
        void SetLanguage(SystemLanguage language);
        string GetTranslation(string key);
        string GetTranslation(string key, params object[] args);
    }
}