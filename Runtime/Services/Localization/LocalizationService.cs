using System;
using System.Collections.Generic;
using Savvy.Extensions;
using Savvy.Services.Preferences;
using UnityEngine;

namespace Savvy.Services.Localization
{
    public class LocalizationService : ServiceBase, ILocalizationService
    {
        private readonly string _sourceSettings = $"{nameof(Savvy)}/{nameof(LocalizationSettings)}";
        private readonly LocalizationSettings _settings;
        private readonly Dictionary<string, string> _dictionary = new();
        
        public event Action OnLocalizationUpdate;
        
        private IPreferencesService _preferences;

        public LocalizationService() => 
            _settings = _sourceSettings.LoadResources<LocalizationSettings>();

        public override void Inject() => 
            _preferences = GetService<IPreferencesService>();

        public override void CheckSettings()
        {
            if (string.IsNullOrEmpty(_settings.PrefsKey))
                Error($"'{nameof(_settings.PrefsKey)}' cannot be null or empty. Correct the settings file '{_sourceSettings}'");
            
            if (_settings.DefaultTextAsset == null)
                Error($"'{nameof(_settings.DefaultTextAsset)}' cannot be null or empty. Correct the settings file '{_sourceSettings}'");
        }

        public override void Init() => 
            LoadTextAsset(GetTextAsset(GetLanguage()));

        public SystemLanguage GetLanguage() => 
            _preferences.LoadEnum(_settings.PrefsKey, Application.systemLanguage);

        public void SetLanguage(SystemLanguage language) => 
            LoadTextAsset(GetTextAsset(language));

        public string GetTranslation(string key) =>
            _dictionary.ContainsKey(key) ? _dictionary[key] : $"'{key}'";

        public string GetTranslation(string key, params object[] args) => 
            _dictionary.ContainsKey(key) ? string.Format(_dictionary[key], args) : $"'{key}'";

        private TextAsset GetTextAsset(SystemLanguage language)
        {
            foreach (var translation in _settings.TranslationsData)
            {
                if (translation.SystemLanguage == language)
                    return GetAvailableLanguage(translation.SystemLanguage, translation.TextAsset);
            }

            return GetAvailableLanguage(_settings.DefaultSystemLanguage, _settings.DefaultTextAsset);
        }

        private TextAsset GetAvailableLanguage(SystemLanguage systemLanguage, TextAsset language)
        {
            if (language == null)
            {
                _preferences.SaveEnum(_settings.PrefsKey, _settings.DefaultSystemLanguage);
                return _settings.DefaultTextAsset;
            }
            else
            {
                _preferences.SaveEnum(_settings.PrefsKey, systemLanguage);
                return language;
            }
        }

        private void LoadTextAsset(TextAsset language)
        {
            try
            {
                var json = language.text.FromJson<LocalizationDictionaryData>();
                _dictionary.Clear();

                foreach (var localization in json.Localization) 
                    _dictionary.Add(localization.Key, localization.Value);

                OnLocalizationUpdate?.Invoke();
                Debug($"Load localization '{language.name}'", _settings.Debug);
            }
            catch (Exception e)
            {
                Error(e.Message);
            }
        }
    }
}