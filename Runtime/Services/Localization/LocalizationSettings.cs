﻿using Savvy.Infrastructure;
using UnityEngine;

namespace Savvy.Services.Localization
{
    [CreateAssetMenu(menuName = nameof(Savvy) + "/" + nameof(LocalizationSettings), fileName = nameof(LocalizationSettings))]
    public class LocalizationSettings : ValidateScriptableObject, ISavvySettings
    {
        [Header("Logging")]
        public bool Debug = false;
        
        [Header("Save Load")]
        public string PrefsKey = "LocalizationSettings";
        
        [Header("Settings")]
        public SystemLanguage DefaultSystemLanguage = SystemLanguage.English;
        public TextAsset DefaultTextAsset;

        [Header("Translations")]
        public TranslationsData[] TranslationsData;
        
#if UNITY_EDITOR
        [Header("Remote")]
        public string SheetId = "1ia45H4f4MwiF8bR23p52eoRq7oL0LbRLfdZpyLg7r7g";
        [HideInInspector] public string SheetsExportUrl = "https://docs.google.com/spreadsheets/d/{0}/export?format=tsv";
        [HideInInspector] public string SheetsUrl = "https://docs.google.com/spreadsheets/d/{0}";
#endif
    }
}