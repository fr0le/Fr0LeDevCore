﻿using System;
using System.Collections.Generic;

namespace Savvy.Services.Localization
{
    [Serializable]
    public struct LocalizationDictionaryData
    {
        public List<LocalizationData> Localization;
    }
}