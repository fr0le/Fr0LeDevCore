using System;
using UnityEngine;

namespace Savvy.Services.Localization
{
    [Serializable]
    public struct TranslationsData
    {
        public SystemLanguage SystemLanguage;
        public TextAsset TextAsset;
    }
}