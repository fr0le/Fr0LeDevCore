using System;

namespace Savvy.Services.Localization
{
    [Serializable]
    public struct LocalizationData
    {
        public string Key;
        public string Value;
    }
}