using UnityEngine;

namespace Savvy.Services.Randomize
{
    public interface IRandomService : ISavvyService
    {
        int GetRange(int min, int max);
        float GetRange(float min, float max);
        double GetRange(double min, double max);
        float GetRange(Vector2 range);
        int GetRangeWithStrict(int min, int max);
        bool GetBool();
        bool GetBoolWithStrictByValue(int value, int min = 1, int max = 100);
        bool GetBoolWithStrictByValue(float value, int min = 1, int max = 100);
        bool GetBoolWithStrictByValue(double value, int min = 1, int max = 100);
        float GetAngle(float min = 0, float max = 360);
        Vector2 GetInsideUnitCircle(float radius = 1);
        Vector3 GetInsideUnitSphere(float radius = 1);
        string GetUid();
        string GetUidWithPrefix(string prefix = "Guid");
        string GetName(int lengthName = 14);
        string GetNameWithPrefix(string prefix = "Player", int lengthName = 8);
        string GetNicknameFromList();
    }
}