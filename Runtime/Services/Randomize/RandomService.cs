﻿using System;
using Savvy.Extensions;
using UnityEngine;
using Random = System.Random;

namespace Savvy.Services.Randomize
{
    public class RandomService : ServiceBase, IRandomService
    {
        private readonly string _sourceSettings = $"{nameof(Savvy)}/{nameof(RandomSettings)}";
        private readonly RandomSettings _settings;
        private readonly Random _random = new();

        public RandomService() => 
            _settings = _sourceSettings.LoadResources<RandomSettings>();

        public override void CheckSettings()
        {
            if (string.IsNullOrEmpty(_settings.Chars))
                Error($"'{nameof(_settings.Chars)}' cannot be null or empty. Correct the settings file '{_sourceSettings}'");
            
            if (_settings.Nicknames == null || _settings.Nicknames.Length == 0)
                Error($"'{nameof(_settings.Nicknames)}' cannot be null or empty. Correct the settings file '{_sourceSettings}'");
        }

        public int GetRange(int min, int max)
        {
            var result = _random.Next(min, max);
            Debug($"Random result '{result}', range '{min}, {max}'", _settings.Debug);
            return result;
        }

        public float GetRange(float min, float max)
        {
            var result = (float)(_random.NextDouble() * (max - min)) + min;
            Debug($"Random result '{result}', range '{min}, {max}'", _settings.Debug);
            return result;
        }

        public double GetRange(double min, double max)
        {
            var result = _random.NextDouble() * (max - min) + min;
            Debug($"Random result '{result}', range '{min}, {max}'", _settings.Debug);
            return result;
        }

        public float GetRange(Vector2 range)
        {
            var result = (float)(_random.NextDouble() * (range.y - range.x)) + range.x;
            Debug($"Random result '{result}', range '{range.x}, {range.y}'", _settings.Debug);
            return result;
        }

        public int GetRangeWithStrict(int min, int max)
        {
            var result = _random.Next(min, max + 1);
            Debug($"Random result '{result}', strict range '{min}, {max}'", _settings.Debug);
            return result;
        }

        public bool GetBool()
        {
            var result = GetRangeWithStrict(0, 1).ToBoolByInt();
            Debug($"Random result '{result}', strict range 'TRUE, FALSE'", _settings.Debug);
            return result;
        }

        public bool GetBoolWithStrictByValue(int value, int min, int max)
        {
            var random = GetRangeWithStrict(min, max);
            var result = random <= value;
            Debug($"Random result '{result}', random <= value '{random} <= {value}'", _settings.Debug);
            return result;
        }

        public bool GetBoolWithStrictByValue(float value, int min, int max)
        {
            var random = GetRangeWithStrict(min, max);
            var result = random <= value;
            Debug($"Random result '{result}', random <= value '{random} <= {value}'", _settings.Debug);
            return result;
        }

        public bool GetBoolWithStrictByValue(double value, int min, int max)
        {
            var random = GetRangeWithStrict(min, max);
            var result = random <= value;
            Debug($"Random result '{result}', random <= value '{random} <= {value}'", _settings.Debug);
            return result;
        }

        public float GetAngle(float min, float max) =>
            GetRange(min, max);

        public Vector2 GetInsideUnitCircle(float radius)
        {
            var insideUnit = UnityEngine.Random.insideUnitCircle;
            var result = insideUnit * radius;
            Debug($"Random result '{result}', inside unit circle '{insideUnit}', radius '{radius}'", _settings.Debug);
            return result;
        }

        public Vector3 GetInsideUnitSphere(float radius)
        {
            var insideUnit = UnityEngine.Random.insideUnitSphere;
            var result = insideUnit * radius;
            Debug($"Random result '{result}', inside unit sphere '{insideUnit}', radius '{radius}'", _settings.Debug);
            return result;
        }

        public string GetUid()
        {
            var result = $"{Guid.NewGuid()}";
            Debug($"Random result '{result}'", _settings.Debug);
            return result;
        }

        public string GetUidWithPrefix(string prefix)
        {
            var result = $"{prefix}{Guid.NewGuid()}";
            Debug($"Random result '{result}'", _settings.Debug);
            return result;
        }

        public string GetName(int lengthName)
        {
            var result = "";

            for (int i = 0; i < lengthName; i++)
                result += _settings.Chars[GetRange(0, _settings.Chars.Length)];
            
            Debug($"Random result '{result}'", _settings.Debug);
            return result;
        }

        public string GetNameWithPrefix(string prefix, int lengthName)
        {
            var result = prefix;

            for (int i = 0; i < lengthName; i++)
                result += _settings.Chars[GetRange(0, _settings.Chars.Length)];
            
            Debug($"Random result '{result}'", _settings.Debug);
            return result;
        }

        public string GetNicknameFromList()
        {
            var result = _settings.Nicknames[GetRange(0, _settings.Nicknames.Length)];
            Debug($"Random result '{result}'", _settings.Debug);
            return result;
        }
    }
}