﻿namespace Savvy.Services.Social
{
    public enum SocialId
    {
        GooglePlay = 1,
        AppStore = 2,
        
        YouTube = 10,
        Telegram = 11,
        TikTok = 12,
        Vk = 13,
        Instagram = 14,
        Discord = 15
    }
}