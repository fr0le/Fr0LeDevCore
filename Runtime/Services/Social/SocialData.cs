﻿using System;
using UnityEngine;

namespace Savvy.Services.Social
{
    [Serializable]
    public struct SocialData
    {
        public SocialId SocialId;
        public string Url;
    }
}