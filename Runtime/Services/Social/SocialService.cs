﻿using Savvy.Extensions;
using Savvy.Services.Preferences;

namespace Savvy.Services.Social
{
    public class SocialService : ServiceBase, ISocialService
    {
        private readonly string _sourceSettings = $"{nameof(Savvy)}/{nameof(SocialSettings)}";
        private readonly SocialSettings _settings;

        private IPreferencesService _preferences;
        private bool _isOpenedByThisSession;
        private bool _isOpenedByThisUser;
        
        public SocialService() => 
            _settings = _sourceSettings.LoadResources<SocialSettings>();

        public override void Inject() => 
            _preferences = GetService<IPreferencesService>();

        public override void Init() => 
            _isOpenedByThisUser = _preferences.LoadBool(GetPrefsKey(nameof(_isOpenedByThisUser)));

        public bool TryOpenFeedback() => 
            !_isOpenedByThisSession && !_isOpenedByThisUser;

        public void SetOpenByThisSession() => 
            _isOpenedByThisSession = true;
        
        public void SetOpenByThisUser()
        {
            _isOpenedByThisUser = true;
            _preferences.SaveBool(GetPrefsKey(nameof(_isOpenedByThisUser)), _isOpenedByThisUser);
        }

        public void OpenMail()
        {
            _settings.FeedbackMail.OpenEmailClient(_settings.MailHeader);
            Debug($"Open mail to: '{_settings.FeedbackMail}'", _settings.Debug);
        }
        
        public void OpenStore()
        {
#if UNITY_ANDROID
            _settings.GooglePlayUrl.OpenURL();
            Debug($"Open google play: '{_settings.GooglePlayUrl}'", _settings.Debug);
#elif UNITY_IPHONE
            _settings.AppStoreUrl.OpenURL();
            Debug($"Open app store: '{_settings.AppStoreUrl}'", _settings.Debug);
#else
            _settings.GooglePlayUrl.OpenURL();
            Debug($"Open default store: '{_settings.GooglePlayUrl}'", _settings.Debug);
#endif
        }
        
        public void OpenUrlById(SocialId id)
        {
            foreach (var data in _settings.SocialData)
            {
                if (data.SocialId == id)
                {
                    data.Url.OpenURL();
                    Debug($"Open social url: '{data.Url}'", _settings.Debug);
                    return;
                }
            }
            
            Error($"'{id}' link not found");
        }

        private string GetPrefsKey(string name) => 
            _settings.PrefsKey + name;
    }
}