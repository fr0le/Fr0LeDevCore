﻿namespace Savvy.Services.Social
{
    public interface ISocialService : ISavvyService
    {
        bool TryOpenFeedback();
        void SetOpenByThisSession();
        void SetOpenByThisUser();
        void OpenMail();
        void OpenStore();
        void OpenUrlById(SocialId id);
    }
}