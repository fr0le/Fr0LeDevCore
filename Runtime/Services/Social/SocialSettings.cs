using Savvy.Infrastructure;
using UnityEngine;

namespace Savvy.Services.Social
{
    [CreateAssetMenu(menuName = nameof(Savvy) + "/" + nameof(SocialSettings), fileName = nameof(SocialSettings))]
    public class SocialSettings : ValidateScriptableObject, ISavvySettings
    {
        [Header("Logging")]
        public bool Debug = false;
        
        [Header("Save Load")]
        public string PrefsKey = "SocialSettings";
        
        [Header("Feedback")]
        public string FeedbackMail = "recipient@gmail.com";
        public string MailHeader = "Game Name Feedback";
        public string GooglePlayUrl = "https://play.google.com/store/apps/details?id=";
        public string AppStoreUrl = "https://apps.apple.com/";

        [Header("Settings")]
        public SocialData[] SocialData;
    }
}