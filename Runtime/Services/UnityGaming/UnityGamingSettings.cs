using Savvy.Infrastructure;
using UnityEngine;

namespace Savvy.Services.UnityGaming
{
    [CreateAssetMenu(menuName = nameof(Savvy) + "/" + nameof(UnityGamingSettings), fileName = nameof(UnityGamingSettings))]
    public class UnityGamingSettings : ValidateScriptableObject, ISavvySettings
    {
        [Header("Logging")] 
        public bool Debug = false;

        [Header("Settongs")] 
        public UnityGamingServiceEnvironment Environment = UnityGamingServiceEnvironment.production;
    }
}