using Savvy.Extensions;
#if SAVVY_UNITY_GAMING
using System;
using System.Threading.Tasks;
using Unity.Services.Core;
using Unity.Services.Core.Environments;
#else
using UnityEngine;
#endif

namespace Savvy.Services.UnityGaming
{
    public class UnityGamingService : ServiceBase, IUnityGamingService
    {
        private readonly string _sourceSettings = $"{nameof(Savvy)}/{nameof(UnityGamingSettings)}";
        private readonly UnityGamingSettings _settings;

        public UnityGamingService() => 
            _settings = _sourceSettings.LoadResources<UnityGamingSettings>();
        
#if SAVVY_UNITY_GAMING        
        public override async Task InitAsync() => 
            await LoadRemoteConfigAsync();

        private async Task LoadRemoteConfigAsync()
        {
            try
            {
                Debug($"Unity Gaming Services initialization", _settings.Debug);
                var options = new InitializationOptions().SetEnvironmentName(_settings.Environment.ToString());
                await UnityServices.InitializeAsync(options);
                Debug("Unity Gaming Services successfully initialized", _settings.Debug);
            }
            catch (Exception ex)
            {
                Error($"An error occurred during services initialization: {ex.Message}");
            }
        }
#else
        private void DebugNotSupport() =>
            Debug($"'{Application.platform}' is not supported in '{nameof(UnityGamingService)}'", _settings.Debug);
#endif
    }
}