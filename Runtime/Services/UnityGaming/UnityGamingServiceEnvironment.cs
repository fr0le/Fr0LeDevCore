namespace Savvy.Services.UnityGaming
{
    public enum UnityGamingServiceEnvironment
    {
        production = 0,
        development = 1,
        staging = 2
    }
}