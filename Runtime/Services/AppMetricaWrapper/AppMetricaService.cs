﻿using Savvy.Extensions;
using Savvy.Services.CAS;
using UnityEngine;
#if SAVVY_APP_METRICA
using System;
using Io.AppMetrica;
using Savvy.Services.Preferences;
#endif

namespace Savvy.Services.AppMetricaWrapper
{
    public class AppMetricaService : ServiceBase, IAppMetricaService
    {
        private readonly string _sourceSettings = $"{nameof(Savvy)}/{nameof(AppMetricaSettings)}";
        private readonly AppMetricaSettings _settings;
        
#if SAVVY_APP_METRICA
        private IPreferencesService _preferences;
#endif
        
        public AppMetricaService() =>
            _settings = _sourceSettings.LoadResources<AppMetricaSettings>();
        
#if SAVVY_APP_METRICA
        public override void Inject() =>
            _preferences = GetService<IPreferencesService>();

        public override void CheckSettings()
        {
            if (string.IsNullOrEmpty(_settings.PrefsKey))
                Error($"'{nameof(_settings.PrefsKey)}' cannot be null or empty. Correct the settings file '{_sourceSettings}'");

            if (string.IsNullOrEmpty(_settings.ApiKey))
                Error($"'{nameof(_settings.ApiKey)}' cannot be null or empty. Correct the settings file '{_sourceSettings}'");
        }

        public override void Init()
        {
            AppMetrica.Activate(new AppMetricaConfig(_settings.ApiKey)
            {
                CrashReporting = _settings.CrashReporting,
                SessionTimeout = _settings.SessionTimeout,
                LocationTracking = _settings.LocationTracking,
                Logs = _settings.Logs,
                FirstActivationAsUpdate = false,
                DataSendingEnabled = _settings.DataSendingEnabled
            });

            Debug("AppMetrica successfully initialized", _settings.Debug);
            Subscribe();
        }

        public override void Pause(bool isPause)
        {
            if (isPause)
                AppMetrica.PauseSession();
            else
                AppMetrica.ResumeSession();
        }

        public override void Destroy() =>
            Unsubscribe();

        public void SendFirstEvent(string name, string prefsKey) =>
            ReportFirstEvent(name, prefsKey);

        public void SendFirstEvent<TData>(TData data, string prefsKey, string name)
        {
            var eventName = string.IsNullOrEmpty(name) ? typeof(TData).Name : name;
            ReportFirstEvent(eventName, data.ToJson(), prefsKey);
        }

        public void SendEvent(string name) =>
            ReportEvent(name);

        public void SendEvent<TData>(TData data, string name = null)
        {
            var eventName = string.IsNullOrEmpty(name) ? typeof(TData).Name : name;
            ReportEvent(eventName, data.ToJson());
        }

        public void SendAdRevenue(AdRevenueData data)
        {
            AdRevenue adRevenue = new(data.revenue, data.currency)
            {
                AdType = GetAdType(data.ad_type),
                AdNetwork = data.network_name,
                AdUnitId = data.ad_unit_id,
                AdUnitName = null,
                AdPlacementId = null,
                AdPlacementName = data.placement,
                Precision = data.price_accuracy
            };

            AppMetrica.ReportAdRevenue(adRevenue);
            Debug($"Send AdRevenue", _settings.Debug);
            
            if(_settings.SendExperimentalAdRevenue)
                SendEvent(data, "c_ad_revenue");
        }
        
        private AdType GetAdType(string adTypeString)
        {
            foreach (AdType adType in Enum.GetValues(typeof(AdType)))
                if (adTypeString.Contains(adType.ToString(), StringComparison.OrdinalIgnoreCase))
                    return adType;

            return AdType.Other;
        }

        private void ReportFirstEvent(string name, string prefsKey)
        {
            var key = $"{_settings.PrefsKey}_{name}_{prefsKey}";

            if (!_preferences.HasKey(key))
            {
                if (ReportEvent(name))
                {
                    _preferences.SaveBool(key, true);
                }
            }
            else
            {
                Warning($"Canceling sending an first event. Event has already been sent previously. Prefs key '{key}'", _settings.DebugCanceledEvents);
            }
        }

        private void ReportFirstEvent(string name, string json, string prefsKey)
        {
            var key = $"{_settings.PrefsKey}_{name}_{prefsKey}";

            if (!_preferences.HasKey(key))
            {
                if (ReportEvent(name, json))
                {
                    _preferences.SaveBool(key, true);
                }
            }
            else
            {
                Warning($"Canceling sending an first event. Event has already been sent previously. Prefs key '{key}'", _settings.DebugCanceledEvents);
            }
        }

        private bool ReportEvent(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                Error($"Canceling sending an event. Name '{name}' is null");
                return false;
            }

            AppMetrica.ReportEvent(name);
            Debug($"Send event '{name}'", _settings.Debug);
            return true;
        }

        private bool ReportEvent(string name, string json)
        {
            if (string.IsNullOrEmpty(name))
            {
                Error($"Canceling sending an event. Name '{name}' is null");
                return false;
            }

            if (string.IsNullOrEmpty(json))
            {
                Error($"Canceling sending an event. Json '{json}' is null");
                return false;
            }

            if (!json.IsValidJson())
            {
                Error($"Canceling sending an event. Json '{json}' is not valid");
                return false;
            }

            AppMetrica.ReportEvent(name, json);
            Debug($"Send event '{name}', json '{json}'", _settings.Debug);
            return true;
        }

        private void Subscribe()
        {
            if (_settings.CrashReporting)
                Application.logMessageReceived += HandleLog;
        }

        private void Unsubscribe()
        {
            if (_settings.CrashReporting)
                Application.logMessageReceived -= HandleLog;
        }

        private void HandleLog(string condition, string stackTrace, LogType type)
        {
            if (type == LogType.Exception)
                AppMetrica.ReportError(condition, stackTrace);
        }
#else
        public void SendFirstEvent(string name, string prefsKey) =>
            DebugNotSupport();

        public void SendFirstEvent<TData>(TData data, string prefsKey, string name = null) =>
            DebugNotSupport();

        public void SendEvent(string name) =>
            DebugNotSupport();
        
        public void SendEvent<TData>(TData data, string name = null) =>
            DebugNotSupport();

        public void SendAdRevenue(AdRevenueData data) =>
            DebugNotSupport();
#endif
        
        private void DebugNotSupport() => 
            Debug($"'{Application.platform}' is not supported in '{nameof(AppMetricaService)}'", _settings.Debug);
    }
}