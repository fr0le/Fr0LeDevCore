﻿using Savvy.Infrastructure;
using UnityEngine;

namespace Savvy.Services.AppMetricaWrapper
{
    [CreateAssetMenu(menuName = nameof(Savvy) + "/" + nameof(AppMetricaSettings), fileName = nameof(AppMetricaSettings))]
    public class AppMetricaSettings : ValidateScriptableObject, ISavvySettings
    {
        [Header("Logging")]
        public bool Debug = false;
        public bool DebugCanceledEvents = false;
        
        [Header("Save Load")]
        public string PrefsKey = "AppMetrica";
        
        [Header("Settings")] 
        public string ApiKey;
        public bool CrashReporting = true;
        [Min(5)]
        public int SessionTimeout = 10;
        public bool LocationTracking = false;
        public bool Logs = false;
        public bool DataSendingEnabled = true;

        [Header("Experimental")]
        public bool SendExperimentalAdRevenue = false;
    }
}