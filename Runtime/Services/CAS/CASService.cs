﻿using System;
using Savvy.Extensions;
using UnityEngine;
#if SAVVY_CAS
using CAS;
using Savvy.Services.AppMetricaWrapper;
using Savvy.Services.Mediation;
using Savvy.Services.Preferences;
#endif

namespace Savvy.Services.CAS
{
    public class CASService : ServiceBase, ICASService
    {
        private readonly string _sourceSettings = $"{nameof(Savvy)}/{nameof(CASSettings)}";
        private readonly CASSettings _settings;

        public event Action OnBannerAdLoaded;
        public event Action<bool> OnInterstitialAdLoaded;
        public event Action<bool> OnRewardedAdLoaded;

#if SAVVY_CAS
        private IMediationManager _manager;
        private IAppMetricaService _appMetrica;
        private IPreferencesService _preferences;
        private IAdView _adView;

        private Action _rewardedAdCompleted;
        private Action _rewardedAdClosed;
        private string _placementInterstitialAd;
        private string _placementRewardedAd;
        private bool _isRewardedComplete;
        private int _bannerAdCount;
        private int _interstitialAdCount;
        private int _rewardedAdCount;
        public bool BannerAdIsReady => _adView.isReady;
        public bool InterstitialAdIsReady => _manager.IsReadyAd(AdType.Interstitial);
        public bool RewardedAdIsReady => _manager.IsReadyAd(AdType.Rewarded);
#else
        public bool BannerAdIsReady { get; }
        public bool InterstitialAdIsReady { get; }
        public bool RewardedAdIsReady { get; }
#endif

        public CASService() =>
            _settings = _sourceSettings.LoadResources<CASSettings>();

#if SAVVY_CAS
        public override void Inject()
        {
            _appMetrica = GetService<IAppMetricaService>();
            _preferences = GetService<IPreferencesService>();
        }

        public override void CheckSettings()
        {
            if (string.IsNullOrEmpty(_settings.Currency))
                Error($"'{nameof(_settings.Currency)}' cannot be null or empty. Correct the settings file '{_sourceSettings}'");
        }

        public override void Init()
        {
            _bannerAdCount = _preferences.LoadInt(GetPrefsKey(nameof(_bannerAdCount)));
            _interstitialAdCount = _preferences.LoadInt(GetPrefsKey(nameof(_interstitialAdCount)));
            _rewardedAdCount = _preferences.LoadInt(GetPrefsKey(nameof(_rewardedAdCount)));
            
            _manager = MobileAds.BuildManager().Build();
            _adView = _manager.GetAdView(_settings.BannerType.ToEnumOrDefault(AdSize.Banner));
            _adView.position = _settings.BannerPosition.ToEnumOrDefault(AdPosition.TopCenter);
            Debug("CAS successfully initialized", _settings.Debug);

            Subscribe();
        }

        public override void Destroy() =>
            Unsubscribe();

        public void SetInterstitialInterval(int interval)
        {
            MobileAds.settings.interstitialInterval = interval;
            Debug($"Set interstitial interval 'interval'", _settings.Debug);
        }

        public void ShowBannerAd()
        {
            if (BannerAdIsReady)
            {
                _adView.SetActive(true);
                Debug("Show banner Ad", _settings.Debug);
            }
            else
            {
                Error($"Banner {nameof(AdError.NotReady)}");
            }
        }

        public void HideBannerAd()
        {
            _adView.SetActive(false);
            Debug("Hide banner Ad", _settings.Debug);
        }

        public void ShowInterstitialAd(string placement)
        {
            if (InterstitialAdIsReady)
            {
                _placementInterstitialAd = $"{placement}_{AdType.Interstitial}";
                Debug($"Show interstitial Ad, source '{_placementInterstitialAd}'", _settings.Debug);
                _manager.ShowAd(AdType.Interstitial);
            }
            else
            {
                Error($"Interstitial {nameof(AdError.NotReady)}");
            }
        }

        public void ShowRewardedAd(Action rewardedAdCompleted, Action rewardedAdClosed, string placement)
        {
            if (RewardedAdIsReady)
            {
                _rewardedAdCompleted = rewardedAdCompleted;
                _rewardedAdClosed = rewardedAdClosed;
                _placementRewardedAd = $"{placement}_{AdType.Rewarded}";
                _isRewardedComplete = false;

                Debug($"Show rewarded Ad, source '{_placementRewardedAd}'", _settings.Debug);
                _manager.ShowAd(AdType.Rewarded);
            }
            else
            {
                Error($"Rewarded {nameof(AdError.NotReady)}");
            }
        }

        private void Subscribe()
        {
            if (_manager == null)
                return;

            _adView.OnLoaded += HandleAdViewLoaded;
            _adView.OnFailed += HandleAdViewFailedToLoad;
            _adView.OnClicked += HandleAdViewClicked;
            _adView.OnImpression += HandleAdViewImpression;

            _manager.OnInterstitialAdLoaded += HandleInterstitialAdLoaded;
            _manager.OnInterstitialAdFailedToLoad += HandleInterstitialAdFailedToLoad;
            _manager.OnInterstitialAdShown += HandleInterstitialAdShown;
            _manager.OnInterstitialAdFailedToShow += HandleInterstitialAdFailedToShow;
            _manager.OnInterstitialAdClicked += HandleInterstitialAdClicked;
            _manager.OnInterstitialAdClosed += HandleInterstitialAdClosed;
            _manager.OnInterstitialAdImpression += HandleInterstitialAdImpression;

            _manager.OnRewardedAdCompleted += HandleRewardedAdCompleted;
            _manager.OnRewardedAdLoaded += HandleRewardedAdLoaded;
            _manager.OnRewardedAdFailedToLoad += HandleRewardedAdFailedToLoad;
            _manager.OnRewardedAdShown += HandleRewardedAdShown;
            _manager.OnRewardedAdFailedToShow += HandleRewardedAdFailedToShow;
            _manager.OnRewardedAdClicked += HandleRewardedAdClicked;
            _manager.OnRewardedAdClosed += HandleRewardedAdClosed;
            _manager.OnRewardedAdImpression += HandleRewardedAdImpression;

            _manager.OnAppReturnAdImpression += HandleAppReturnAdImpression;
        }

        private void Unsubscribe()
        {
            if (_manager == null)
                return;

            _adView.OnLoaded -= HandleAdViewLoaded;
            _adView.OnFailed -= HandleAdViewFailedToLoad;
            _adView.OnClicked -= HandleAdViewClicked;
            _adView.OnImpression -= HandleAdViewImpression;

            _manager.OnInterstitialAdLoaded -= HandleInterstitialAdLoaded;
            _manager.OnInterstitialAdFailedToLoad -= HandleInterstitialAdFailedToLoad;
            _manager.OnInterstitialAdShown -= HandleInterstitialAdShown;
            _manager.OnInterstitialAdFailedToShow -= HandleInterstitialAdFailedToShow;
            _manager.OnInterstitialAdClicked -= HandleInterstitialAdClicked;
            _manager.OnInterstitialAdClosed -= HandleInterstitialAdClosed;
            _manager.OnInterstitialAdImpression -= HandleInterstitialAdImpression;

            _manager.OnRewardedAdCompleted -= HandleRewardedAdCompleted;
            _manager.OnRewardedAdLoaded -= HandleRewardedAdLoaded;
            _manager.OnRewardedAdFailedToLoad -= HandleRewardedAdFailedToLoad;
            _manager.OnRewardedAdShown -= HandleRewardedAdShown;
            _manager.OnRewardedAdFailedToShow -= HandleRewardedAdFailedToShow;
            _manager.OnRewardedAdClicked -= HandleRewardedAdClicked;
            _manager.OnRewardedAdClosed -= HandleRewardedAdClosed;
            _manager.OnRewardedAdImpression -= HandleRewardedAdImpression;

            _manager.OnAppReturnAdImpression -= HandleAppReturnAdImpression;
        }

        private void HandleAdViewLoaded(IAdView view)
        {
            OnBannerAdLoaded?.Invoke();
            Debug($"Banner Ad '{_manager.GetAdView(view.size)}' loaded", _settings.Debug);

            if (_settings.ShowAfterLoad)
                ShowBannerAd();
        }

        private void HandleAdViewFailedToLoad(IAdView view, AdError error) =>
            Debug($"Banner failed to load an Ad '{_manager.GetAdView(view.size)}' with error '{error.GetMessage()}'",
                _settings.Debug);

        private void HandleAdViewClicked(IAdView view) =>
            Debug("Banner Ad was clicked", _settings.Debug);

        private void HandleAdViewImpression(IAdView view, AdMetaData impression) => 
            SendAdRevenue(impression);

        private void HandleInterstitialAdLoaded()
        {
            OnInterstitialAdLoaded?.Invoke(true);
            Debug("Interstitial Ad loaded", _settings.Debug);
        }

        private void HandleInterstitialAdFailedToLoad(AdError error) =>
            Debug($"Interstitial failed to load an Ad with error '{error.GetMessage()}'", _settings.Debug);

        private void HandleInterstitialAdShown()
        {
            OnInterstitialAdLoaded?.Invoke(false);
            Debug("Interstitial Ad full screen content opened", _settings.Debug);
        }

        private void HandleInterstitialAdFailedToShow(string error) =>
            Error($"Interstitial Ad failed to open full screen content '{error}'");

        private void HandleInterstitialAdClicked() =>
            Debug("Interstitial Ad was clicked", _settings.Debug);

        private void HandleInterstitialAdClosed() =>
            Debug("Interstitial Ad full screen content closed", _settings.Debug);

        private void HandleInterstitialAdImpression(AdMetaData impression) => 
            SendAdRevenue(impression);

        private void HandleRewardedAdCompleted()
        {
            _isRewardedComplete = true;
            _rewardedAdCompleted?.Invoke();
            Debug("The user earned the reward", _settings.Debug);
        }

        private void HandleRewardedAdLoaded()
        {
            OnRewardedAdLoaded?.Invoke(true);
            Debug("Rewarded Ad loaded", _settings.Debug);
        }

        private void HandleRewardedAdFailedToLoad(AdError error) =>
            Debug($"Rewarded failed to load an Ad with error '{error.GetMessage()}'", _settings.Debug);

        private void HandleRewardedAdShown()
        {
            OnRewardedAdLoaded?.Invoke(false);
            Debug("Rewarded Ad full screen content opened", _settings.Debug);
        }

        private void HandleRewardedAdFailedToShow(string error) =>
            Error($"Rewarded Ad failed to open full screen content '{error}'");

        private void HandleRewardedAdClicked() =>
            Debug("Rewarded Ad was clicked", _settings.Debug);

        private void HandleRewardedAdClosed()
        {
            if (!_isRewardedComplete)
            {
                _rewardedAdClosed?.Invoke();
                Debug("The user has not earned the reward", _settings.Debug);
            }

            Debug("Rewarded Ad full screen content closed", _settings.Debug);
        }

        private void HandleRewardedAdImpression(AdMetaData impression) => 
            SendAdRevenue(impression);

        private void HandleAppReturnAdImpression(AdMetaData impression) => 
            SendAdRevenue(impression);

        private void SendAdRevenue(AdMetaData impression)
        {
            if (_settings.SendAdRevenueToAppMetrica) 
                _appMetrica.SendAdRevenue(GetCASAdRevenueData(impression));

            if (_settings.SendAdCountDataToAppMetrica)
                SendAdCount(impression);
        }

        private AdRevenueData GetCASAdRevenueData(AdMetaData impression) => new()
        {
            currency = _settings.Currency,
            ad_type = impression.type.ToString(),
            placement = GetPlacement(impression.type),
            cur_ticks = System.DateTime.Now.Ticks,
            session_length = Time.realtimeSinceStartup,
            cpm = impression.cpm,
            revenue = impression.revenue,
            value = impression.revenue,
            ads_ltv = GetLifetimeRevenue(impression.revenue),
            ad_country_code = "",
            network_name = impression.network.ToString(),
            ad_unit_id = impression.identifier,
            ad_network_placement = "",
            creativeId = impression.creativeIdentifier
        };

        private void SendAdCount(AdMetaData impression)
        {
            var adType = impression.type;
            
            switch (adType)
            {
                case AdType.Banner:
                    _bannerAdCount++;
                    _preferences.SaveInt(GetPrefsKey(nameof(_bannerAdCount)), _bannerAdCount);
                    _appMetrica.SendEvent(GetBannerAdData());
                    break;
                case AdType.Interstitial:
                    _interstitialAdCount++;
                    _preferences.SaveInt(GetPrefsKey(nameof(_interstitialAdCount)), _interstitialAdCount);
                    _appMetrica.SendEvent(GetInterstitialAdData());
                    break;
                case AdType.Rewarded:
                    _rewardedAdCount++;
                    _preferences.SaveInt(GetPrefsKey(nameof(_rewardedAdCount)), _rewardedAdCount);
                    _appMetrica.SendEvent(GetRewardedAdData());
                    break;
                default:
                    adType.ToEnumUnknown();
                    break;
            }
        }

        private BannerAdData GetBannerAdData() => new()
        {
            Count = _bannerAdCount
        };

        private InterstitialAdData GetInterstitialAdData() => new()
        {
            Count = _interstitialAdCount
        };

        private RewardedAdData GetRewardedAdData() => new()
        {
            Count = _rewardedAdCount
        };

        private string GetPlacement(AdType type)
        {
            switch (type)
            {
                case AdType.Banner:
                    return AdType.Banner.ToString();
                case AdType.Interstitial:
                    return _placementInterstitialAd;
                case AdType.Rewarded:
                    return _placementRewardedAd;
                default:
                    type.ToEnumUnknown();
                    return "unknown";
            }
        }

        private double GetLifetimeRevenue(double adRevenue)
        {
            var revenue = Math.Max(0f, adRevenue);
            var key = GetPrefsKey(nameof(AdRevenueData.ads_ltv));
            var lifetimeRevenue = _preferences.LoadDouble(key);
            lifetimeRevenue += revenue;

            _preferences.SaveDouble(key, lifetimeRevenue);
            return lifetimeRevenue;
        }

        private string GetPrefsKey(string name) =>
            _settings.PrefsKey + name;
#else
        public void SetInterstitialInterval(int interval) =>
            DebugNotSupport();

        public void ShowBannerAd() => 
            DebugNotSupport();

        public void HideBannerAd() => 
            DebugNotSupport();

        public void ShowInterstitialAd(string placement) => 
            DebugNotSupport();

        public void ShowRewardedAd(Action rewardedAdCompleted, Action rewardedAdAction, string placement) => 
            DebugNotSupport();
#endif

        private void DebugNotSupport() =>
            Debug($"'{Application.platform}' is not supported in '{nameof(CASService)}'", _settings.Debug);
    }
}