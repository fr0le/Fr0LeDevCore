﻿using System;

namespace Savvy.Services.CAS
{
    [Serializable]
    public struct AdRevenueData
    {
        public string currency;
        public double cpm;
        public string ad_type;
        public string placement;
        public long cur_ticks;
        public float session_length;
        public double revenue;
        public double value;
        public double ads_ltv;
        public string ad_country_code;
        public string network_name;
        public string ad_unit_id;
        public string ad_network_placement;
        public string creativeId;
        public string price_accuracy;
    }
}