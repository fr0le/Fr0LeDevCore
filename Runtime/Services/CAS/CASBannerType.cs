﻿namespace Savvy.Services.CAS
{
    public enum CASBannerType
    {
        Banner = 1,
        AdaptiveBanner = 2,
        SmartBanner = 3,
        Leaderboard = 4,
        MediumRectangle = 5,
        AdaptiveFullWidth = 6,
        ThinBanner = 7
    }
}