﻿namespace Savvy.Services.CAS
{
    public enum CASBannerPosition
    {
        TopCenter = 1,
        TopLeft = 2,
        TopRight = 3,
        BottomCenter = 4,
        BottomLeft = 5,
        BottomRight = 6,
        MiddleCenter = 7,
        MiddleLeft = 8,
        MiddleRight = 9,
        Undefined = 10
    }
}