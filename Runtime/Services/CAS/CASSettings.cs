﻿using Savvy.Infrastructure;
using UnityEngine;

namespace Savvy.Services.CAS
{
    [CreateAssetMenu(menuName = nameof(Savvy) + "/" + nameof(CASSettings), fileName = nameof(CASSettings))]
    public class CASSettings : ValidateScriptableObject, ISavvySettings
    {
        [Header("Logging")] 
        public bool Debug = false;
        
        [Header("Save Load")]
        public string PrefsKey = "CASSettings";
        
        [Header("Banner")]
        public CASBannerType BannerType = CASBannerType.Banner;
        public CASBannerPosition BannerPosition = CASBannerPosition.TopCenter;
        public bool ShowAfterLoad = false;
        
        [Header("Ad Revenue")]
        public string Currency = "USD";
        
        [Header("Metrics")] 
        public bool SendAdRevenueToAppMetrica = false;
        public bool SendAdCountDataToAppMetrica = false;
    }
}