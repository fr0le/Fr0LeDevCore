using Savvy.Services.CAS;

namespace Savvy.Services.Metrics
{
    public interface IMetricsService : ISavvyService
    {
        void SendFirstEvent(string name, string prefsKey);
        void SendFirstEvent<TData>(TData data, string prefsKey, string name = null);
        void SendEvent(string name);
        void SendEvent<TData>(TData data, string name = null);
        void SendAdRevenue(AdRevenueData data);
    }
}