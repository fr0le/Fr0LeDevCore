﻿using System.Threading.Tasks;
using Savvy.Infrastructure;

namespace Savvy.Services
{
    public abstract class ServiceBase : NetSavvy
    {
        public virtual void Inject() { }
        public virtual void CheckSettings() { }
        public virtual void Init() { }

        public virtual Task InitAsync()
        {
            Init();
            return Task.CompletedTask;
        }

        public virtual void Pause(bool isPause) { }
        public virtual void Destroy() { }
    }
}