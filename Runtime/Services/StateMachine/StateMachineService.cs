﻿using System;
using System.Collections.Generic;
using Savvy.Extensions;

namespace Savvy.Services.StateMachine
{
    public class StateMachineService : ServiceBase, IStateMachineService
    {
        private readonly string _sourceSettings = $"{nameof(Savvy)}/{nameof(StateMachineSettings)}";
        private readonly StateMachineSettings _settings;
        private readonly Dictionary<Type, IExitableState> _states = new();
        
        private IExitableState _activeState;
        
        public StateMachineService() => 
            _settings = _sourceSettings.LoadResources<StateMachineSettings>();

        public void AddState(Type type, IExitableState state)
        {
            _states.Add(type, state);
            Debug($"Add '{type.Name}' state to list. Count states: {_states.Count}", _settings.Debug);
        }

        public void Enter<TState>() where TState : class, IState
        { 
            Debug($"Enter '{typeof(TState).Name}' state", _settings.Debug);
            ChangeState<TState>().Enter();
        }

        public void Enter<TState, TPayload>(TPayload payload) where TState : class, IState<TPayload>
        { 
            Debug($"Enter '{typeof(TState).Name}' state", _settings.Debug);
            ChangeState<TState>().Enter(payload);
        }

        private TState ChangeState<TState>() where TState : class
        {
            _activeState?.Exit();
            var state = GetState<TState>();
            _activeState = state;
            return state as TState;
        }

        private IExitableState GetState<TState>()
        {
            var state = _states[typeof(TState)];
            
            if (state == null)
                Error($"State '{typeof(TState).Name}' is not found");
            
            return state;
        }
    }
}