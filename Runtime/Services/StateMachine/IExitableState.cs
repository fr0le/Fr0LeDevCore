namespace Savvy.Services.StateMachine
{
    public interface IExitableState
    {
        void Exit();
    }
}