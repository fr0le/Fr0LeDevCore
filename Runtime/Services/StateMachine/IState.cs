﻿namespace Savvy.Services.StateMachine
{
    public interface IState : IExitableState
    {
        void Enter();
    }
    
    public interface IState<TPayload> : IExitableState
    {
        void Enter(TPayload payload);
    }
}