﻿using Savvy.Infrastructure;
using UnityEngine;

namespace Savvy.Services.StateMachine
{
    [CreateAssetMenu(menuName = nameof(Savvy) + "/" + nameof(StateMachineSettings), fileName = nameof(StateMachineSettings))]
    public class StateMachineSettings : ValidateScriptableObject, ISavvySettings
    {
        [Header("Logging")]
        public bool Debug = false;
    }
}