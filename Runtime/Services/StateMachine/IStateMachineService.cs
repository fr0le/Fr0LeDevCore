using System;

namespace Savvy.Services.StateMachine
{
    public interface IStateMachineService : ISavvyService
    {
        void AddState(Type type, IExitableState state);
        void Enter<TState>() where TState : class, IState;
        void Enter<TState, TPayload>(TPayload payload) where TState : class, IState<TPayload>;
    }
}