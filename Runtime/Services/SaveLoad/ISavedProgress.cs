namespace Savvy.Services.SaveLoad
{
    public interface ISavedProgress<TProgressData>
    {
        void UpdateProgress(ref TProgressData data);
    }
}