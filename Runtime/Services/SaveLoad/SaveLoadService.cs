﻿using System.Collections;
using System.Collections.Generic;
using Savvy.Extensions;
using Savvy.Services.CoroutineRunner;
using Savvy.Services.Preferences;
using UnityEngine;

namespace Savvy.Services.SaveLoad
{
    public class SaveLoadService<TProgressData> : ServiceBase, ISaveLoadService<TProgressData>
    {
        private readonly string _sourceSettings = $"{nameof(Savvy)}/{nameof(SaveLoadSettings)}";
        private readonly SaveLoadSettings _settings;
        private readonly List<ISavedProgress<TProgressData>> _savedProgress = new();
        
        private IPreferencesService _preferences;
        private ICoroutineRunnerService _coroutineRunner;
        private Coroutine _autoSaver;
        private TProgressData _progressData;
        private bool _isAutoSave;
        private bool _isAllowedSave;

        public SaveLoadService() => 
            _settings = _sourceSettings.LoadResources<SaveLoadSettings>();

        public override void Inject()
        {
            _preferences = GetService<IPreferencesService>();             
            _coroutineRunner = GetService<ICoroutineRunnerService>();
        }

        public override void CheckSettings()
        {
            if (string.IsNullOrEmpty(_settings.PrefsKey))
                Error($"'{nameof(_settings.PrefsKey)}' cannot be null or empty. Correct the settings file '{_sourceSettings}'");
        }

        public override void Init()
        {
            _progressData = LoadProgress();
            _isAutoSave = _settings.IsAutoSave;
            _autoSaver = _coroutineRunner.StartCoroutine(AutoSaver());
        }

        public override void Pause(bool isPause)
        {
            if (isPause) 
                SaveProgress(SaveType.Pause);
        }

        public override void Destroy()
        {
            SaveProgress(SaveType.Destroy);
            _coroutineRunner.StopCoroutine(_autoSaver);
        }

        public TProgressData GetProgressData() => 
            _progressData;

        public void SaveProgress(SaveType saveType)
        {
            if (!_isAllowedSave)
            {
                Error("Save is not allowed");
                return;
            }
            
            foreach (var progress in _savedProgress)
            {
                progress.UpdateProgress(ref _progressData);
                Debug($"Save '{progress.GetType().Name}'", _settings.Debug);
            }
            
            _preferences.SaveJson(_settings.PrefsKey, _progressData);
            Debug($"'{saveType}' Save in PlayerPrefs '{_progressData.ToJson()}'", _settings.Debug);
        }

        public void SetAllowedSave(bool isAllowedSave)
        {
            _isAllowedSave = isAllowedSave;
            Debug($"Set allowed save '{isAllowedSave}'", _settings.Debug);
        }

        public void SetAutoSave(bool isAutoSave)
        {
            _isAutoSave = isAutoSave;
            Debug($"Set auto save '{isAutoSave}'", _settings.Debug);
        }

        public void AddToSavedList(IService service)
        {
            var savedProgress = service as ISavedProgress<TProgressData>;

            if (savedProgress != null)
            {
                _savedProgress.Add(savedProgress);
                Debug($"'{service.GetType().Name}' added to 'Saved List'. Count list: '{_savedProgress.Count}'", _settings.Debug);
            }
        }

        public void RemoveFromSavedList(IService service)
        {
            var savedProgress = service as ISavedProgress<TProgressData>;
            
            if (savedProgress != null)
            {
                _savedProgress.Remove(savedProgress);
                Debug($"'{service.GetType().Name}' removed from 'Saved List'. Count list: '{_savedProgress.Count}'", _settings.Debug);
            }
        }

        private TProgressData LoadProgress()
        {
            var loadData = _preferences.LoadJson<TProgressData>(_settings.PrefsKey);
            Debug($"Load from PlayerPrefs '{loadData.ToJson()}'", _settings.Debug);
            return loadData;
        }

        private IEnumerator AutoSaver()
        {
            while (true)
            {
                yield return new WaitForSeconds(_settings.AutoSaveTimer);
                
                if (_isAutoSave)
                    SaveProgress(SaveType.Auto);
            }
        }
    }
}