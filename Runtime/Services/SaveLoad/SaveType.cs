﻿namespace Savvy.Services.SaveLoad
{
    public enum SaveType
    {
        Auto = 0,
        Manual = 1,
        Pause = 2,
        Quit = 3,
        Destroy = 4
    }
}