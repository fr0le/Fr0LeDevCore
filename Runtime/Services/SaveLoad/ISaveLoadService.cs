namespace Savvy.Services.SaveLoad
{
    public interface ISaveLoadService : ISavvyService
    {       
        void AddToSavedList(IService saveable);
        void RemoveFromSavedList(IService saveable);
        void SaveProgress(SaveType saveType); 
        void SetAllowedSave(bool isAllowedSave);
        void SetAutoSave(bool isAutoSave);
    }

    public interface ISaveLoadService<TProgressData> : ISaveLoadService
    {
        TProgressData GetProgressData();
    }
}