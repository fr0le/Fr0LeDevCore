﻿using Savvy.Infrastructure;
using UnityEngine;

namespace Savvy.Services.SaveLoad
{
    [CreateAssetMenu(menuName = nameof(Savvy) + "/" + nameof(SaveLoadSettings), fileName = nameof(SaveLoadSettings))]
    public class SaveLoadSettings : ValidateScriptableObject, ISavvySettings
    {
        [Header("Logging")]
        public bool Debug = false;
        
        [Header("Save Load")]
        public string PrefsKey = "ProgressData";
        
        [Header("Settings")]
        public bool IsAutoSave = true;
        [Min(5)]
        public uint AutoSaveTimer = 30;
    }
}