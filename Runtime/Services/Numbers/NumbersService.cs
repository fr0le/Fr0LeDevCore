﻿using System.Collections.Generic;
using Savvy.Extensions;

namespace Savvy.Services.Numbers
{
    public class NumbersService : ServiceBase, INumbersService
    {
        private readonly string _sourceSettings = $"{nameof(Savvy)}/{nameof(NumbersSettings)}";
        private readonly NumbersSettings _settings;
        private readonly List<string> _names = new();

        public NumbersService() => 
            _settings = _sourceSettings.LoadResources<NumbersSettings>();

        public override void CheckSettings()
        {
            if (_settings.Names == null || _settings.Names.Length == 0)
                Error($"'{nameof(_settings.Names)}' cannot be null or empty. Correct the settings file '{_sourceSettings}'");
            
            if (_settings.Alphabet == null || _settings.Alphabet.Length == 0)
                Error($"'{nameof(_settings.Alphabet)}' cannot be null or empty. Correct the settings file '{_sourceSettings}'");
        }

        public override void Init() => 
            CreateNameList();

        public string GetFormat(double value)
        {
            if (value < 1000)
            {
                switch (value)
                {
                    case > 99 and < 1000:
                        value = value.ToRound();
                        break;
                    case > 9 and < 100:
                        value = value.ToRound(1);
                        break;
                    default:
                        value = value.ToRound(2);
                        break;
                }

                if (value % 1 == 0)
                    return $"{value:N0}";

                if (value * 10 % 1 == 0)
                    return $"{value:N1}";

                return $"{value:N2}";
            }

            var i = 0;
            double result = 0;

            while (value >= 1000)
            {
                if (value < 1000 * 1000)
                {
                    result = value / 1000;
                    value = 0;
                }
                else
                {
                    value /= 1000;
                }

                i++;
            }

            if (i < _names.Count)
                return $"{result:N2}{_names[i]}";
            else
                return $"{result:N2}e{i * 3}";
        }

        private void CreateNameList()
        {
            foreach (var name in _settings.Names) 
                _names.Add(name);

            foreach (var char1 in _settings.Alphabet)
            foreach (var char2 in _settings.Alphabet)
                _names.Add(char1 + char2);
            
            Debug($"Format nums list created, '{_names.Count}' chars", _settings.Debug);
        }
    }
}