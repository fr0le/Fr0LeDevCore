﻿namespace Savvy.Services.Numbers
{
    public interface INumbersService : ISavvyService
    {
        string GetFormat(double value);
    }
}