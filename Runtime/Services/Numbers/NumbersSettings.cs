﻿using Savvy.Infrastructure;
using UnityEngine;

namespace Savvy.Services.Numbers
{
    [CreateAssetMenu(menuName = nameof(Savvy) + "/" + nameof(NumbersSettings), fileName = nameof(NumbersSettings))]
    public class NumbersSettings : ValidateScriptableObject, ISavvySettings
    {
        [Header("Logging")] 
        public bool Debug = false;
        
        [Header("Settings")]
        public string[] Names =
        {
            "",
            "K",
            "M",
            "B",
            "T"
        };

        public string[] Alphabet = 
        {
            "a",
            "b",
            "c",
            "d",
            "e",
            "f",
            "g",
            "h",
            "i",
            "j",
            "k",
            "l",
            "m",
            "n",
            "o",
            "p",
            "q",
            "r",
            "s",
            "t",
            "u",
            "v",
            "w",
            "x",
            "y",
            "z"
        };
    }
}