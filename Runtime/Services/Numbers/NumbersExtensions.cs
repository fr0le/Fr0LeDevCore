﻿using System;

namespace Savvy.Services.Numbers
{
    public static class NumbersExtensions
    {
        public static double GetPercentage(this double percentage) => 
            1 + percentage / 100;
        
        public static double GetPercentageOfNumber(this double percentage) => 
            percentage / 100;
        
        public static double ToRound(this double num, int digits = 0) => 
            Math.Round(num, digits);
        
        public static string ToNumsFormat(this int num) => 
            GetFormat(num);

        public static string ToNumsFormat(this double num) => 
            GetFormat(num);

        private static string GetFormat(double num)
        {
            var numbers = Service.Container.GetSingle<INumbersService>();
            return numbers.GetFormat(num);
        }
    }
}