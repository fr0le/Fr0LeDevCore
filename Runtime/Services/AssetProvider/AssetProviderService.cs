﻿using Savvy.Extensions;
#if SAVVY_ASSET_PROVIDER
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
#else
using UnityEngine;
#endif

namespace Savvy.Services.AssetProvider
{
    public class AssetProviderService : ServiceBase, IAssetProviderService
    {
        private readonly string _sourceSettings = $"{nameof(Savvy)}/{nameof(AssetProviderSettings)}";
        private readonly AssetProviderSettings _settings;

#if SAVVY_ASSET_PROVIDER
        private readonly Dictionary<string, AsyncOperationHandle> _completedCache = new();
        private readonly Dictionary<string, List<AsyncOperationHandle>> _handles = new();
#endif
        
        public AssetProviderService() => 
            _settings = _sourceSettings.LoadResources<AssetProviderSettings>();
        
#if SAVVY_ASSET_PROVIDER        
        public override void Init() => 
            Addressables.InitializeAsync();

        public async Task<TObject> LoadAssetAsync<TObject>(AssetReference assetReference) where TObject : class
        {
            if (_completedCache.TryGetValue(assetReference.AssetGUID, out var completedHandle))
            {
#if UNITY_EDITOR
                Debug($"Get asset '{assetReference.editorAsset.name}' by reference", _settings.Debug);
#else
                Debug($"Get asset '{assetReference.AssetGUID}' by reference", _settings.Debug);
#endif
                return completedHandle.Result as TObject;
            }
            
#if UNITY_EDITOR
            Debug($"Load asset '{assetReference.editorAsset.name}' by reference", _settings.Debug);
#else
            Debug($"Load asset '{assetReference.AssetGUID}' by reference", _settings.Debug);
#endif
            return await RunWithCacheOnCompleteAsync(Addressables.LoadAssetAsync<TObject>(assetReference), assetReference.AssetGUID);
        }

        public async Task<TObject> LoadAssetAsync<TObject>(string assetName) where TObject : class
        {
            if (_completedCache.TryGetValue(assetName, out var completedHandle))
            {
                Debug($"Get asset '{assetName}' by reference", _settings.Debug);
                return completedHandle.Result as TObject;
            }

            Debug($"Load asset '{assetName}' by name", _settings.Debug);
            return await RunWithCacheOnCompleteAsync(Addressables.LoadAssetAsync<TObject>(assetName), assetName);
        }

        /*public Task<GameObject> InstantiateAsync(object key)
        {
            Debug($"Instantiate '{key.GetType().Name}'", _settings.Debug);
            return Addressables.InstantiateAsync(key).Task;
        }

        public Task<GameObject> InstantiateAsync(object key, Transform parent)
        {
            Debug($"Instantiate '{key.GetType().Name}' on '{parent.position}'", _settings.Debug);
            return Addressables.InstantiateAsync(key, parent).Task;
        }

        public Task<GameObject> InstantiateAsync(object key, Transform parent, Vector3 position, Quaternion rotation)
        {
            Debug($"Instantiate '{key.GetType().Name}' on '{position}'", _settings.Debug);
            return Addressables.InstantiateAsync(key, position, rotation).Task;
        }*/
        
        public void CleanUp()
        {
            foreach (var resourceHandles in _handles.Values)
            foreach (var handle in resourceHandles) 
                Addressables.Release(handle);
            
            _completedCache.Clear();
            _handles.Clear();
            Debug("Cleared asset cache", _settings.Debug);
        }

        private async Task<T> RunWithCacheOnCompleteAsync<T>(AsyncOperationHandle<T> handle, string key)
        {
            handle.Completed += completeHandle => _completedCache[key] = completeHandle;
            AddHandle(handle, key);
            return await handle.Task;
        }

        private void AddHandle(AsyncOperationHandle handle, string key)
        {
            if (!_handles.TryGetValue(key, out var resourceHandles))
            {
                resourceHandles = new List<AsyncOperationHandle>();
                _handles[key] = resourceHandles;
            }

            resourceHandles.Add(handle);
        }
#else
        private void DebugNotSupport() => 
            Debug($"'{Application.platform}' is not supported in '{nameof(AssetProviderService)}'", _settings.Debug);  
#endif
    }
}