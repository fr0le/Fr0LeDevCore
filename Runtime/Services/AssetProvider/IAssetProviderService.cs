﻿#if SAVVY_ASSET_PROVIDER
using System.Threading.Tasks;
using UnityEngine.AddressableAssets;
#endif

namespace Savvy.Services.AssetProvider
{
    public interface IAssetProviderService : ISavvyService
    {
#if SAVVY_ASSET_PROVIDER
        Task<TObject> LoadAssetAsync<TObject>(AssetReference assetReference) where TObject : class;
        Task<TObject> LoadAssetAsync<TObject>(string assetName) where TObject : class;
        //Task<GameObject> InstantiateAsync(object key);
        //Task<GameObject> InstantiateAsync(object key, Transform parent);
        //Task<GameObject> InstantiateAsync(object key, Transform parent, Vector3 position, Quaternion rotation);
        void CleanUp();
#endif
    }
}