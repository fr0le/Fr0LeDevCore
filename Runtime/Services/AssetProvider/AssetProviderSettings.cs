﻿using Savvy.Infrastructure;
using UnityEngine;

namespace Savvy.Services.AssetProvider
{
    [CreateAssetMenu(menuName = nameof(Savvy) + "/" + nameof(AssetProviderSettings), fileName = nameof(AssetProviderSettings))]
    public class AssetProviderSettings : ValidateScriptableObject, ISavvySettings
    {
        [Header("Logging")] 
        public bool Debug = false;
    }
}