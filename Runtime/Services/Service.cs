﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Savvy.Infrastructure;
using Savvy.Services.SaveLoad;

namespace Savvy.Services
{
    public sealed class Service : NetSavvy
    {
        public static Service Container { get; private set; }

        private readonly SavvySettings _settings;
        private readonly Dictionary<IService, Action> _services = new();

        private ISaveLoadService _saveLoad;
        
        public Service(SavvySettings settings)
        {
            Container = this;
            _settings = settings;
        }

        public void RegisterSingle<TService>(TService service, ServiceType type) where TService : IService
        {
            var registeredService = SingleService<TService>.Instance;

            if (registeredService != null)
            {
                Error($"{type} service '{registeredService.GetType().Name}' is already registered");
                return;
            }

            registeredService = SingleService<TService>.Instance = service;
            Action action = SingleService<TService>.Remove;
            _services.Add(registeredService, action);
            Debug($"{type} service '{registeredService.GetType().Name}' registered. Count services: {_services.Count}", _settings.Debug);
            
            if (registeredService is ISaveLoadService saveLoad) 
                _saveLoad = saveLoad;
        }

        public TService GetSingle<TService>() where TService : IService
        {
            var service = SingleService<TService>.Instance;

            if (service == null)
            {
                var type = typeof(TService);
                var serviceType = ServiceType.Unknown;

                if (typeof(IProjectService).IsAssignableFrom(type)) 
                    serviceType = ServiceType.Project;

                if (typeof(ISceneService).IsAssignableFrom(type))
                    serviceType = ServiceType.Scene;
                
                var msg = $"{serviceType} service '{type.Name}' not registered. ";

                if (typeof(ISavvyService).IsAssignableFrom(type))
                    msg += $"Select '{SavvyConstants.ResourcesPath}/{nameof(SavvySettings)}', " +
                           $"enter checkbox on the service you need and click '{SavvyConstants.ResolveButton}'";
                else
                    msg += $"Register the service '{type.Name}'";
                
                Error(msg);
            }

            return service;
        }

        public async Task SetupFromBehaviourAsync(ServiceType type)
        {
            Inject(type);
            CheckSettings(type);
            await InitAsync(type);
            AddToSavedList(type);
        }

        public void UpdateFromBehaviour()
        {
            foreach (var service in _services)
                if (service.Key is ITickable updatable)
                    updatable.Tick();
        }

        public void FixedUpdateFromBehaviour()
        {
            foreach (var service in _services)
                if (service.Key is IFixedTickable updatable)
                    updatable.FixedTick();
        }

        public void LateUpdateFromBehaviour()
        {
            foreach (var service in _services)
                if (service.Key is ILateTickable updatable)
                    updatable.LateTick();
        }

        public void PauseFromBehaviour(bool isPause)
        {
            foreach (var service in _services)
            {
                service.Key.Pause(isPause);
                Debug($"Service '{service.GetType().Name}' paused '{isPause}'", _settings.Debug);
            }
        }

        public void DestroyFromBehaviour(ServiceType type)
        {
            Destroy(type);
            RemoveFromSavedList(type);
            Remove(type);
        }
        
        private void Inject(ServiceType type)
        {
            foreach (var service in GetServicesByType(type))
            {
                service.Inject();
                Debug($"{type} service '{service.GetType().Name}' injected", _settings.Debug);
            }
        }
        
        private void CheckSettings(ServiceType type)
        {
            foreach (var service in GetServicesByType(type))
            {
                service.CheckSettings();
                Debug($"{type} service '{service.GetType().Name}' check settings", _settings.Debug);
            }
        }

        private async Task InitAsync(ServiceType type)
        {
            foreach (var service in GetServicesByType(type))
            {
                await service.InitAsync();
                Debug($"{type} service '{service.GetType().Name}' initiated", _settings.Debug);
            }
        }

        private void AddToSavedList(ServiceType type)
        {
            if (_saveLoad == null)
                return;
            
            foreach (var service in GetServicesByType(type))
            {
                _saveLoad.AddToSavedList(service);
                Debug($"{type} service '{service.GetType().Name}' add to 'Saved List'", _settings.Debug);
            }
        }

        private void RemoveFromSavedList(ServiceType type)
        {
            if (_saveLoad == null)
                return;
            
            foreach (var service in GetServicesByType(type))
            {
                _saveLoad.RemoveFromSavedList(service);
                Debug($"{type} service '{service.GetType().Name}' remove from 'Saved List'", _settings.Debug);
            }
        }

        private void Destroy(ServiceType type)
        {
            foreach (var service in GetServicesByType(type))
            {
                service.Destroy();
                Debug($"{type} service '{service.GetType().Name}' destroyed", _settings.Debug);
            }
        }

        private void Remove(ServiceType type)
        {
            foreach (var service in GetActionsByType(type))
            {
                service.Value?.Invoke();
                Debug($"{type} service '{service.Key.GetType().Name}' invoke action 'Remove'", _settings.Debug);
            }

            foreach (var service in GetServicesByType(type))
            {
                _services.Remove(service);
                Debug($"{type} service '{service.GetType().Name}' remove. Count services: {_services.Count}", _settings.Debug);
            }

            if (type == ServiceType.Project)
            {
                Container = null;
                Debug("'Service container' removed", _settings.Debug);
            }
        }

        private IEnumerable<IService> GetServicesByType(ServiceType type)
        {
            switch (type)
            {
                case ServiceType.Project:
                    return _services.Where(pair => pair.Key is IProjectService).Select(pair => pair.Key).ToList();
                case ServiceType.Scene:
                    return _services.Where(pair => pair.Key is ISceneService).Select(pair => pair.Key).ToList();
                default:
                    return null;
            }
        }

        private IDictionary<IService, Action> GetActionsByType(ServiceType type)
        {
            switch (type)
            {
                case ServiceType.Project:
                    return _services.Where(service => service.Key is IProjectService).ToDictionary(service => service.Key, service => service.Value);
                case ServiceType.Scene:
                    return _services.Where(service => service.Key is ISceneService).ToDictionary(service => service.Key, service => service.Value);
                default:
                    return null;
            }
        }
    }
}