﻿using Savvy.Extensions;
#if SAVVY_FIREBASE_APP && SAVVY_FIREBASE_REMOTE_CONFIG
using FirebaseInstance = Firebase.RemoteConfig.FirebaseRemoteConfig;
using System;
using System.Text;
using System.Threading.Tasks;
#else
using UnityEngine;
#endif

namespace Savvy.Services.FirebaseRemoteConfig
{
    public class FirebaseRemoteConfigService<TConfigData> : ServiceBase, IFirebaseRemoteConfigService<TConfigData>
    {
        private readonly string _sourceSettings = $"{nameof(Savvy)}/{nameof(FirebaseRemoteConfigSettings)}";
        private readonly FirebaseRemoteConfigSettings _settings;
        private readonly TConfigData _configDataDefault;

#if SAVVY_FIREBASE_APP && SAVVY_FIREBASE_REMOTE_CONFIG
        private FirebaseInstance _instance;
#endif
        private TConfigData _configData;
        
        public FirebaseRemoteConfigService(TConfigData configDataDefault)
        {
            _settings = _sourceSettings.LoadResources<FirebaseRemoteConfigSettings>();
            _configDataDefault = configDataDefault;
        }

        public TConfigData GetConfigData()
        {
#if SAVVY_FIREBASE_APP && SAVVY_FIREBASE_REMOTE_CONFIG
            return _configData;
#else
            return _configDataDefault;
#endif
        }

#if SAVVY_FIREBASE_APP && SAVVY_FIREBASE_REMOTE_CONFIG
        public override async Task InitAsync()
        {
            _instance = FirebaseInstance.DefaultInstance;
            await LoadRemoteConfigAsync();
        }
        
        private async Task LoadRemoteConfigAsync()
        {
            try
            {
                Debug($"Load remote config", _settings.Debug);
                await _instance.FetchAsync(TimeSpan.Zero);
                
                Debug($"Activate remote config", _settings.Debug);
                await _instance.ActivateAsync();
                
                await BuildJson();
            }
            catch (Exception ex)
            {
                Error($"Failed to fetch remote config: {ex.Message}");
                await BuildDefault();
            }
        }

        private Task BuildJson()
        {
            var jsonBuilder = new StringBuilder();
            jsonBuilder.Append("{");
            
            foreach (var value in _instance.AllValues) 
                jsonBuilder.Append($"\"{value.Key}\":\"{value.Value.StringValue}\",");

            if (jsonBuilder.Length > 1) 
                jsonBuilder.Length--;

            jsonBuilder.Append("}");
            
            var json = jsonBuilder.ToString();
            _configData = json.FromJson<TConfigData>();
            Debug($"Remote config built '{_configData.ToJson()}'", _settings.Debug);
            return Task.CompletedTask;
        }

        private Task BuildDefault()
        {
            _configData = _configDataDefault;
            Debug($"Default config built '{_configData.ToJson()}'", _settings.Debug);
            return Task.CompletedTask;
        }
#else
        private void DebugNotSupport() =>
            Debug($"'{Application.platform}' is not supported in '{nameof(FirebaseRemoteConfigService<TConfigData>)}'", _settings.Debug);
#endif
    }
}