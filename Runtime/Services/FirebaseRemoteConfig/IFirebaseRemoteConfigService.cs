namespace Savvy.Services.FirebaseRemoteConfig
{
    public interface IFirebaseRemoteConfigService<TConfigData> : ISavvyService
    {
        TConfigData GetConfigData();
    }
}