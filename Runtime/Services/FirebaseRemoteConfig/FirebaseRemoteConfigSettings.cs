using Savvy.Infrastructure;
using UnityEngine;

namespace Savvy.Services.FirebaseRemoteConfig
{
    [CreateAssetMenu(menuName = nameof(Savvy) + "/" + nameof(FirebaseRemoteConfigSettings), fileName = nameof(FirebaseRemoteConfigSettings))]
    public class FirebaseRemoteConfigSettings : ValidateScriptableObject, ISavvySettings
    {
        [Header("Logging")] 
        public bool Debug = false;
    }
}