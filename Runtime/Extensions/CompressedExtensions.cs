﻿using System.IO;
using System.IO.Compression;
using System.Runtime.Serialization.Formatters.Binary;

namespace Savvy.Extensions
{
    public static class CompressedExtensions
    {
        public static T ToDeserializeCompressed<T>(this byte[] bytes)
        {
            using (var input = new MemoryStream(bytes))
            using (var compression = new GZipStream(input, CompressionMode.Decompress))
            using (var output = new MemoryStream())
            {
                compression.CopyTo(output);
                var binaryFormatter = new BinaryFormatter();
                output.Position = 0;
                
                var deserialized = binaryFormatter.Deserialize(output);
                var result = (T)deserialized;
                compression.Close();

                return result;
            }
        }
        
        public static byte[] ToSerializeCompressed<T>(this T value)
        {
            var binaryFormatter = new BinaryFormatter();
            
            using (var memoryStream = new MemoryStream())
            {
                binaryFormatter.Serialize(memoryStream, value);

                using (var input = new MemoryStream(memoryStream.ToArray()))
                using (var output = new MemoryStream())
                {
                    using (var compression = new GZipStream(output, CompressionMode.Compress)) 
                        input.CopyTo(compression);

                    return output.ToArray();
                }
            }
        }
    }
}