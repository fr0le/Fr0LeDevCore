﻿using System.Globalization;

namespace Savvy.Extensions
{
    public static class IntExtensions
    {
        public static int ToIntByBool(this bool value) => 
            value ? 1 : 0;
        
        public static int ToIntOrDefault(this int? value, int defaultValue = default) =>
            value ?? defaultValue;

        public static int ToIntOrDefault(this string value, int defaultValue = default)
        {
            try
            {
                return int.Parse(value, CultureInfo.InvariantCulture.NumberFormat);
            }
            catch
            {
                return defaultValue;
            }
        }
        
        public static bool IsEqual(this int value1, int value2) => 
            value1 == value2;

        public static bool IsGreaterThan(this int value1, int value2) => 
            value1 > value2;

        public static bool IsLessThan(this int value1, int value2) => 
            value1 < value2;

        public static bool IsGreaterOrEqual(this int value1, int value2) => 
            value1 >= value2;

        public static bool IsLessOrEqual(this int value1, int value2) => 
            value1 <= value2;
    }
}