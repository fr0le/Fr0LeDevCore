﻿using System;
using Savvy.Services;
using Savvy.Services.Logger;
using UnityEngine;

namespace Savvy.Extensions
{
    public static class EnumExtensions
    {
        public static TEnum ToEnumOrDefault<TEnum>(this string value, TEnum defaultValue = default) where TEnum : struct => 
            Enum.TryParse(value, out TEnum result) ? result : defaultValue;   
        
        public static TEnum ToEnumOrDefault<TEnum>(this Enum value, TEnum defaultValue = default) where TEnum : struct => 
            Enum.TryParse(value.ToString(), out TEnum result) ? result : defaultValue;

        public static void ToEnumUnknown<TEnum>(this TEnum enumValue) => 
            Error($"Unknown type '{enumValue}' in enum '{typeof(TEnum).Name}'");
    
        public static void ToEnumTypeUnknown<TEnum>(this TEnum enumValue) => 
            Error($"Unknown type '{enumValue}'");
        
        private static void Error(object msg)
        {
            var log = SingleService<ILoggerService>.Instance;
     
            if (log != null)
                log.Error(msg);
            else
                Debug.LogError(msg);
        }
    }
}