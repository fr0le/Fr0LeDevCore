﻿namespace Savvy.Extensions
{
    public static class StringExtensions
    {
        public static string ToStringOrDefault(this object value, string defaultValue = default) => 
            value == null ? defaultValue : value.ToString();
    }
}