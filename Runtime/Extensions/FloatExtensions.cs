﻿using System.Globalization;

namespace Savvy.Extensions
{
    public static class FloatExtensions
    {
        public static float ToFloatOrDefault(this float? value, float defaultValue = default) =>
            value ?? defaultValue;
        
        public static float ToFloatOrDefault(this string value, float defaultValue = default)
        {
            try
            {
                return float.Parse(value, CultureInfo.InvariantCulture.NumberFormat);
            }
            catch
            {
                return defaultValue;
            }
        }
        
        public static bool IsEqual(this float value1, float value2) => 
            value1 == value2;

        public static bool IsGreaterThan(this float value1, float value2) => 
            value1 > value2;

        public static bool IsLessThan(this float value1, float value2) => 
            value1 < value2;

        public static bool IsGreaterOrEqual(this float value1, float value2) => 
            value1 >= value2;

        public static bool IsLessOrEqual(this float value1, float value2) => 
            value1 <= value2;
    }
}