﻿using System;
using UnityEngine;

namespace Savvy.Extensions
{
    public static class UrlExtensions
    {
        public static void OpenURL(this string url) => 
            Application.OpenURL(url);
        
        public static void OpenEmailClient(this string recipient, string subject = default, string body = default)
        {
            var url = $"mailto:{recipient}?subject={EscapeDataString(subject)}&body={EscapeDataString(body)}";
            Application.OpenURL(url);
        }

        private static string EscapeDataString(string url) => 
            Uri.EscapeDataString(url.ToStringOrDefault(""));
    }
}