﻿using UnityEngine;

namespace Savvy.Extensions
{
    public static class VectorsExtensions
    {
        public static Vector2Data ToVector2Data(this Vector2 vector) => new()
        {
            X = vector.x,
            Y = vector.y
        };

        public static Vector2 FromVector2Data(this Vector2Data vectorData) =>
            new(vectorData.X, vectorData.Y);
        
        public static Vector3Data ToVector3Data(this Vector3 vector) => new()
        {
            X = vector.x,
            Y = vector.y,
            Z = vector.z
        };

        public static Vector3 FromVector3Data(this Vector3Data vectorData) =>
            new(vectorData.X, vectorData.Y, vectorData.Z);   
        
        public static Vector4Data ToVector4Data(this Vector4 vector) => new()
        {
            X = vector.x,
            Y = vector.y,
            Z = vector.z,
            W = vector.w
        };
        
        public static Vector4 FromVector4Data(this Vector4Data vectorData) =>
            new(vectorData.X, vectorData.Y, vectorData.Z, vectorData.W);
    }
}