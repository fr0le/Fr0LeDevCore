﻿using System;

namespace Savvy.Extensions
{
    [Serializable]
    public class Vector4Data
    {
        public float X;
        public float Y;
        public float Z;
        public float W;
    }
}