﻿using System;

namespace Savvy.Extensions
{
    [Serializable]
    public class Vector3Data
    {
        public float X;
        public float Y;
        public float Z;
    }
}