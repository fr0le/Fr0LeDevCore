﻿using System;

namespace Savvy.Extensions
{
    [Serializable]
    public class Vector2Data
    {
        public float X;
        public float Y;
    }
}