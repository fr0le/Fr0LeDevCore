﻿using System;

namespace Savvy.Extensions
{
    [Serializable]
    public class QuaternionData
    {
        public float X;
        public float Y;
        public float Z;
        public float W;
    }
}