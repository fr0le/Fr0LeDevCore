﻿using UnityEngine;

namespace Savvy.Extensions
{
    public static class QuaternionExtensions
    {
        public static QuaternionData ToQuaternionData(this Quaternion quaternion) => new()
        {
            X = quaternion.x,
            Y = quaternion.y,
            Z = quaternion.z,
            W = quaternion.w
        };

        public static Quaternion FromQuaternionData(this QuaternionData quaternionData) =>
            new(quaternionData.X, quaternionData.Y, quaternionData.Z, quaternionData.W);
    }
}