﻿using Savvy.Services;
using Savvy.Services.Logger;
using UnityEngine;

namespace Savvy.Extensions
{
    public static class ResourcesExtensions
    {
        public static TResource LoadResources<TResource>(this string source) where TResource : ScriptableObject
        {
            var resource = Resources.Load<TResource>(source);

            if (resource == null)
            {
                var type = typeof(TResource);
                var msg = $"Resource file '{SavvyConstants.ResourcesPath}/{source}' not found. ";
                
                if (typeof(ISavvySettings).IsAssignableFrom(type))
                    msg += $"Select '{SavvyConstants.ResourcesPath}/{nameof(SavvySettings)}', " +
                           $"enter checkbox on the service you need and click '{SavvyConstants.ResolveButton}'";
                else
                    msg += $"Create resource file on path '{SavvyConstants.ResourcesPath}/{source}'";
                
                Error(msg);
            }

            return resource;
        }

        public static TResources[] LoadResourcesAll<TResources>(this string source) where TResources : ScriptableObject
        {
            var resources = Resources.LoadAll<TResources>(source);

            if (resources == null || resources.Length == 0)
            {
                var type = typeof(TResources);
                var msg = $"Resource files '{SavvyConstants.ResourcesPath}/{source}' not found. ";
                
                if (typeof(ISavvySettings).IsAssignableFrom(type))
                    msg += $"Select '{SavvyConstants.ResourcesPath}/{nameof(SavvySettings)}', " +
                           $"enter checkbox on the service you need and click '{SavvyConstants.ResolveButton}'";
                else
                    msg += $"Create resource file on path '{SavvyConstants.ResourcesPath}/{source}'";
                
                Error(msg);
            }
            
            return resources;
        }

        private static void Error(object msg)
        {
            var log = SingleService<ILoggerService>.Instance;

            if (log != null)
                log.Error(msg);
            else
                Debug.LogError(msg);
        }
    }
}