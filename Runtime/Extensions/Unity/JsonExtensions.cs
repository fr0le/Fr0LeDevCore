﻿using UnityEngine;

namespace Savvy.Extensions
{
    public static class JsonExtensions
    {
        public static string ToJson(this object obj) =>
            JsonUtility.ToJson(obj);

        public static TData FromJson<TData>(this string json) =>
            JsonUtility.FromJson<TData>(json);

        public static bool IsValidJson(this string json) => 
            json.StartsWith("{") && json.EndsWith("}") && json.Length > 2;
    }
}