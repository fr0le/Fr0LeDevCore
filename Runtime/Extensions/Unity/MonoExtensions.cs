﻿using UnityEngine;

namespace Savvy.Extensions
{
    public static class MonoExtensions
    {
        public static void SetActiveGameObject(this MonoBehaviour monoBehaviour, bool value) => 
            monoBehaviour.gameObject.SetActive(value);
        
        public static void SetActiveComponent(this Behaviour behaviour, bool value) => 
            behaviour.enabled = value;
        
        public static void SetActiveComponent(this Collider collider, bool value) => 
            collider.enabled = value;
        
        public static bool TagIsEnum<TEnum>(this Collider collider, TEnum enumValue) where TEnum : struct => 
            collider.tag.ToEnumOrDefault<TEnum>().Equals(enumValue);
        
        public static bool LayerIsEnum<TEnum>(this Collider collider, TEnum enumValue) where TEnum : struct => 
            LayerMask.LayerToName(collider.gameObject.layer).ToEnumOrDefault<TEnum>().Equals(enumValue);
        
        public static string ToTextColor(this string value, Color color) => 
            $"<color=#{ColorUtility.ToHtmlStringRGBA(color)}>{value}</color>";
    }
}