﻿using System;
using UnityEngine;

namespace Savvy.Extensions
{
    public static class AnimationsExtensions
    {
        public static int ToAnimatorHash(this string name) => 
            Animator.StringToHash(name);
        
        public static int ToAnimatorHash(this Enum name) => 
            Animator.StringToHash(name.ToString());
    }
}