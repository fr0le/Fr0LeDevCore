﻿using System.Globalization;

namespace Savvy.Extensions
{
    public static class LongExtensions
    {
        public static long ToLongOrDefault(this long? value, long defaultValue = default) =>
            value ?? defaultValue;
        
        public static long ToLongOrDefault(this string value, long defaultValue = default)
        {
            try
            {
                return long.Parse(value, CultureInfo.InvariantCulture.NumberFormat);
            }
            catch
            {
                return defaultValue;
            }
        }
        
        public static bool IsEqual(this long value1, long value2) => 
            value1 == value2;

        public static bool IsGreaterThan(this long value1, long value2) => 
            value1 > value2;

        public static bool IsLessThan(this long value1, long value2) => 
            value1 < value2;

        public static bool IsGreaterOrEqual(this long value1, long value2) => 
            value1 >= value2;

        public static bool IsLessOrEqual(this long value1, long value2) => 
            value1 <= value2;
    }
}