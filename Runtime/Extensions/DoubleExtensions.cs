﻿using System.Globalization;

namespace Savvy.Extensions
{
    public static class DoubleExtensions
    {
        public static double ToDoubleOrDefault(this double? value, double defaultValue = default) =>
            value ?? defaultValue;
        
        public static double ToDoubleOrDefault(this string value, double defaultValue = default)
        {
            try
            {
                return double.Parse(value, CultureInfo.InvariantCulture.NumberFormat);
            }
            catch
            {
                return defaultValue;
            }
        }
        
        public static bool IsEqual(this double value1, double value2) => 
            value1 == value2;

        public static bool IsGreaterThan(this double value1, double value2) => 
            value1 > value2;

        public static bool IsLessThan(this double value1, double value2) => 
            value1 < value2;

        public static bool IsGreaterOrEqual(this double value1, double value2) => 
            value1 >= value2;

        public static bool IsLessOrEqual(this double value1, double value2) => 
            value1 <= value2;
    }
}