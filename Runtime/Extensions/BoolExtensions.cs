﻿namespace Savvy.Extensions
{
    public static class BoolExtensions
    {
        public static bool ToBoolOrDefault(this bool? value, bool defaultValue = default) =>
            value ?? defaultValue;

        public static bool ToBoolByInt(this int value) => 
            value == 1;

        public static bool ToBoolByString(this string value) => 
            value.ToIntOrDefault() == 1;
    }
}