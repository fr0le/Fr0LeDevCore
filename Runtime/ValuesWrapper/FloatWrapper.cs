﻿using Savvy.Extensions;

namespace Savvy.ValuesWrapper
{
    public class FloatWrapper
    {
        public float Value { get; private set; }
        
        public FloatWrapper(float value = 0) => 
            Value = value;

        public void Set(float value) => 
            Value = value;

        public void Add(float value = 1) => 
            Value += value;

        public void AddWrapper(float value = 1, float max = float.MaxValue) => 
            Value = GetReliableValue(Value, value, max);

        public void Sub(float value = 1) => 
            Value -= value;

        public bool SubWrapper(float value = 1)
        {
            if (Value.IsGreaterOrEqual(value))
            {
                Value -= value;
                return true;
            }

            return false;
        }

        private float GetReliableValue(float a, float b, float max)
        {
            var result = a + b;

            if (float.IsInfinity(result) || float.IsNaN(result))
                return max;

            return result > max ? max : result;
        }
    }
}