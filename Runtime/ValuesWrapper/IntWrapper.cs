﻿using System;
using Savvy.Extensions;

namespace Savvy.ValuesWrapper
{
    public class IntWrapper
    {
        public int Value { get; private set; }
        
        public IntWrapper(int value = 0) => 
            Value = value;

        public void Set(int value) => 
            Value = value;

        public void Add(int value = 1) => 
            Value += value;

        public void AddWrapper(int value = 1, int max = int.MaxValue) => 
            Value = GetReliableValue(Value, value, max);

        public void Sub(int value = 1) => 
            Value -= value;

        public bool SubWrapper(int value = 1)
        {
            if (Value.IsGreaterOrEqual(value))
            {
                Value -= value;
                return true;
            }

            return false;
        }

        private int GetReliableValue(int a, int b, int max)
        {
            try
            {
                checked
                {
                    var result = a + b;
                    return result > max ? max : result;
                }
            }
            catch (OverflowException)
            {
                return max;
            }
        }
    }
}