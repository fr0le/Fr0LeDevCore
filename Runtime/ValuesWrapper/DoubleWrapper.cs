﻿using Savvy.Extensions;

namespace Savvy.ValuesWrapper
{
    public class DoubleWrapper
    {
        public double Value { get; private set; }
        
        public DoubleWrapper(double value = 0) => 
            Value = value;

        public void Set(double value) => 
            Value = value;

        public void Add(double value = 1) => 
            Value += value;

        public void AddWrapper(double value = 1, double max = double.MaxValue) => 
            Value = GetReliableValue(Value, value, max);

        public void Sub(double value = 1) => 
            Value -= value;

        public bool SubWrapper(double value = 1)
        {
            if (Value.IsGreaterOrEqual(value))
            {
                Value -= value;
                return true;
            }

            return false;
        }

        private double GetReliableValue(double a, double b, double max)
        {
            var result = a + b;

            if (double.IsInfinity(result) || double.IsNaN(result))
                return max;

            return result > max ? max : result;
        }
    }
}