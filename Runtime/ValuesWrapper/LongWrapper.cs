﻿using System;
using Savvy.Extensions;

namespace Savvy.ValuesWrapper
{
    public class LongWrapper
    {
        public long Value { get; private set; }
        
        public LongWrapper(long value = 0) => 
            Value = value;

        public void Set(long value) => 
            Value = value;

        public void Add(long value = 1) => 
            Value += value;

        public void AddWrapper(long value = 1, long max = long.MaxValue) => 
            Value = GetReliableValue(Value, value, max);

        public void Sub(long value = 1) => 
            Value -= value;

        public bool SubWrapper(long value = 1)
        {
            if (Value.IsGreaterOrEqual(value))
            {
                Value -= value;
                return true;
            }

            return false;
        }
        
        private long GetReliableValue(long a, long b, long max)
        {
            try
            {
                checked
                {
                    var result = a + b;
                    return result > max ? max : result;
                }
            }
            catch (OverflowException)
            {
                return max;
            }
        }
    }
}