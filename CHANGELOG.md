## Savvy SDK

### [3.4.1] - 2025-03-09

- Оптимизация файлов
- Удалил примеры
- Добавил отдельную выгрузку иконок

### [3.4.0] - 2025-03-09

Asset Provider Service

- Добавлена обертка для Addressables

Save Load Service:

- Удалил костыль изменения сейва из любого места
- Добавил управляемую настройку разрешения сейва
- Добавил управляемую настройку для авто сейва

### [3.3.0] - 2025-03-03

Tenjin Service:

- Добавил обертку для `Tenjin`

### [3.2.0] - 2025-02-28

Random Service:

- Добавил получение `InsideUnitCircle` по радиусу

Исправления:

- Отказ от `.Equals`в пользу `==` для Enum
- Рефакторинг `SceneLoaderService`

Добавления:

- QuaternionExtensions
- ValidateScriptableObject
- Отправлка евентов о покупках
- Правки по `SocialService`

### [3.1.0] - 2024-12-13

Yookassa Service:

- Добавлена возможность отправки `Successful` и `Failed` евентов о покупках в AppMetrica

Extensions:

- Удалил `IsEnum` для сравнения enum, можно использовать дефолтный `Equals`

Packages:

- Update `AppMetrica` to 6.3.0
- Update `EDMU` to 1.2.183
- Update `CAS` to 4.0.0-beta8
- Update `Firebase` to 12.5.0

### [3.0.20] - 2024-11-24

Country Checker Service:

- Добавлен сервис, который определяет страну в виде `US`, `RU`, `UA` и т.д.
- Требует подключение к интернету для корректной проверки

Yookassa Service:

- Добавлен сервис платежей `Yookassa`, доступен через `Game Dev Club`

### [3.0.19] - 2024-11-22

IAP Service:

- Добавлена перегрузка методов, id можно передавать в виде `Enum` 

### [3.0.18] - 2024-11-21

IAP Service:

- Добавлено получение `ProductData`, в котором содержится вся информация о товаре

### [3.0.17] - 2024-11-07

Добавление:

- Поддержка `CAS` 4.0.0

Изменения:

- Рефакторинг `AdRevenue` евентов

### [3.0.16] - 2024-11-02

Добавление:

- Добавлена отправка `LTV` для `IMetricsService`

Изменения:

- Удален старый метод отправки для `IMetricsService`

### [3.0.15] - 2024-11-01

Integration:

- Added method `SendEvent(string name)` for `IMetricsService`

Changed:

- Remove old methods

### [3.0.14] - 2024-10-31

Changed:

- Remove old methods
- `Update Tickable` now works only after initialization
- Pause is common for all types of services

### [3.0.13] - 2024-10-31

Integration:

- Added detailed `AdRevenue` logs
- Added debug for canceled events for metrics

### [3.0.12] - 2024-10-18

Integration:

- Added save feedback status
- Added async Init for services
- Added `FirebaseRemoteConfigService`
- Added `UnityGamingService`
- Added `IAPServiceService`

### [3.0.11] - 2024-09-17

Integration:

- Add feedback logic to `SocialService`

### [3.0.10] - 2024-09-17

Integration:

- Added `GetRange(Vector2 range)` to `RandomService`
- Added `GetInsideUnitSphere(float radius)` to `RandomService`
- Added `SendAdAmount` from `CasService`

### [3.0.9] - 2024-09-07

Integration:

- Added `SocialService`

### [3.0.8] - 2024-08-27

Integration:

- Added `Instantiate` by `Vector3` and `Quaternion` for `IFactoryService`

### [3.0.7] - 2024-08-26

Integration:

- Added `ServiceSample` showcases the basis for creating a microservice

Changed:

- Rename method `LoadScene` to `LoadSceneAsync` in `ISceneLoaderService`

### [3.0.6] - 2024-08-18

Changed:

- Disable wrapper `FirstActivationAsUpdate` for `AppMetrica`

### [3.0.5] - 2024-08-17

Changed:

- Sample App with `AppMetrica`

### [3.0.4] - 2024-08-17

Integration:

- Added `RewardedAd` processing if closed early
- Added events for `InterstitialAd` and `RewardedAd`
- Added new sample `App`

Changed:

- Constants
- Naming

### [3.0.3] - 2024-08-10

Integration:

- Added `IsReady` events to mediations
- Added `IsReady` checked to mediations

### [3.0.2] - 2024-08-10

Integration:

- Added notification clearing when starting a game

Changed:

- Changed check by interface type
- Changed processor directive check

### [3.0.1] - 2024-08-09

Integration:

- Added `Notification` service
- Added `OnEnable` and `OnDisable` methods to Services

Changed:

- Setup corrections
- Using corrections

### [3.0.0] - 2024-08-08

Integration:

- Added define symbols `SAVVY_APP_METRICA` for `AppMetrica`
- Added define symbols `SAVVY_CAS` for `CAS`
- Added define symbols `SAVVY_FIREBASE` for `Firebase`

Changed:

- External service `AppMetrica` added to `SDK`
- External service `CAS` added to `SDK`
- External service `Firebase` added to `SDK`
- `Savvy Core` -> `Savvy SDK`
- Refactoring for new services
- Adjusted namespaces

### [2.1.5] - 2024-08-04

Integration:

- Added `WebGLService` wrapper

### [2.1.4] - 2024-08-03

Changed:

- Fixed `NullReferenceException` in `Service`

### [2.1.3] - 2024-08-03

Integration:

- Added `FactoryService` Extension

Changed:

- `ProgressData` moved to `SaveLoadService`

Removing:

- Remove `ProgressService`
- Remove `StaticDataService`

### [2.1.2] - 2024-07-31

Integration:

- Added `LoadResourcesAll` Extension

Changed:

- Renaming `StaticData` files

### [2.1.0] - 2024-07-30

Integration:

- Added new services
- Added `ProjectBehaviourBase`
- Added `SceneBehaviourBase`

Changed:

- Full refactoring `Services`
- Full refactoring `Infrastructure`

### [2.0.0] - 2024-04-12

Integration:

- Added default `timer` in `FixedTimer` constructor
- Added abstract for `StaticDataService`
- Added `NetSavvy` and `MonoSavvy` base classes

Changed:

- Refactoring `Fr0LeDevLib Core` => `Savvy Core`
- Refactoring infrastructure

### [1.3.2] - 2024-04-03

Integration:

- Added `RandomAngle` method in `RandomService`
- Added `NumbersService`

### [1.3.1] - 2024-03-25

Integration:

- Added `TimeFormat` (sample: 99:23:59:59)
- Added `TimeFormatLocalization` (sample: 99d 23h 59m 59s)
- Added `Extensions` to `DateTimeService`

Changed:

- Refactoring `DateTimeService`
- Update `NewApp` example

### [1.3.0] - 2024-03-21

Integration:

- Added default values in constructor for `ValuesWrapper`
- Added `DateTimeService`
- Added check duplicate registry services
- Added auto registration services

Changed:

- Refactoring DI
- Remove old modules
- Update `NewApp` example

### [1.2.12] - 2024-03-17

Integration:

- Added `double GetRange(min, max)` in `RandomService`

Changed:

- `RandomService` now uses `System.Random` instead of `UnityEngine.Random`

### [1.2.11] - 2024-03-12

Integration:

- Added `FixedTimer` utils
- Added `Add` and `Sub` operation in `ValuesWrapper` without checking

Changed:

- Checking for maximum value when adding values in `ValuesWrapper`
- Refactoring `VectorData`
- Refactoring `MixerData`
- Refactoring `LocalizationService`
- Translation error handling
- Update `NewApp` example

### [1.2.10] - 2024-02-21

Integration:

- Added bool methods in `SceneLoaderService`

Changed:

- Update `NewApp` example, added registration `SceneLoaderService`

### [1.2.9] - 2024-02-20

Integration:

- Added bool methods in `RandomService`

Changed:

- Update `NewApp` example

### [1.2.8] - 2024-02-19

Integration:

- Added `RandomService`

Changed:

- Update `NewApp` example, added registration `RandomService`

### [1.2.7] - 2024-02-18

Integration:

- Added `AudioService`

Changed:

- Update `NewApp` example, added registration `AudioService`

### [1.2.6] - 2024-02-13

Integration:

- Added `LocalizationService`

Changed:

- Update `NewApp` example, added registration `LocalizationService`

### [1.2.5] - 2024-02-10

Integration:

- Added `SaveType` to `SaveLoadService`

Changed:

- Update `NewApp` example

### [1.2.4] - 2024-02-06

Integration:

- Added `NewApp` example

### [1.2.3] - 2024-02-06

Changed:

- Refactoring `SaveLoadService`
- Refactoring `ProgressService`

### [1.2.2] - 2024-02-05

Integration:

- Added `SaveLoadService`
- Added interface for `ProgressService`

Changed:

- Refactoring `Fr0leDevLib`

### [1.2.1] - 2024-02-05

Integration:

- Added additional functionality to `LoggerService`

Changed:

- Refactoring `Fr0leDevLib`

### [1.2.0] - 2024-02-04

Changed:

- Division library into `Core`
- Refactoring `Logs module` to `Logs service`

Integration:

- Added automatic creator of `LogSettings` file

### [1.1.6] - 2024-01-31

Integration:

- Added comparison methods to `ValuesWrapper/[int, long, float, double]`

### [1.1.5] - 2024-01-28

Integration:

- Added check for scene existence
- Added `defaultSceneName` in `StateMachine`

### [1.1.4] - 2024-01-27

Removing:

- Remove incorrect methods in `Extensions/[int, long, float, double]`

Changed:

- Refactoring `Extensions/[int, long, float, double]`

Integration:

- Added new methods which check value be null or return default `Extensions/[int, long, float, double]`

### [1.1.3] - 2024-01-25

Integration:

- Added `ValuesWrapper/*`

### [1.1.2] - 2024-01-25

Changed:

- Refactoring `Infrastructure/InitWrapper` to `Infrastructure/BootstrapEditorWrapper`

### [1.1.1] - 2024-01-25

Integration:

- Added `Infrastructure/StateMachine/BaseState`

### [1.1.0] - 2024-01-25

Integration:

- Added `Infrastructure/StateMachine` for app states
- Added `Infrastructure/Services` DI Container
- Added `Infrastructure/AppStaticData` for options

### [1.0.26] - 2024-01-24

Integration:

- Added `Unity Vectors` Extensions

### [1.0.25] - 2024-01-19

Integration:

- Added `Timer` example

### [1.0.24] - 2024-01-05

Integration:

- Added `Utils/DefaultSettings` module
- Added `Utils/FpsCounter` module

### [1.0.23] - 2023-12-05

Changed:

- Update `Anticheats` and `Localizations` modules
- Refactoring `Example`

### [1.0.22] - 2023-11-10

Changed:

- Added flag `isDontDestroy` to `Anticheats` and `Localizations` modules

### [1.0.21] - 2023-11-05

Changed:

- Added 100 random nicknames to `GeneratorName` module

### [1.0.20] - 2023-10-31

Changed:

- Refactoring `Anticheats` module

### [1.0.19] - 2023-10-31

Integration:

- Added `Anticheats` module

### [1.0.18] - 2023-10-27

Integration:

- Added `Infrastructure` sample

### [1.0.17] - 2023-10-27

Integration:

- Added `Infrastructure/InitWrapper` module

### [1.0.16] - 2023-10-24

Changed:

- Refactoring `Extensions/*` modules

### [1.0.15] - 2023-10-24

Changed:

- Added work with values to `Extensions/*` modules

### [1.0.14] - 2023-10-23

Changed:

- Added args on signature to `LocalizationExtensions` module

### [1.0.13] - 2023-10-21

Integration:

- Added `Interfaces/Confirmations/IConfirmationAmount` interface

### [1.0.12] - 2023-10-19

Integration:

- Added `Interfaces/Confirmations/IConfirmation` interface

### [1.0.11] - 2023-10-17

Changed:

- Refactoring `Utils/RandomName` module to `Generators/GeneratorName`
- Refactoring `Utils/GeneratorUid` module to `Generators/GeneratorUid`
- Refactoring `Utils/Randomize` module to `Generators/GeneratorRandom`

### [1.0.10] - 2023-10-16

Integration:

- Added `Utils/RandomName` module

### [1.0.9] - 2023-10-13

Integration:

- Added `Localizations` module
- Added samples for `Localizations` module

### [1.0.8] - 2023-10-12

Integration:

- Added timestamp in `Logs` module

Changed:

- Refactoring `Logs` module

### [1.0.7] - 2023-10-08

Integration:

- Added `Extensions/BoolExtensions` module

Changed:

- Refactoring `Extensions/*` modules
- Refactoring `Logs` module

### [1.0.6] - 2023-10-06

Integration:

- Added `Utils/Randomize` module

### [1.0.5] - 2023-10-06

Changed:

- Refactoring `Extensions/*` modules

### [1.0.4] - 2023-10-06

Integration:

- Added `Extensions/StringExtensions` module

### [1.0.3] - 2023-10-06

Integration:

- Added `Extensions/IntExtensions` module
- Added `Extensions/LongExtensions` module
- Added `Extensions/FloatExtensions` module
- Added `Extensions/DoubleExtensions` module
- Added `Extensions/EnumExtensions` module

### [1.0.2] - 2023-10-05

Integration:

- Added `Extensions/JsonExtensions` module

### [1.0.1] - 2023-10-05

Integration:

- Added `Utils/GeneratorUid` module

### [1.0.0] - 2023-09-30

Integration:

- Added `Logs` module