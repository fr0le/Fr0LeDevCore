<table align="center">
<tr><td align="center" width="9999">

### Savvy SDK

<a href="https://gitlab.com/savvy-labs/savvy-sdk/-/tree/3.4.1![img.png](img.png)" target="_blank">
<img src="https://img.shields.io/badge/v3.4.1-f5a97f?style=for-the-badge&colorA=363a4f&label=Release" alt="Release" />
</a>

<a href="https://github.com/googlesamples/unity-jar-resolver?path=upm#v1.2.183">
<img src="https://img.shields.io/badge/v1.2.183-f5a97f?style=for-the-badge&colorA=363a4f&label=EDM4U" alt="EDM4U" />
</a>

<a href="https://savvy-sdk.gitbook.io/docs">
<img src="https://img.shields.io/badge/Docs-f5a97f?style=for-the-badge&logo=gitbook&logoColor=white&colorA=363a4f" alt="Docs" />
</a>

</td></tr>

<tr><td align="center">

### Mediation

<a href="https://github.com/cleveradssolutions/CAS-Unity/tree/4.0.0-beta8">
<img src="https://img.shields.io/badge/v4.0.0.beta8-f5a97f?style=for-the-badge&colorA=363a4f&label=CAS" alt="CAS" />
</a>

</td></tr>

<tr><td align="center">

### Analytics

<a href="https://github.com/appmetrica/appmetrica-unity-plugin/tree/v6.3.0">
<img src="https://img.shields.io/badge/v6.3.0-f5a97f?style=for-the-badge&colorA=363a4f&label=AppMetrica" alt="AppMetrica" />
</a>

</td></tr>

<tr><td align="center">

### Other

<a href="https://dl.google.com/firebase/sdk/unity/firebase_unity_sdk_12.5.0.zip">
<img src="https://img.shields.io/badge/v12.5.0-f5a97f?style=for-the-badge&colorA=363a4f&label=Firebase" alt="Firebase" />
</a>

</td></tr>

</table>

## Installation

### 1. Add the Savvy Core to Your Project

- In your open Unity project, navigate to `Window > Package Manager > + > Add package from Git URL...`
- Enter the Git URL in the text box:

```
https://gitlab.com/savvy-labs/savvy-sdk.git#3.4.1
```

- Click `Add`

## Configuring SDK

### 1. Create Savvy Settings File

- In your open Unity project, navigate to `Savvy > Create Savvy Settings File`

Path to configs:

```
Assets/Resources/Savvy/SavvySettings
```

### 2. Create Internal Services Settings Files

- Select `Assets/Resources/Savvy/SavvySettings`
- Select internal services and checkbox
- Click `Resolve`

Path to configs:

```
Assets/Resources/Savvy/Core/*
```

## Implementation SDK

### 1. Create Bootstrap Scene

- Create new scene `Bootstrap`
- Open `File > Build Settings...`
- Add scene `Bootstrap` to `Scenes In Build`
- Set id `0` for `Bootstrap` scene

### 2. Configuring Bootstrap Scene

- Open `Bootstrap` scene
- Remove all objects in scene
- Create empty GameObject
- Create script `ProjectBehaviour` and added to this GameObject

#### Example `ProjectBehaviour`

```
using Savvy.Core.Infrastructure;

public class ProjectBehaviour : ProjectBehaviourBase
{
    protected override void PostAwake()
    {
        //Implementation of your further loading
    }
    
    protected override void RegisterGameServices()
    {
        //Implementation of your custom services
    }
}
```

### 3. Configuring Game Scene

- Open your `Game` scene
- Create empty GameObject
- Create script `SceneBehaviour` and added to this GameObject

#### Example `SceneBehaviour`

```
using Savvy.Core.Infrastructure;

public class SceneBehaviour : SceneBehaviourBase
{
    protected override void PostAwake()
    {
        //Implementation of your further loading
    }
    
    protected override void RegisterGameServices()
    {
        //Implementation of your custom services
    }
}
```

## Implementation External Services

### 1. Create External Services Settings Files

- Add external services to Unity with link:

```
https://gitlab.com/savvy-sdk
```

- Select `Assets/Resources/Savvy/SavvySettings`
- Select external services and checkbox
- Click `Resolve`

Path to configs:

```
Assets/Resources/Savvy/*
```

## Implementation Custom Services

- Create interface

```
using Savvy.Core.Services;

public interface ICurrencyService : IProjectService
{
    int Money { get; }
    void Add(int count);
}
```

- Create service

```
public class using Savvy.Core.Services;

public class CurrencyService : ServiceBase, ICurrencyService
{
    public int Money { get; private set; }

    public CurrencyService()
    {
        //constructor logic
        //example: load settings for service
    }
    
    public override void Inject()
    {
        //inject services
        //example: inject custom SaveLoadService
    }

    public override void Init()
    {
        //init services
        //example: load progress
    }

    public void Add(int count)
    {
        Money += count;
    }
}
```

- Register service

```
using Savvy.Core.Infrastructure;

public class ProjectBehaviour : ProjectBehaviourBase
{
    protected override void PostAwake()
    {
        //Implementation of your further loading
    }
    
    protected override void RegisterGameServices()
    {
        //Implementation of your custom services
        RegisterService<ICurrencyService>(new CurrencyService());
    }
}
```

- Using service in other scripts

```
using Savvy.Core.Infrastructure;

public class Player : MonoSavvy
{
    private ICurrencyService _currency;

    private void Awake()
    {
        _currency = GetService<ICurrencyService>();
    }

    private void Start()
    {
        _currency.Add(100);
    }
}
```

## Dependencies

### 1. AppMetrica

```
{
  "dependencies": {
    "com.google.external-dependency-manager": "https://github.com/google-unity/external-dependency-manager.git#1.2.177",
    "io.appmetrica.analytics": "https://github.com/appmetrica/appmetrica-unity-plugin.git#v6.2.0"
  }
}
```

### 2. CAS

```
{
  "dependencies": {
    "com.google.external-dependency-manager": "https://github.com/google-unity/external-dependency-manager.git#1.2.177",
    "com.cleversolutions.ads.unity": "https://github.com/cleveradssolutions/CAS-Unity.git#3.9.2"
  }
}
```

### 3. Firebase

```
{
  "dependencies": {
    "com.google.external-dependency-manager": "https://github.com/google-unity/external-dependency-manager.git#1.2.177"
  }
}
```

### 4. Mobile Notifications

```
{
  "dependencies": {
    "com.unity.mobile.notifications": "2.3.2"
  }
}
```

### 5. In App Purchasing

```
{
  "dependencies": {
    "com.unity.purchasing": "4.12.2"
  }
}
```

# Thanks 🙏
- Oleksii Naumenko (Knowledge Syndicate)
- Kate Revvo (Knowledge Syndicate)